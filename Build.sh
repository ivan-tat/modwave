#!/bin/sh -e
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense
reset
make -j $(nproc) depend
make -j $(nproc)
