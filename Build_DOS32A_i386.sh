#!/bin/sh -e
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense
reset
. ./Version.mak
target="build/modwave-$VERSION-dos32a-i386.zip"
rm -rf build/dos32a-i386 "$target"
make -j $(nproc) BUILD=dos32a_i386 "$target"
