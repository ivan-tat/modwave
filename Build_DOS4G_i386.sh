#!/bin/sh -e
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense
reset
. ./Version.mak
target="build/modwave-$VERSION-dos4g-i386.zip"
rm -rf build/dos4g-i386 "$target"
make -j $(nproc) BUILD=dos4g_i386 "$target"
