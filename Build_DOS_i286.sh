#!/bin/sh -e
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense
reset
. ./Version.mak
target="build/modwave-$VERSION-dos-i286.zip"
rm -rf build/dos-i286 "$target"
make -j $(nproc) BUILD=dos_i286 "$target"
