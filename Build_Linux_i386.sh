#!/bin/sh -e
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense
reset
. ./Version.mak
target="build/modwave-$VERSION-linux-i386.tar.bz2"
rm -rf build/linux-i386 "$target"
make -j $(nproc) BUILD=linux_i386 "$target"
