#!/bin/sh -e
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense
reset
. ./Version.mak
target="build/modwave-$VERSION-linux-x86_64.tar.bz2"
rm -rf build/linux-x86_64 "$target"
make -j $(nproc) BUILD=linux_x86_64 "$target"
