#!/bin/sh -e
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense
reset
. ./Version.mak
target="build/modwave-$VERSION-windows-i386.zip"
rm -rf build/windows-i386 "$target"
make -j $(nproc) BUILD=windows_i386 "$target"
