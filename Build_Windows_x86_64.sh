#!/bin/sh -e
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense
reset
. ./Version.mak
target="build/modwave-$VERSION-windows-x86_64.zip"
rm -rf build/windows-x86_64 "$target"
make -j $(nproc) BUILD=windows_x86_64 "$target"
