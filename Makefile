#/bin/make -f
#
# 1. make Defines.mak
# 2. make -j $(nproc) depend
# 3. make -j $(nproc)
# 4. profit
# 5. make clean or distclean if needed
# 6. go to 2 or 3
#
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

builddir?=build
prefix?=/usr/local
exec_prefix?=$(prefix)
bindir?=$(exec_prefix)/bin

-include Defines.mak
include Version.mak
-include $(builddir)/Defines.mak
-include $(builddir)/Version.mak

LINUX_I386=modwave-$(VERSION)-linux-i386.tar.bz2
LINUX_X86_64=modwave-$(VERSION)-linux-x86_64.tar.bz2
WINDOWS_I386=modwave-$(VERSION)-windows-i386.zip
WINDOWS_X86_64=modwave-$(VERSION)-windows-x86_64.zip
DOS_I086=modwave-$(VERSION)-dos-i086.zip
DOS_I286=modwave-$(VERSION)-dos-i286.zip
DOS_I386=modwave-$(VERSION)-dos-i386.zip
DOS32A_I386=modwave-$(VERSION)-dos32a-i386.zip
DOS4G_I386=modwave-$(VERSION)-dos4g-i386.zip

# $F=list of flags to be defined in source files
F=

# $1=flag, $2=value
define def_flag=
$1?=$2
F+=$1
endef

$(eval $(call def_flag,DEBUG,0))
$(eval $(call def_flag,DEBUG_SONG,0))
$(eval $(call def_flag,DEBUG_LOAD,0))
$(eval $(call def_flag,DEBUG_DUMP_INSTRUMENTS,0))
$(eval $(call def_flag,DEBUG_DUMP_SAMPLES,0))
$(eval $(call def_flag,DEBUG_DUMP_ORDER,0))
$(eval $(call def_flag,DEBUG_DUMP_PATTERNS_STATS,0))
$(eval $(call def_flag,DEBUG_DUMP_PATTERNS,0))
$(eval $(call def_flag,DEBUG_AMPTAB,0))
$(eval $(call def_flag,DEBUG_PLAY,0))
# Correct dependent flags
ifeq ($(DEBUG),0)
 DEBUG_SONG=0
 DEBUG_LOAD=0
 DEBUG_DUMP_INSTRUMENTS=0
 DEBUG_DUMP_SAMPLES=0
 DEBUG_DUMP_ORDER=0
 DEBUG_DUMP_PATTERNS_STATS=0
 DEBUG_DUMP_PATTERNS=0
 DEBUG_AMPTAB=0
 DEBUG_PLAY=0
endif

INSTALL?=install
INSTALL_PROGRAM?=$(INSTALL)
MKDIR?=mkdir
RM?=rm -f

# $(CCTYPEH)=host C compiler type
# $(CCH)    =host C compiler
# $(CFLAGSH)=host C compiler's flags
# $(EH)     =host binary executable file extension
ifeq ($(OS),Windows_NT)
 CCTYPEH=gcc
 CCH=gcc
 CFLAGSH=
 EH=.exe
else ifneq ($(DJGPP),)
 CCTYPEH=gcc
 CCH=gcc
 CFLAGSH=
 EH=.exe
else
 CCTYPEH=gcc
 CCH=gcc
 CFLAGSH=
 EH=
endif

ifeq ($(CCTYPEH),gcc)
 CFLAGSH+=-O3 -std=c99 -Wall -Wextra -Wpedantic -I$(builddir)
 ifeq ($(DEBUG),1)
  CFLAGSH+=-g3
 endif
else
 $(error Unknown host compiler type: "$(CCTYPEH)")
endif

# $(CCTYPE)=target C compiler type
# $(LDTYPE)=target linker type
# $E       =target binary executable file extension
ifeq ($(BUILD),)
 CCTYPE=gcc
 CC=gcc
 LDTYPE=$(CCTYPE)
 LD=$(CC)
 LFLAGS=$(CFLAGS)
 E=$(EH)
else ifeq ($(BUILD),linux_i386)
 CCTYPE=gcc
 CC=gcc
 CFLAGS+=-m32 -march=i386 -mtune=generic
 LDTYPE=$(CCTYPE)
 LD=$(CC)
 LFLAGS=$(CFLAGS)
 E=
else ifeq ($(BUILD),linux_x86_64)
 CCTYPE=gcc
 CC=gcc
 CFLAGS+=-m64 -march=x86-64 -mtune=generic
 LDTYPE=$(CCTYPE)
 LD=$(CC)
 LFLAGS=$(CFLAGS)
 E=
else ifeq ($(BUILD),windows_i386)
 CCTYPE=gcc
 CC=i686-w64-mingw32-gcc
 CFLAGS+=-m32 -march=i386 -mtune=generic
 LDTYPE=$(CCTYPE)
 LD=$(CC)
 LFLAGS=$(CFLAGS)
 E=.exe
else ifeq ($(BUILD),windows_x86_64)
 CCTYPE=gcc
 CC=x86_64-w64-mingw32-gcc
 CFLAGS+=-m64 -march=x86-64 -mtune=generic
 LDTYPE=$(CCTYPE)
 LD=$(CC)
 LFLAGS=$(CFLAGS)
 E=.exe
else ifeq ($(BUILD),dos_i086)
 ifeq ($(WATCOM),)
  $(error Environment variable WATCOM is not set)
 endif
 CCTYPE=wcc
 CC=wcc
 CFLAGS+=-q -bt=dos -0 -fpi -ml
 LDTYPE=wlink
 LD=wlink
 LFLAGS=system dos\
  libpath $(WATCOM)/lib286\
  libpath $(WATCOM)/lib286/dos
 E=.exe
else ifeq ($(BUILD),dos_i286)
 ifeq ($(WATCOM),)
  $(error Environment variable WATCOM is not set)
 endif
 CCTYPE=wcc
 CC=wcc
 CFLAGS+=-q -bt=dos -2 -fpi -ml
 LDTYPE=wlink
 LD=wlink
 LFLAGS=system dos\
  libpath $(WATCOM)/lib286\
  libpath $(WATCOM)/lib286/dos
 E=.exe
else ifeq ($(BUILD),dos_i386)
 ifeq ($(WATCOM),)
  $(error Environment variable WATCOM is not set)
 endif
 CCTYPE=wcc
 CC=wcc
 CFLAGS+=-q -bt=dos -3 -fpi -ml -zff -zgf
 LDTYPE=wlink
 LD=wlink
 LFLAGS=system dos\
  libpath $(WATCOM)/lib386\
  libpath $(WATCOM)/lib386/dos
 E=.exe
else ifeq ($(BUILD),dos32a_i386)
 ifeq ($(WATCOM),)
  $(error Environment variable WATCOM is not set)
 endif
 CCTYPE=wcc
 CC=wcc386
 CFLAGS+=-q -bt=dos4g -3r -fpi
 LDTYPE=wlink
 LD=wlink
 LFLAGS=system dos32a option stub=$(WATCOM)/binw/dos32a.exe\
  libpath $(WATCOM)/lib386\
  libpath $(WATCOM)/lib386/dos
 E=.exe
else ifeq ($(BUILD),dos4g_i386)
 ifeq ($(WATCOM),)
  $(error Environment variable WATCOM is not set)
 endif
 CCTYPE=wcc
 CC=wcc386
 CFLAGS+=-q -bt=dos4g -3r -fpi
 LDTYPE=wlink
 LD=wlink
 LFLAGS=system dos4g\
  libpath $(WATCOM)/lib386\
  libpath $(WATCOM)/lib386/dos
 E=.exe
else
 $(error Unknown build: "$(BUILD)")
endif

ifeq ($(CCTYPE),gcc)
 CFLAGS+=-O3 -std=c99 -Wall -Wextra -Wpedantic -I$(builddir)
 ifeq ($(DEBUG),1)
  CFLAGS+=-g3
 endif
else ifeq ($(CCTYPE),wcc)
 # W111: Meaningless use of an expression (case: "if(1) {<...>}")
 # W201: Unreachable code (case: "if(0) {<...>}")
 CFLAGS+=-ob -oe -oh -oi -ol+ -om -or -ot -wcd=111 -wcd=201 -we -wo -wx\
  -i=$(builddir)
 ifeq ($(DEBUG),1)
  CFLAGS+=-d3 -db -hd
 endif
endif

ifeq ($(LDTYPE),wlink)
 LFLAGS+=option quiet
endif

# $A=auto-generated target source files (kept on `clean')
# $T=all (except $A,$D) auto-generated temporary files (deleted on `clean')
# $S=all source files compiled to object files
# $D=all dependency files
# $O=target object files
# $B=target binary executable file

A=\
 $(builddir)/Defines.mak\
 $(builddir)/Defines.h\
 $(builddir)/Finetune.inc\
 $(builddir)/Periods.inc\
 $(builddir)/Semitone.inc\
 $(builddir)/Sinewave.inc
T=$(builddir)/gentab$(EH)
S=\
 amp.c\
 ampq.c\
 debug.c\
 errors.c\
 file.c\
 interp.c\
 interp2.c\
 interp2q.c\
 interpq.c\
 mem.c\
 mix.c\
 mixc.c\
 mixcq.c\
 mixl.c\
 mixlq.c\
 mixq.c\
 mod.c\
 play.c\
 song.c\
 tables.c\
 vol.c\
 volq.c\
 wav.c
D=$(foreach a,gentab.c $S modwave.c,$(builddir)/$(a:.c=.d))
O=$(foreach a,$S modwave.c,$(builddir)/$(a:.c=.o))
B=$(builddir)/modwave$E

.PHONY: all
all: $B

build \
$(builddir) \
$(DESTDIR)$(bindir):
	$(MKDIR) -p $@

# Dependency files

# $1=source C file, $2=extra target file (object or binary executable)
define source_dep_host=
$$(builddir)/$(1:.c=.d): $1 Makefile | $$(builddir)
 # HINT: `-MG' is needed to ignore missing `*.inc' files
	$$(CCH) $$(CFLAGSH) -MM -MG -MT "$$@ $2" $$< -MF $$@
endef

# $1=source C file, $2=extra target file (object or binary executable)
define source_dep=
$$(builddir)/$(1:.c=.d): $1 Makefile | $$(builddir)
 ifeq ($$(CCTYPE),gcc)
  # HINT: `-MG' is needed to ignore missing `*.inc' files
	$$(CC) $$(CFLAGS) -MM -MG -MT "$$@ $2" $$< -MF $$@
 else ifeq ($$(CCTYPE),wcc)
	$$(CC) $$(CFLAGS) -ad=$$@ $(foreach x,$$@ $2,-adt=$x)\
	 -fr=$$(@:.d=.err) -fo=$$(@:.d=.tmp) $$<
 else
  $$(error Missing rule for "$$(CCTYPE)" target C compiler)
 endif
endef

.PHONY: depend
depend: $A
	$(MAKE) $D

# Auto-generated shared source files

# $1 = output makefile
define make_Defines=
	{\
	 echo '# Local configuration file';\
	 $(foreach x,$F,echo '$x=$($x)';)\
	 } > $1
endef

Defines.mak: Makefile
	$(call make_Defines,$@)

$(builddir)/Defines.mak: Defines.mak Makefile | $(builddir)
	$(call make_Defines,$@)

$(builddir)/Version.mak: Version.mak Makefile | $(builddir)
	{\
	 echo '# Local configuration file';\
	 echo 'VERSION=$(VERSION)';\
	 } > $@

$(builddir)/Defines.h:\
 $(builddir)/Defines.mak\
 $(builddir)/Version.mak\
 Makefile | $(builddir)
	{\
	 echo '/* File is automatically generated. Do not modify it. */';\
	 echo '#pragma once';\
	 echo '#ifndef DEFINES_H';\
	 echo '#define DEFINES_H';\
	 echo '#define VERSION "$(VERSION)"';\
	 $(foreach x,$F,echo '#define $x $($x)';)\
	 echo '#endif /* !DEFINES_H */';\
	 } > $@

# Auto-generated tools

GENTAB=$(builddir)/gentab$(EH)

$(foreach a,gentab.c,$(eval $(call source_dep_host,$a,$(builddir)/$(a:.c=$(EH)))))

$(GENTAB): gentab.c Makefile | $(builddir)
 ifeq ($(CCTYPEH),gcc)
	$(CCH) $(CFLAGSH) -o $@ $< -lm
 else
  $(error Missing rule for "$(CCTYPEH)" host C compiler)
 endif

# Auto-generated source files

$(builddir)/Finetune.inc: $(GENTAB) Makefile | $(builddir)
	./$< finetune > $@

$(builddir)/Periods.inc: $(GENTAB) Makefile | $(builddir)
	./$< periods > $@

$(builddir)/Semitone.inc: $(GENTAB) Makefile | $(builddir)
	./$< semitones > $@

$(builddir)/Sinewave.inc: $(GENTAB) Makefile | $(builddir)
	./$< sinewave > $@

# Object files

# $1=source C file
define source_obj=
$$(builddir)/$(1:.c=.o): $1 Makefile | $$(builddir)
 ifeq ($$(CCTYPE),gcc)
	$$(CC) $$(CFLAGS) -c -o $$@ $$<
 else ifeq ($$(CCTYPE),wcc)
	$$(CC) $$(CFLAGS) -fo=$$@ $$<
 else
  $$(error Missing rule for "$$(CCTYPE)" target C compiler)
 endif
endef

# Binary executable

$(foreach a,$S,$(eval $(call source_dep,$a,$(builddir)/$(a:.c=.o))))
$(foreach a,modwave.c,$(eval $(call source_dep,$a,$(builddir)/$(a:.c=.o) $(builddir)/$(a:.c=$E))))

$(foreach a,$S modwave.c,$(eval $(call source_obj,$a)))

$(builddir)/modwave$E: $O Makefile | $(builddir)
 ifeq ($(LDTYPE),gcc)
	$(LD) $(LFLAGS) -o $@ $O
 else ifeq ($(LDTYPE),wlink)
	$(LD) $(LFLAGS) name $@ $(foreach x,$O,file $x)
 else
  $(error Missing rule for "$(LDTYPE)" target linker)
 endif

$(DESTDIR)$(bindir)/modwave$E: $(builddir)/modwave$E | $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) $< $@

.PHONY: install
install: $(DESTDIR)$(bindir)/modwave$E

.PHONY: install-strip
install-strip:
	$(MAKE) -w INSTALL_PROGRAM='$(INSTALL_PROGRAM) -s' install

.PHONY: uninstall
uninstall:
	$(RM) $(DESTDIR)$(bindir)/modwave$E

.PHONY: clean
clean:
	-$(RM) $D $T $O $B

.PHONY: distclean
distclean:
	-$(RM) Defines.mak
	-$(RM) -r $(builddir)

# Release

build/$(LINUX_I386):
	$(RM) $@
	$(MAKE) -w builddir=build/linux-i386 depend
	$(MAKE) -w builddir=build/linux-i386
	$(MAKE) -w\
	 builddir=build/linux-i386\
	 prefix=build/linux-i386\
	 install-strip
	cd build/linux-i386 && tar --owner= --group= -cjf ../$(@F)\
	 bin/modwave

build/$(LINUX_X86_64):
	$(RM) $@
	$(MAKE) -w builddir=build/linux-x86_64 depend
	$(MAKE) -w builddir=build/linux-x86_64
	$(MAKE) -w\
	 builddir=build/linux-x86_64\
	 prefix=build/linux-x86_64\
	 install-strip
	cd build/linux-x86_64 && tar --owner= --group= -cjf ../$(@F)\
	 bin/modwave

build/$(WINDOWS_I386):
	$(RM) $@
	$(MAKE) -w builddir=build/windows-i386 depend
	$(MAKE) -w builddir=build/windows-i386
	$(MAKE) -w\
	 builddir=build/windows-i386\
	 prefix=build/windows-i386\
	 install-strip
	cd build/windows-i386 && zip -r9 ../$(@F)\
	 bin/modwave.exe

build/$(WINDOWS_X86_64):
	$(RM) $@
	$(MAKE) -w builddir=build/windows-x86_64 depend
	$(MAKE) -w builddir=build/windows-x86_64
	$(MAKE) -w\
	 builddir=build/windows-x86_64\
	 prefix=build/windows-x86_64\
	 install-strip
	cd build/windows-x86_64 && zip -r9 ../$(@F)\
	 bin/modwave.exe

build/$(DOS_I086):
	$(RM) $@
	$(MAKE) -w builddir=build/dos-i086 depend
	$(MAKE) -w builddir=build/dos-i086
	$(MAKE) -w\
	 builddir=build/dos-i086\
	 prefix=build/dos-i086\
	 install
	cd build/dos-i086 && zip -r9 ../$(@F)\
	 bin/modwave.exe

build/$(DOS_I286):
	$(RM) $@
	$(MAKE) -w builddir=build/dos-i286 depend
	$(MAKE) -w builddir=build/dos-i286
	$(MAKE) -w\
	 builddir=build/dos-i286\
	 prefix=build/dos-i286\
	 install
	cd build/dos-i286 && zip -r9 ../$(@F)\
	 bin/modwave.exe

build/$(DOS_I386):
	$(RM) $@
	$(MAKE) -w builddir=build/dos-i386 depend
	$(MAKE) -w builddir=build/dos-i386
	$(MAKE) -w\
	 builddir=build/dos-i386\
	 prefix=build/dos-i386\
	 install
	cd build/dos-i386 && zip -r9 ../$(@F)\
	 bin/modwave.exe

build/$(DOS32A_I386):
	$(RM) $@
	$(MAKE) -w builddir=build/dos32a-i386 depend
	$(MAKE) -w builddir=build/dos32a-i386
	$(MAKE) -w\
	 builddir=build/dos32a-i386\
	 prefix=build/dos32a-i386\
	 install
	cd build/dos32a-i386 && zip -r9 ../$(@F)\
	 bin/modwave.exe

build/$(DOS4G_I386):
	$(RM) $@
	$(MAKE) -w builddir=build/dos4g-i386 depend
	$(MAKE) -w builddir=build/dos4g-i386
	$(MAKE) -w\
	 builddir=build/dos4g-i386\
	 prefix=build/dos4g-i386\
	 install
	cd build/dos4g-i386 && zip -r9 ../$(@F)\
	 bin/modwave.exe

.PHONY: release
release: | build
	echo -n '' >build/release.md
 ifneq ($(OS),Windows_NT)
	{\
	 echo '**GNU/Linux** on **Intel x86** platform:';\
	 echo '';\
	 echo 'Architecture | File | Size | SHA-256';\
	 echo '---|---|---|---';\
	 } >>build/release.md
	$(MAKE) BUILD=linux_i386 build/$(LINUX_I386)
	{\
	 echo -n '32-bits (i386) | $(LINUX_I386) | ';\
	 echo -n $$(numfmt --to=iec $$(stat -c '%s' build/$(LINUX_I386)));\
	 echo -n ' | ';\
	 sha256sum build/$(LINUX_I386) | cut -d\  -f 1;\
	 } >>build/release.md
	$(MAKE) BUILD=linux_x86_64 build/$(LINUX_X86_64)
	{\
	 echo -n '64-bits (AMD64) | $(LINUX_X86_64) | ';\
	 echo -n $$(numfmt --to=iec $$(stat -c '%s' build/$(LINUX_X86_64)));\
	 echo -n ' | ';\
	 sha256sum build/$(LINUX_X86_64) | cut -d\  -f 1;\
	 echo '';\
	 } >>build/release.md
 endif
	{\
	 echo '**Microsoft Windows** on **Intel x86** platform:';\
	 echo '';\
	 echo 'Architecture | File | Size | SHA-256';\
	 echo '---|---|---|---';\
	 } >>build/release.md
	$(MAKE) BUILD=windows_i386 build/$(WINDOWS_I386)
	{\
	 echo -n '32-bits (i386) | $(WINDOWS_I386) | ';\
	 echo -n $$(numfmt --to=iec $$(stat -c '%s' build/$(WINDOWS_I386)));\
	 echo -n ' | ';\
	 sha256sum build/$(WINDOWS_I386) | cut -d\  -f 1;\
	 } >>build/release.md
	$(MAKE) BUILD=windows_x86_64 build/$(WINDOWS_X86_64)
	{\
	 echo -n '64-bits (AMD64) | $(WINDOWS_X86_64) | ';\
	 echo -n $$(numfmt --to=iec $$(stat -c '%s' build/$(WINDOWS_X86_64)));\
	 echo -n ' | ';\
	 sha256sum build/$(WINDOWS_X86_64) | cut -d\  -f 1;\
	 echo '';\
	 } >>build/release.md
	{\
	 echo '**DOS** on **Intel x86** platform:';\
	 echo '';\
	 echo 'Architecture | File | Size | SHA-256';\
	 echo '---|---|---|---';\
	 } >>build/release.md
	$(MAKE) BUILD=dos_i086 build/$(DOS_I086)
	{\
	 echo -n '16-bits (i8086) | $(DOS_I086) | ';\
	 echo -n $$(numfmt --to=iec $$(stat -c '%s' build/$(DOS_I086)));\
	 echo -n ' | ';\
	 sha256sum build/$(DOS_I086) | cut -d\  -f 1;\
	 } >>build/release.md
	$(MAKE) BUILD=dos_i286 build/$(DOS_I286)
	{\
	 echo -n '16-bits (i286) | $(DOS_I286) | ';\
	 echo -n $$(numfmt --to=iec $$(stat -c '%s' build/$(DOS_I286)));\
	 echo -n ' | ';\
	 sha256sum build/$(DOS_I286) | cut -d\  -f 1;\
	 } >>build/release.md
	$(MAKE) BUILD=dos_i386 build/$(DOS_I386)
	{\
	 echo -n '16-bits (i386) | $(DOS_I386) | ';\
	 echo -n $$(numfmt --to=iec $$(stat -c '%s' build/$(DOS_I386)));\
	 echo -n ' | ';\
	 sha256sum build/$(DOS_I386) | cut -d\  -f 1;\
	 } >>build/release.md
	$(MAKE) BUILD=dos32a_i386 build/$(DOS32A_I386)
	{\
	 echo -n '32-bits DOS/32A (i386) | $(DOS32A_I386) | ';\
	 echo -n $$(numfmt --to=iec $$(stat -c '%s' build/$(DOS32A_I386)));\
	 echo -n ' | ';\
	 sha256sum build/$(DOS32A_I386) | cut -d\  -f 1;\
	 } >>build/release.md
	$(MAKE) BUILD=dos4g_i386 build/$(DOS4G_I386)
	{\
	 echo -n '32-bits DOS/4G (i386) | $(DOS4G_I386) | ';\
	 echo -n $$(numfmt --to=iec $$(stat -c '%s' build/$(DOS4G_I386)));\
	 echo -n ' | ';\
	 sha256sum build/$(DOS4G_I386) | cut -d\  -f 1;\
	 } >>build/release.md

# Include dependency files
ifeq ($(foreach a,$(MAKECMDGOALS),$(foreach b,clean distclean,$(if ifeq ($a,$b),y,))),)
$(foreach a,$D,$(eval include $a))
endif
