# modwave

.MOD file (Amiga music module) to .WAV file (Microsoft PCM Wave) command-line converter.

Features:

* 15 and 31 instruments Amiga modules with 1-32 channels support
* Sample size up to 128 KB
* Full 8 octaves note range
* Patterns packing
* 16 bits low and normal quality internal mixing
* No filter, linear and cubic Lagrange sample interpolation
* Precalculated interpolation tables
* Precalculated volume table *(optional)*
* Precalculated amplification table *(optional)*
* 4-192 kHz output sample rate
* Mono and stereo output
* 8 and 16 bits per channel output
* Written in ISO C99 (almost ANSI C with minor exceptions)
* Released into the public domain ([Unlicense](LICENSES/Unlicense.txt) for code, [CC0 1.0 Universal](LICENSES/CC0-1.0.txt) for images)

Have fun!

## Build

Type `./Build.sh` to build. Type `make clean` to clean.

## Usage

```
modwave [options] [--] INPUT OUTPUT
here:
  options  any combination of options (see below)
  --       stop parsing options and treat all arguments as filenames
  INPUT    input Amiga MOD music file
  OUTPUT   output Microsoft PCM Wave audio file
options [current value]:
  -h    show help and exit
  -a N  set Amiga base clock: 0=PAL,1=NTSC [0]
  -r N  set output rate in Hertz: 4000-192000 [44100]
  -c N  set output channels: 1,2 [2]
  -b N  set output bits: 8,16 [16]
  -i N  set sample interpolation: 0=no,1=linear,2=cubic [2]
  -v N  set output volume: 0-1024 (0=auto,64=100%) [0]
  -mb N set mixing buffer length: 1024-16384 [8192]
  -mq N set mixing quality: 0=low,1=normal [1]
  -vt N use volume table: 0=no,1=yes [1]
  -at N use amplification table: 0=no,1=yes [1]
```

# Authors

2022-2024 Ivan Tatarinov \<ivan-tat at ya dot ru\>

# References

* [REUSE SOFTWARE](https://reuse.software/) - a set of recommendations to make licensing your projects easier
* [The Software Package Data Exchange (SPDX)](https://spdx.dev/) - an open standard for communicating software bill of material information, including components, licenses, copyrights, and security references
* [GNU Operating System](https://www.gnu.org/)
* [GNU Standards](https://savannah.gnu.org/projects/gnustandards) ([package](https://pkgs.org/search/?q=gnu-standards)) - GNU coding and package maintenance standards
* [GNU Core Utilities](https://www.gnu.org/software/coreutils/) ([package](https://pkgs.org/search/?q=coreutils))
* [GNU Bash](https://www.gnu.org/software/bash/) ([package](https://pkgs.org/search/?q=bash)) - GNU Bourne Again SHell
* [GNU Compiler Collection](https://www.gnu.org/software/gcc/) ([package](https://pkgs.org/search/?q=gcc))
* [Cppcheck](https://sourceforge.net/p/cppcheck/wiki/Home/) ([package](https://pkgs.org/search/?q=cppcheck)) - a tool for static C/C++ code analysis
* [GNU Make](https://www.gnu.org/software/make/) ([package](https://pkgs.org/search/?q=make)) - utility for directing compilation
* [GNU Tar](https://www.gnu.org/software/tar/) ([package](https://pkgs.org/search/?q=tar)) - a utility to create tar archives, as well as various other kinds of manipulation
* [bzip2](https://sourceware.org/bzip2/) ([package](https://pkgs.org/search/?q=bzip2)) - a freely available, patent free, data compressor
* [Zip](https://infozip.sourceforge.net/Zip.html) ([package](https://pkgs.org/search/?q=zip)) - a compression and file packaging/archive utility
* [Cygwin](https://cygwin.com/) - a large collection of GNU and Open Source tools which provide functionality similar to a Linux distribution on Windows
* [MSYS2](https://www.msys2.org/) - a collection of tools and libraries providing you with an easy-to-use environment for building, installing and running native Windows software
* [MinGW](https://osdn.net/projects/mingw/) - Minimalist GNU for Windows
* [MinGW-w64](https://mingw-w64.org/) ([package](https://pkgs.org/search/?q=mingw-w64)) - an advancement of the original mingw.org project, created to support the GCC compiler on Windows systems
* [cmd](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/cmd) - command interpreter in Windows
* [Windows commands](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands)
* [FreeDOS](https://www.freedos.org/) - a complete, free, DOS-compatible operating system
* [GNU GnuDOS](https://www.gnu.org/software/gnudos/) - a library designed to help new users of the GNU system, who are coming from a DOS background
* [DJGPP](https://www.delorie.com/djgpp/) - DOS ports of many GNU development utilities
* [The Doszip Commander](https://sourceforge.net/projects/doszip/) - an LFN-aware TUI file manager (NC clone) with built-in Zip and UnZip for DOS and Windows
* [Open Watcom](https://www.openwatcom.org/) - an original optimizing C/C++ and assembler compilers for DOS
* [Open Watcom v2](https://github.com/open-watcom) - a fork of Open Watcom compilers for various platforms
* [DOS/32A Advanced DOS Extender (WebArchive)](https://web.archive.org/web/20220628021610/https://dos32a.narechk.net/index_en.html) - a free, open-source DOS Extender which can be used as a drop-in replacement for the popular DOS/4GW DOS Extender and compatibles
* [DOS/32A sources](https://github.com/amindlost/dos32a) - the source code of DOS/32A v9.12, released 2006-04-20
* [DOSEMU](http://www.dosemu.org/) ([package](https://pkgs.org/search/?q=dosemu)) - DOS emulator with LFN support
* [DOSBox](https://www.dosbox.com/) ([package](https://pkgs.org/search/?q=dosbox)) - DOS emulator with good hardware support
* [DOSBox Staging](https://dosbox-staging.github.io/) - a modern continuation of DOSBox with advanced features and current development practices
* [Protracker 2.3A Song/Module Format](http://coppershade.org/articles/More!/Topics/Protracker_File_Format/)
* [Various file format descriptions on Modland](https://ftp.modland.com/pub/documents/format_documentation/)
* [The Mod Archive](https://modarchive.org/) - a huge archive of MODs
* [Modland](https://www.exotica.org.uk/wiki/Modland) - a comprehensive, highly categorized demoscene archive. Great resource for songs in obscure file formats, or old versions of trackers
* [Mods Anthology CD-1](https://archive.org/details/cdrom-amiga-mods-anthology-1), [CD-2](https://archive.org/details/cdrom-amiga-mods-anthology-2), [CD-3](https://archive.org/details/cdrom-amiga-mods-anthology-3), [CD-4](https://archive.org/details/cdrom-amiga-mods-anthology-4) - 4 CD-ROMs with 18000+ mods, 1996 year
* [PT2 clone](https://16-bits.org/pt2.php) ([package](https://pkgs.org/search/?q=pt2-clone)) - a portable, free, open source, cross-platform ProTracker 2 clone
* [FT2 clone](https://16-bits.org/ft2.php) ([package](https://pkgs.org/search/?q=ft2-clone)) - a portable, free, open source, cross-platform Fast Tracker 2 clone
* [MilkyTracker](https://milkytracker.org/) ([package](https://pkgs.org/search/?q=milkytracker)) - a free, open source, multi-platform music application for creating .MOD and .XM module files inspired by Fast Tracker 2
* [SchismTracker](https://schismtracker.org/) ([package](https://pkgs.org/search/?q=schism)) - a free, open source, multi-platform ImpulseTracker clone aiming at providing the same look&feel
* [LMMS](https://lmms.io/) ([package](https://pkgs.org/search/?q=lmms)) - a free, open source, cross-platform alternative to popular (but commercial and closed-source) programs like FruityLoops, Cubase and Logic
* [FFmpeg](https://ffmpeg.org/) ([package](https://pkgs.org/search/?q=ffmpeg)) - a complete, cross-platform solution to record, convert and stream audio and video
* [Audacity](https://www.audacityteam.org/) ([package](https://pkgs.org/search/?q=audacity)) - a free, open source, cross-platform audio editor
* [Inkscape](https://inkscape.org) ([package](https://pkgs.org/search/?q=inkscape)) - a free, open source, cross-platform vector graphics editor
* [OptiPNG](https://optipng.sourceforge.net/) ([package](https://pkgs.org/search/?q=optipng)) - a free, open source, cross-platform advanced PNG (Portable Network Graphics) optimizer
