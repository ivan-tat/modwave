#!/bin/bash -e
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense
MODWAVE=build/modwave
MODDIR=modules
OUTDIR=output
run_ffmpeg() {
	local r=$1
	local c=$2
	local b=$3
	local f_name="$4"
	local f_mod="$MODDIR/$f_name.mod"
	local f="$OUTDIR/${f_name}_r${r}_c${c}_b${b}_ffmpeg"
	local opts="-ar $r -ac $c"
	case $b in
	8) opts="$opts -c:a pcm_u8";;
	16) opts="$opts -c:a pcm_s16le";;
	esac
	opts="$opts -f wav -y"
	echo "> ffmpeg -i \"$f_mod\" $opts \"$f.wav\""
	ffmpeg -i "$f_mod" $opts "$f.wav"
}
run_modwave() {
	local r=$1
	local c=$2
	local b=$3
	local a=$4
	local i=$5
	local v=$6
	local mq=$7
	local vt=$8
	local at=$9
	local f_name="${10}"
	local f_mod="$MODDIR/$f_name.mod"
	local f="$OUTDIR/${f_name}_r${r}_c${c}_b${b}_a${a}_i${i}_v${v}_mq${mq}_vt${vt}_at${at}"
	local opts="-r $r -c $c -b $b -a $a -i $i -v $v -mq $mq -vt $vt -at $at"
	if [[ $DEBUG -eq 0 ]]; then
		echo "> modwave $opts \"$f_mod\" \"$f.wav\""
		$MODWAVE $opts "$f_mod" "$f.wav"
		chmod 644 "$f.wav"
	else
		echo "> modwave $opts \"$f_mod\" \"$f.wav\" > \"$f.log\""
		$MODWAVE $opts "$f_mod" "$f.wav" > "$f.log"
		chmod 644 "$f.wav" "$f.log"
	fi
}
source Defines.mak
if [ $# -ne 1 ]; then
	cat<<-EOT
	Usage:
	  $0 NAME
	where:
	  NAME - filename w/o extension. Actual file is "$MODDIR/NAME.mod"
	Output directory is "$OUTDIR"
	EOT
	exit 1
fi
mkdir -p "$OUTDIR"
if false; then
	for r in 8000 44100; do
	for c in 1 2; do
	for b in 8 16; do
		run_ffmpeg $r $c $b "$1"
		for a in 0 1; do
		for i in 0 1 2; do
		for v in 0 64 128 256; do
		for mq in 0 1; do
		for vt in 0 1; do
		for at in 0 1; do
			run_modwave $r $c $b $a $i $v $mq $vt $at "$1"
		done
		done
		done
		done
		done
		done
	done
	done
	done
fi
if true; then
	for r in 44100; do
	for c in 2; do
	for b in 16; do
#		run_ffmpeg $r $c $b "$1"
		for mq in 0 1; do
		for vt in 0 1; do
		for at in 0 1; do
			run_modwave $r $c $b 0 2 0 $mq $vt $at "$1"
		done
		done
		done
	done
	done
	done
fi
echo 'Done.'
