/*
 * amp.c - low quality amplifier
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdlib.h> /* malloc(), free() */
#include "Defines.h"
#include "common.h"
#include "amp.h"
#include "play.h"
#include "song.h"

#define CLIP_MIN -0x8000
#define CLIP_MAX 0x7FFF

u8 (*Amptab)[256*SONG_CHANNELS_MAX]=NULL;

/*
 * m=CLIP_MIN
 * M=CLIP_MAX
 * amp=1->AMP_AT_MAX
 * x=0->256*SONG_CHANNELS_MAX-1
 * Amptab[x]=RANGE((x-128*SONG_CHANNELS_MAX)*amp/AMP_BASE,m,M)/256+128
 *
 * Returns `false' on success, `true' on error
 */
bool init_Amptab(u16 amp) {
  u8*t;
  i32 s=(i32)-128*SONG_CHANNELS_MAX*AMP_BASE*amp;
  u32 d=amp*AMP_BASE;
  u16 n=256*SONG_CHANNELS_MAX;
  if(!(Amptab=malloc(sizeof(*Amptab)))) return true;
  t=(u8*)Amptab;
  while(n) {
#ifdef __WATCOMC__
    if(s<CLIP_MIN*AMP_BASE) *t=(((i16)CLIP_MIN&~255)/256)+128;
#else /* !defined(__WATCOMC__) */
    if(s<CLIP_MIN*AMP_BASE) *t=((CLIP_MIN&~255)/256)+128;
#endif /* !defined(__WATCOMC__) */
    else if(s>CLIP_MAX*AMP_BASE) *t=((CLIP_MAX&~255)/256)+128;
    else *t=(((i32)s&~(256*AMP_BASE-1))/(256*AMP_BASE))+128;
    s+=d; t++; n--;
  }
  return false;
}

void free_Amptab() {
  if(Amptab) {
    free(Amptab);
    Amptab=NULL;
  }
}

void amplify_at(i16*i,u16 len,u8*o) {
  if(play.interp==2) {
    do {
      i16 s=*i+128*SONG_CHANNELS_MAX;
      /* check range */
      if(s<0) s=0;
      else if(s>256*SONG_CHANNELS_MAX-1) s=256*SONG_CHANNELS_MAX-1;
      *o=(*Amptab)[s];
      i++; o++; len--;
    } while(len);
  } else {
    /* assume -128*SONG_CHANNELS_MAX<=*i<=128*SONG_CHANNELS_MAX-1 */
    do {
      *o=(*Amptab)[*i+128*SONG_CHANNELS_MAX];
      i++; o++; len--;
    } while(len);
  }
}

i16 (*Amptab2)[256*SONG_CHANNELS_MAX]=NULL;

/*
 * m=CLIP_MIN
 * M=CLIP_MAX
 * amp=1->AMP_AT_MAX
 * x=0->256*SONG_CHANNELS_MAX-1
 * Amptab2[x]=RANGE((x-128*SONG_CHANNELS_MAX)*amp/AMP_BASE,m,M)
 *
 * Returns `false' on success, `true' on error
 */
bool init_Amptab2(u16 amp) {
  i16*t;
  i32 s=(i32)-128*SONG_CHANNELS_MAX*AMP_BASE*amp;
  u32 d=amp*AMP_BASE;
  u16 n=256*SONG_CHANNELS_MAX;
  if(!(Amptab2=malloc(sizeof(*Amptab2)))) return true;
  t=(i16*)Amptab2;
  while(n) {
#ifdef __WATCOMC__
    if(s<CLIP_MIN*AMP_BASE) *t=(i16)CLIP_MIN;
#else /* !defined(__WATCOMC__) */
    if(s<CLIP_MIN*AMP_BASE) *t=CLIP_MIN;
#endif /* !defined(__WATCOMC__) */
    else if(s>CLIP_MAX*AMP_BASE) *t=CLIP_MAX;
    else *t=((i32)s&~(AMP_BASE-1))/AMP_BASE;
    s+=d; t++; n--;
  }
  return false;
}

void free_Amptab2() {
  if(Amptab2) {
    free(Amptab2);
    Amptab2=NULL;
  }
}

void amplify2_at(i16*i,u16 len,i16*o) {
  if(play.interp==2) {
    do {
      i16 s=*i+128*SONG_CHANNELS_MAX;
      /* check range */
      if(s<0) s=0;
      else if(s>256*SONG_CHANNELS_MAX-1) s=256*SONG_CHANNELS_MAX-1;
      *o=(*Amptab2)[s];
      i++; o++; len--;
    } while(len);
  } else {
    /* assume -128*SONG_CHANNELS_MAX<=*i<=128*SONG_CHANNELS_MAX-1 */
    do {
      *o=(*Amptab2)[*i+128*SONG_CHANNELS_MAX];
      i++; o++; len--;
    } while(len);
  }
}

/* amp=1->AMP_MAX */
void amplify(i16*i,u16 len,u16 amp,u8*o) {
  i16 m=CLIP_MIN/amp,M=CLIP_MAX/amp;
  do {
#ifdef __WATCOMC__
    if(*i<m) *o=(((i16)CLIP_MIN&~255)/256)+128;
#else /* !defined(__WATCOMC__) */
    if(*i<m) *o=((CLIP_MIN&~255)/256)+128;
#endif /* !defined(__WATCOMC__) */
    else if(*i>M) *o=((CLIP_MAX&~255)/256)+128;
    else *o=((((i16)*i*amp)&~255)/256)+128;
    i++; o++; len--;
  } while(len);
}

/* amp=1->AMP_MAX */
void amplify2(i16*i,u16 len,u16 amp,i16*o) {
  i16 m=CLIP_MIN/amp,M=CLIP_MAX/amp;
  do {
#ifdef __WATCOMC__
    if(*i<m) *o=(i16)CLIP_MIN;
#else /* !defined(__WATCOMC__) */
    if(*i<m) *o=CLIP_MIN;
#endif /* !defined(__WATCOMC__) */
    else if(*i>M) *o=CLIP_MAX;
    else *o=(i16)*i*amp;
    i++; o++; len--;
  } while(len);
}
