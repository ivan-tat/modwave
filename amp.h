/*
 * amp.h - low quality amplifier
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef AMP_H
#define AMP_H

#include <stdbool.h>
#include "Defines.h"
#include "common.h"
#include "song.h"

#define AMP_SHIFT 6
#define AMP_BASE (1<<AMP_SHIFT)

/* amplification limit to fit in 32 bits variable for 16-bits mixing */
#define AMP_MAX ((1L<<16)/AMP_BASE)
#define AMP_AT_MAX ((1L<<24)/SONG_CHANNELS_MAX/AMP_BASE)

/*
 * len = total number of samples to amplify
 * amp = 1->AMP_MAX
 */

extern void amplify(i16*i,u16 len,u16 amp,u8*o);
extern void amplify2(i16*i,u16 len,u16 amp,i16*o);

/* amp = 1->AMP_AT_MAX */

extern u8 (*Amptab)[256*SONG_CHANNELS_MAX];
extern bool init_Amptab(u16 amp);
extern void free_Amptab();
extern void amplify_at(i16*i,u16 len,u8*o);

extern i16 (*Amptab2)[256*SONG_CHANNELS_MAX];
extern bool init_Amptab2(u16 amp);
extern void free_Amptab2();
extern void amplify2_at(i16*i,u16 len,i16*o);

#endif /* !AMP_H */
