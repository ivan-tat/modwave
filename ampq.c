/*
 * ampq.c - normal quality amplifier
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdlib.h> /* malloc(), free() */
#include <string.h>
#include "Defines.h"
#include "common.h"
#include "ampq.h"
#include "debug.h"
#include "play.h"
#include "song.h"

#define CLIP_MIN -0x8000
#define CLIP_MAX 0x7FFF

#define INPUT_MIN(amp) (i32)CLIP_MIN*AMP_Q_BASE/amp
#define INPUT_MAX(amp) (i32)CLIP_MAX*AMP_Q_BASE/amp

/*
 * i=0->255
 * Amptab_q[256*0+i]: offset (index) table: valid values are 256+(0->3)*512
 * Amptab_q[256*1+i]: MSB-based part for scale (amplification) table
 * Amptab_q[256*2+i]: LSB-based part for scale (amplification) table
 * Amptab_q[256*3+i]: MSB-based part for underflow table
 * Amptab_q[256*4+i]: LSB-based part for underflow table
 * Amptab_q[256*5+i]: MSB-based part for overflow table
 * Amptab_q[256*6+i]: LSB-based part for overflow table
 * Amptab_q[256*7+i]: MSB-based part for clip (min&max) table
 * Amptab_q[256*8+i]: LSB-based part for clip (min&max) table: always zero
 */
i16 (*Amptab_q)[256*9];

#define IOFS  256*0
#define IAMP  256*1
#define IAMP1 256*2
#define IUND  256*3
#define IUND1 256*4
#define IOVR  256*5
#define IOVR1 256*6
#define ICLP  256*7
#define ICLP1 256*8

#if DEBUG_AMPTAB

static const char*amppart[9]={
  "offsets",
  "scale_MSB",
  "scale_LSB",
  "underflow_MSB",
  "underflow_LSB",
  "overflow_MSB",
  "overflow_LSB",
  "clip_MSB",
  "clip_LSB"
};

static void DBG_DUMP_AMPTAB() {
  u16 o=0;
  i16 i;
  for(i=0;i<9;i++) {
    DBG_PRINTF("Amptab_q[0x%hX->0x%hX] (%s):"NL,
      (u16)o,(u16)o+255,(char*)amppart[i]);
    DBG_MEMDUMPW((i16*)Amptab_q+o,256*2);
    o+=256;
  }
}

static const char*DBG_AMPPART(i16 o) {
  if((o<0)||(o%256)||(o>=(i16)sizeof(*Amptab_q)/2)) return "?";
  else return amppart[o/256];
}

#else /* !DEBUG_AMPTAB */

# define DBG_DUMP_AMPTAB()
# define DBG_AMPPART(x) NULL

#endif /* !DEBUG_AMPTAB */

static void set_const(i16 o,u8 a,u8 b,i16 x) {
  i16*t=(i16*)Amptab_q+o+a;
  u16 n=b-a+1;
  if(DEBUG_AMPTAB) {
    DBG_ENTER();
    if(!o) {
      DBG_PRINTF("o=%hd(%hXh=%s),a=%hhd(%hhXh),b=%hhd(%hhXh),x=%hd(%hXh=%s)"NL,
        (i16)o,(i16)o,(char*)DBG_AMPPART(o),
        (i8)a,(i8)a,(i8)b,(i8)b,(i16)x,(i16)x,(char*)DBG_AMPPART(x));
    } else if(o&256) {
      DBG_PRINTF("o=%hd(%hXh=%s),a=%hhd(%hhXh),b=%hhd(%hhXh),x=%hd(%hXh)"NL,
        (i16)o,(i16)o,(char*)DBG_AMPPART(o),
        (i8)a,(i8)a,(i8)b,(i8)b,(i16)x,(i16)x);
    } else {
      DBG_PRINTF("o=%hd(%hXh=%s),a=%hhu(%hhXh),b=%hhu(%hhXh),x=%hd(%hXh)"NL,
        (i16)o,(i16)o,(char*)DBG_AMPPART(o),
        (u8)a,(u8)a,(u8)b,(u8)b,(i16)x,(i16)x);
    }
  }
  while(n) { *t=x; t++; n--; }
  if(DEBUG_AMPTAB) { DBG_LEAVE(); }
}

static void set_amp_pos(i16 o,u8 a,u16 amp) {
  i16*t=(i16*)Amptab_q+o;
  i32 d=(i32)amp*256,s=0;
  u16 n=a+1;
  if(DEBUG_AMPTAB) {
    DBG_ENTER();
    DBG_PRINTF("o=%hd(%hXh=%s),a=%hhd(%hhXh),amp=%hu(%hXh)"NL,
      (i16)o,(i16)o,(char*)DBG_AMPPART(o),(i8)a,(i8)a,(u16)amp,(u16)amp);
  }
  while(n) { *t=s/AMP_Q_BASE; s+=d; t++; n--; }
  if(DEBUG_AMPTAB) { DBG_LEAVE(); }
}

static void set_amp_neg(i16 o,i8 a,u16 amp) {
  i16*t=(i16*)Amptab_q+o+256+a;
  i32 d=(i32)amp*256,s=d*a;
  u16 n=-a;
  if(DEBUG_AMPTAB) {
    DBG_ENTER();
    DBG_PRINTF("o=%hd(%hXh=%s),a=%hhd(%hhXh),amp=%hu(%hXh)"NL,
      (i16)o,(i16)o,(char*)DBG_AMPPART(o),(i8)a,(i8)a,(u16)amp,(u16)amp);
  }
  while(n) { *t=s/AMP_Q_BASE; s+=d; t++; n--; }
  if(DEBUG_AMPTAB) { DBG_LEAVE(); }
}

static void set_amp_int(i16 o,u8 a,u8 b,u16 amp) {
  i16*t=(i16*)Amptab_q+o+a;
  u32 s=a?(u32)amp*a:0;
  u16 n=b-a+1;
  if(DEBUG_AMPTAB) {
    DBG_ENTER();
    DBG_PRINTF(
      "o=%hd(%hXh=%s),a=%hhu(%hhXh),b=%hhu(%hhXh),amp=%hu(%hXh)"NL,
      (i16)o,(i16)o,(char*)DBG_AMPPART(o),
      (u8)a,(u8)a,(u8)b,(u8)b,(u16)amp,(u16)amp);
  }
  while(n) { *t=s/AMP_Q_BASE; s+=amp; t++; n--; }
  if(DEBUG_AMPTAB) { DBG_LEAVE(); }
}

/*
 * amp=1->AMP_Q_MAX
 *
 * Returns `false' on success, `true' on error
 */
bool init_Amptab_q(u16 amp) {
  if(DEBUG_AMPTAB) { DBG_ENTER(); }
  if(!(Amptab_q=malloc(sizeof(*Amptab_q)))) return true;
  if(DEBUG_AMPTAB) memset(Amptab_q,0xCC,sizeof(*Amptab_q));
  if(amp<=AMP_Q_BASE) {
    set_const(IOFS,0,255,IAMP);
    set_amp_pos(IAMP,127,amp);
    set_amp_neg(IAMP,-128,amp);
    set_amp_int(IAMP1,0,255,amp);
  } else {
    i16 a=INPUT_MIN(amp);
    i16 b=INPUT_MAX(amp);
    u8 al=a&255;
    u8 bl=b&255;
    i8 ah=((u16)a&~255)/256;
    i8 bh=((u16)b&~255)/256;
    i16 i,j;
    if(DEBUG_AMPTAB) {
      DBG_PRINTF("a=%6hd(%4hXh),ah=%4hhd(%2hhXh),al=%3hhu(%2hhXh)"NL,
        (i16)a,(i16)a,(i8)ah,(i8)ah,(u8)al,(u8)al);
      DBG_PRINTF("b=%6hd(%4hXh),bh=%4hhd(%2hhXh),bl=%3hhu(%2hhXh)"NL,
        (i16)b,(i16)b,(i8)bh,(i8)bh,(u8)bl,(u8)bl);
    }
    i=ah;
    if(al>0) {
      i16 base=(a&~255)*amp/AMP_Q_BASE;
      if(DEBUG_AMPTAB) {
        DBG_PRINTF("underflow: i=%hhd(%hhXh),base=%hd(%hXh)"NL,
          (i8)i,(i8)i,(i16)base,(i16)base);
      }
      (*Amptab_q)[256+i]=IUND;
      (*Amptab_q)[IUND+256+i]=base;
      set_const(IUND1,0,al-1,CLIP_MIN-base);
      set_amp_int(IUND1,al,255,amp);
      i++;
    }
    if(i<0) {
      set_const(IOFS,256+i,255,IAMP);
      set_amp_neg(IAMP,i,amp);
    }
    j=bh;
    if(bl<255) {
      i16 base=(b&~255)*amp/AMP_Q_BASE;
      if(DEBUG_AMPTAB) {
        DBG_PRINTF("overflow: j=%hhd(%hhXh),base=%hd(%hXh)"NL,
          (i8)j,(u8)j,(i16)base,(i16)base);
      }
      (*Amptab_q)[j]=IOVR;
      (*Amptab_q)[IOVR+j]=base;
      set_amp_int(IOVR1,0,bl,amp);
      set_const(IOVR1,bl+1,255,CLIP_MAX-base);
      j--;
    }
    if(j>=0) {
      set_const(IOFS,0,j,IAMP);
      set_amp_pos(IAMP,j,amp);
    }
    if(i<=j) set_amp_int(IAMP1,0,255,amp);
    i=ah-1;
    if(i>=-128) {
      set_const(IOFS,128,256+i,ICLP);
#ifdef __WATCOMC__
      set_const(ICLP,128,256+i,(i16)CLIP_MIN);
#else /* !defined(__WATCOMC__) */
      set_const(ICLP,128,256+i,CLIP_MIN);
#endif /* !defined(__WATCOMC__) */
    }
    j=bh+1;
    if(j<=127) {
      set_const(IOFS,j,127,ICLP);
      set_const(ICLP,j,127,CLIP_MAX);
    }
    if((i>=-128)||(j<=127)) set_const(ICLP1,0,255,0);
  }
  if(DEBUG_AMPTAB) {
    DBG_DUMP_AMPTAB();
    DBG_LEAVE();
  }
  return false;
}

void free_Amptab_q() {
  if(Amptab_q) {
    free(Amptab_q);
    Amptab_q=NULL;
  }
}

void amplify_at_q(i16*i,u16 len,u8*o) {
  do {
    #pragma pack(push,1)
    union { i16 iw; u8 b[2]; } s;
    #pragma pack(pop)
    u16 ofs;
    s.iw=*i;
    ofs=(*Amptab_q)[s.b[1]];
    *o=((((*Amptab_q)[ofs+s.b[1]]+(*Amptab_q)[ofs+256+s.b[0]])&~255)/256)+128;
    i++; o++; len--;
  } while(len);
}

void amplify2_at_q(i16*i,u16 len,i16*o) {
  do {
    #pragma pack(push,1)
    union { i16 iw; u8 b[2]; } s;
    #pragma pack(pop)
    u16 ofs;
    s.iw=*i;
    ofs=(*Amptab_q)[s.b[1]];
    *o=(*Amptab_q)[ofs+s.b[1]]+(*Amptab_q)[ofs+256+s.b[0]];
    i++; o++; len--;
  } while(len);
}

/* amp=1->AMP_Q_MAX */
void amplify_q(i16*i,u16 len,u16 amp,u8*o) {
  if(amp<AMP_Q_BASE)
    do {
      *o=((((((i32)*i*amp)&~(AMP_Q_BASE-1))/AMP_Q_BASE)&~255)/256)+128;
      i++; o++; len--;
    } while(len);
  else if(amp>AMP_Q_BASE) {
    i16 a=INPUT_MIN(amp);
    i16 b=INPUT_MAX(amp);
    do {
#ifdef __WATCOMC__
      if(*i<a) *o=(((i16)CLIP_MIN&~255)/256)+128;
#else /* !defined(__WATCOMC__) */
      if(*i<a) *o=((CLIP_MIN&~255)/256)+128;
#endif /* !defined(__WATCOMC__) */
      else if(*i>b) *o=((CLIP_MAX&~255)/256)+128;
      else *o=((((i32)*i*amp)&~(256*AMP_Q_BASE-1))/(256*AMP_Q_BASE))+128;
      i++; o++; len--;
    } while(len);
  } else {
    do {
      *o=(((i16)*i&~255)/256)+128;
      i++; o++; len--;
    } while(len);
  }
}

/* amp=1->AMP_Q_MAX */
void amplify2_q(i16*i,u16 len,u16 amp,i16*o) {
  if(amp<AMP_Q_BASE)
    do {
      *o=(((i32)*i*amp)&~(AMP_Q_BASE-1))/AMP_Q_BASE;
      i++; o++; len--;
    } while(len);
  else if(amp>AMP_Q_BASE) {
    i16 a=INPUT_MIN(amp);
    i16 b=INPUT_MAX(amp);
    do {
#ifdef __WATCOMC__
      if(*i<a) *o=(i16)CLIP_MIN;
#else /* !defined(__WATCOMC__) */
      if(*i<a) *o=CLIP_MIN;
#endif /* !defined(__WATCOMC__) */
      else if(*i>b) *o=CLIP_MAX;
      else *o=(((i32)*i*amp)&~(AMP_Q_BASE-1))/AMP_Q_BASE;
      i++; o++; len--;
    } while(len);
  }
}
