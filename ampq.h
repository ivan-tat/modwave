/*
 * ampq.h - normal quality amplifier
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef AMPQ_H
#define AMPQ_H

#include <stdbool.h>
#include "Defines.h"
#include "common.h"
#include "song.h"

#define AMP_Q_SHIFT 6
#define AMP_Q_BASE (1<<AMP_Q_SHIFT)

/* amplification limit to fit in 32 bits variable for 16-bits mixing */
#define AMP_Q_MAX ((1L<<16)/AMP_Q_BASE)

/*
 * len = total number of samples to amplify
 * amp = 1->AMP_Q_MAX
 */

extern void amplify_q(i16*i,u16 len,u16 amp,u8*o);
extern void amplify2_q(i16*i,u16 len,u16 amp,i16*o);

extern i16 (*Amptab_q)[256*9];
extern bool init_Amptab_q(u16 amp);
extern void free_Amptab_q();
extern void amplify_at_q(i16*i,u16 len,u8*o);
extern void amplify2_at_q(i16*i,u16 len,i16*o);

#endif /* !AMPQ_H */
