/*
 * common.h - common defines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include "Defines.h"

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define MK_U32(a,b,c,d) (a+((u16)b<<8)+((u32)c<<16)+((u32)d<<24))
#define SWAPU16(x) ((((u16)x&0xFF)<<8)+((u16)x>>8))
#define MIN(a,b) (((a)<=(b))?a:b)
#define MAX(a,b) (((a)<=(b))?b:a)

#ifndef NL
# define NL "\n"
#endif /* !NL */

#ifdef __GNUC__
# define UNUSED_PARAM __attribute__((unused))
#else /* !defined(__GNUC__) */
# define UNUSED_PARAM
#endif /* !defined(__GNUC__) */

#ifdef __WATCOMC__
# define UNUSED_HINT(x) (void)x
#else /* !defined(__WATCOMC__) */
# define UNUSED_HINT(x)
#endif /* !defined(__WATCOMC__) */

#endif /* !COMMON_H */
