/*
 * debug.c - debug routines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

/* DEBUG=1 */

#include <stdarg.h> /* va_list, va_start(), va_arg(), va_end() */
#include <stdbool.h>
#include <stdio.h> /* printf(), vprintf() */
#include <string.h> /* strlen(), strcmp() */
#include "common.h" /* NL, MIN() */
#include "debug.h"

#define DEPTH 32

static int _depth=0;
static const char*_stack[DEPTH];
static bool _nl=true;

static void _show_trace() {
  if(_depth) {
    int i,n=MIN(_depth,DEPTH);
    for(i=0;i<n;i++) printf(i?":%s()":"%s()",_stack[i]);
    if(_depth>=DEPTH) printf(":...");
    printf(": ");
  }
}

static bool _check_nl(const char*s) {
  int l,n;
  l=strlen(s);
  n=strlen(NL);
  if(l<n) return false; else return (strcmp(s+l-n,NL)==0)?true:false;
}

void _dbg_printf(const char*fmt,...) {
  va_list ap;
  va_start(ap,fmt);
  if(_nl) _show_trace();
  vprintf(fmt,ap);
  va_end(ap);
  if(strcmp(fmt,"%s")==0) {
    va_start(ap,fmt);
    _nl=_check_nl((const char*)va_arg(ap,char*));
    va_end(ap);
  } else
    _nl=_check_nl(fmt);
}

void _dbg_enter(const char*name) {
  if(_depth<DEPTH) _stack[_depth]=name;
  _depth++;
  _nl=true;
  _dbg_printf("enter"NL);
}

void _dbg_leave(UNUSED_PARAM const char*name) {
  UNUSED_HINT(name);
  _nl=true;
  _dbg_printf("leave"NL);
  _depth--;
}

#if DEBUG
void _dbg_memdump(void*p,u32 sz) {
#else /* !DEBUG */
void _dbg_memdump(UNUSED_PARAM void*p,u32 sz) {
#endif /* !DEBUG */
  u32 o=0;
  bool nl=false;
  UNUSED_HINT(p);
  while(o<sz) {
    nl=false;
    if((o&15)==0) { _dbg_printf("%05X",(u32)o); }
    _dbg_printf(" %02hhX",(u8)*((u8*)p+o));
    o++;
    if((o&15)==0) {
      _dbg_printf(NL);
      nl=true;
    }
  }
  if(sz&&!nl) { _dbg_printf(NL); }
}

#if DEBUG
void _dbg_memdumpw(void*p,u32 sz) {
#else /* !DEBUG */
void _dbg_memdumpw(UNUSED_PARAM void*p,u32 sz) {
#endif /* !DEBUG */
  u32 o=0;
  bool nl=false;
  UNUSED_HINT(p); /* HINT: MAY not be used */
  while(o<sz) {
    nl=false;
    if((o&15)==0) { _dbg_printf("%05X[%05X]",(u32)o,(u32)o/2); }
    if(o+1<sz) {
      _dbg_printf(" %05X",(u32)*((u32*)p+o/2));
      o+=2;
    } else {
      _dbg_printf(" %02hhX",(u8)*((u8*)p+o));
      o++;
    }
    if((o&15)==0) {
      _dbg_printf(NL);
      nl=true;
    }
  }
  if(sz&&!nl) { _dbg_printf(NL); }
}
