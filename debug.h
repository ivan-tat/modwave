/*
 * debug.h - debug routines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

/* DEBUG: any */

#pragma once

#ifndef DEBUG_H
#define DEBUG_H

#include "Defines.h"

#if DEBUG

#include <stdarg.h> /* __VA_ARGS__ */
#include "common.h"

extern void _dbg_printf(const char*fmt,...);
extern void _dbg_enter(const char*name);
extern void _dbg_leave();
extern void _dbg_memdump(void*p,u32 sz);
extern void _dbg_memdumpw(void*p,u32 sz);

#define DBG_PRINTF(f,...) _dbg_printf(f,__VA_ARGS__)
#define DBG_LOG(s) _dbg_printf("%s",s)
#define DBG_ENTER() _dbg_enter(__func__)
#define DBG_LEAVE() _dbg_leave()
#define DBG_MEMDUMP(p,sz) _dbg_memdump(p,sz)
#define DBG_MEMDUMPW(p,sz) _dbg_memdumpw(p,sz)

#else /* !DEBUG */

#define DBG_PRINTF(f,...)
#define DBG_LOG(s)
#define DBG_ENTER()
#define DBG_LEAVE()
#define DBG_MEMDUMP(p,sz)
#define DBG_MEMDUMPW(p,sz)

#endif /* !DEBUG */

#endif /* !DEBUG_H */
