/*
 * errors.c - output of error messages
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdarg.h> /* va_list, va_start(), va_end() */
#include <stdio.h> /* fprintf(), vfprintf() */
#include "common.h"
#include "debug.h"
#include "errors.h"

static const struct _err_t {
  ERR err;
  const char*msg;
} _Errors[]={
  { E_MALLOC,"Memory allocation failed" },
  { E_FOPEN,"Failed to open a file" },
  { E_FCREATE,"Failed to create a file" },
  { E_FREAD,"Failed to read from a file" },
  { E_FWRITE,"Failed to write to a file" },
  { E_FTYPE,"Unknown file type" },
  { E_NOPATS,"No patterns" },
  { E_ORDLEN,"Bad order length" },
  { E_BADORD,"Order of patterns has bad index" },
  { E_OK,"Unknown error" } /* stop marker */
};

const char*get_error(ERR err) {
  const struct _err_t*e=_Errors;
  while((e->err!=err)&&(e->err!=E_OK)) e++;
  return e->msg;
}

#if DEBUG

void DBG_DUMP_ERR(ERR err) {
  DBG_PRINTF("err=%hhu (%s)"NL,
    (u8)err,(char*)((err==E_OK)?"OK":get_error(err)));
}

#endif /* !DEBUG */

void error(const char*fmt,...) {
  va_list ap;
  fprintf(stderr,"Error: ");
  va_start(ap,fmt);
  vfprintf(stderr,fmt,ap);
  va_end(ap);
  fprintf(stderr,NL);
}
