/*
 * errors.h - output of error messages
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef ERRORS_H
#define ERRORS_H

#include "common.h"

typedef u8 ERR;

#define E_OK      0   /* success */
#define E_MALLOC  1   /* memory allocation failed */
#define E_FOPEN   2   /* file open failed */
#define E_FCREATE 3   /* file create failed */
#define E_FREAD   4   /* file read failed */
#define E_FWRITE  5   /* file write failed */
#define E_FTYPE   6   /* mod: unsupported file type */
#define E_NOPATS  7   /* mod: order of patterns is empty */
#define E_ORDLEN  8   /* mod: bad order length */
#define E_BADORD  9   /* mod: order of patterns has invalid pattern index */

extern const char*get_error(ERR err); /* call only if err!=E_OK */

#if DEBUG

extern void DBG_DUMP_ERR(ERR err);

#else /* !DEBUG */

# define DBG_DUMP_ERR(err)

#endif /* !DEBUG */

extern void error(const char*fmt,...);

#endif /* !ERRORS_H */
