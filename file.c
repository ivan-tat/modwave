/*
 * file.c - file-related routines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include "debug.h"
#include "file.h"

/****************************************************************************/

#if DEBUG

void DBG_DUMP_FILENAME(const char*s) {
  DBG_PRINTF("filename=%s"NL,(const char*)s);
}

void DBG_OPEN_ERR(const char*s,int fl) {
  DBG_PRINTF("open() error: filename=\"%s\", flags=%d"NL,(char*)s,(int)fl);
}

void DBG_LSEEK_ERR(int fd,off_t req,int wh,off_t got) {
  DBG_PRINTF("lseek() error: fd=%d, requested=0x%lX, whence=",
    (int)fd,(long)req);
  switch(wh) {
  case SEEK_SET: DBG_LOG("SEEK_SET"); break;
  case SEEK_CUR: DBG_LOG("SEEK_CUR"); break;
  case SEEK_END: DBG_LOG("SEEK_END"); break;
  default: DBG_PRINTF("%d",(int)wh); break;
  }
  DBG_PRINTF(", got=0x%lX"NL,(long)got);
}

void DBG_READ_ERR(int fd,long req,long got) {
  DBG_PRINTF("read() error: fd=%d, requested=0x%lX, got=0x%lX"NL,
    (int)fd,(long)req,(long)got);
}

#endif /* !DEBUG */

/****************************************************************************/

bool _Open(const char*filename,int flags,int*fd) {
  bool result=false;

  if(DEBUG_LOAD) { DBG_ENTER(); }

  if((*fd=open(filename,flags))==-1) {
    DBG_OPEN_ERR(filename,flags);
    result=true;
  } else {
    DBG_PRINTF("opened \"%s\", flags=%d, fd=%d"NL,
      (char*)filename,(int)flags,(int)*fd);
  }

  if(DEBUG_LOAD) { DBG_LEAVE(); }

  return result;
}

/****************************************************************************/

void _Close(int*fd) {
  if(DEBUG_LOAD) { DBG_ENTER(); }

  close(*fd);
  if(DEBUG_LOAD) { DBG_PRINTF("closed fd=%d"NL,(int)*fd); }
  *fd=-1;

  if(DEBUG_LOAD) { DBG_LEAVE(); }
}

/****************************************************************************/

bool _Lseek(int fd,off_t offset,int whence) {
  off_t cur,off;

  switch(whence) {
  case SEEK_SET:
    cur=lseek(fd,offset,SEEK_SET);
    if((cur==-1)||(cur!=offset)) {
      if(DEBUG_LOAD) { DBG_LSEEK_ERR(fd,offset,SEEK_SET,cur); }
      return true;
    }
    break;
  case SEEK_CUR:
    cur=lseek(fd,0,SEEK_CUR);
    if(cur==-1) {
      if(DEBUG_LOAD) { DBG_LSEEK_ERR(fd,0,SEEK_CUR,cur); }
      return true;
    }
    off=lseek(fd,offset,SEEK_CUR);
    if((off==-1)||(cur+offset!=off)) {
      if(DEBUG_LOAD) { DBG_LSEEK_ERR(fd,offset,SEEK_CUR,off); }
      return true;
    }
    break;
  default:
    return true;
  }

  return false;
}

/****************************************************************************/

bool _Read(int fd,void*buf,size_t count) {
  ssize_t sz=read(fd,buf,count);

  if((long unsigned int)sz!=count) {
    if(DEBUG_LOAD) { DBG_READ_ERR(fd,count,sz); }
    return true;
  }

  return false;
}
