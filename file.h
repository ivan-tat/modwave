/*
 * file.h - file-related routines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef FILE_H
#define FILE_H

#include <stdbool.h>
#include "Defines.h"
#include "syscalls.h"

#if DEBUG

extern void DBG_DUMP_FILENAME(const char*s);
extern void DBG_OPEN_ERR(const char*s,int fl);
extern void DBG_LSEEK_ERR(int fd,off_t req,int wh,off_t got);
extern void DBG_READ_ERR(int fd,long req,long got);

#else /* !DEBUG */

# define DBG_DUMP_FILENAME(s)
# define DBG_OPEN_ERR(s,fl)
# define DBG_LSEEK_ERR(fd,req,wh,got)
# define DBG_READ_ERR(fd,req,got)

#endif /* !DEBUG */

extern bool _Open(const char*filename,int flags,int*fd);
extern void _Close(int*fd);

/* Returns `true' on error */
extern bool _Lseek(int fd,off_t offset,int whence);

/* Returns `true' on error */
extern bool _Read(int fd,void*buf,size_t count);

#endif /* !FILE_H */
