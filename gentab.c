/*
 * gentab.c - static tables generator
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h" /* NL */

#ifndef M_PI
# define M_PI 3.14159265358979323846
#endif /* !M_PI */

#define SEMITONES 12
#define OCTAVES 8
#define PERIOD_BASE 428*4         /* Amiga's base period*4 */
#define PERIOD_MAX (PERIOD_BASE<<4) /* 4 octaves down */
#define PERIOD_MIN (PERIOD_BASE>>3) /* 3 octaves up,not used here */

#define padding "  "

typedef char syntax_t;
#define SYNTAX_ASM 0
#define SYNTAX_C 1

static syntax_t opt_syntax=SYNTAX_C;

static const char
  *FileComment="File is automatically generated. Do not modify it.",
  *FileType="SOURCE",
  *FileCopyrightText="2022, 2023 Ivan Tatarinov",
  *FileLicenseIdentifier="Unlicense",
  *SPDXFileType="SPDX-FileType",
  *SPDXFileCopyrightText="SPDX-FileCopyrightText",
  *SPDXLicenseIdentifier="SPDX-License-Identifier";

static void error(const char*fmt,...) {
  va_list ap;
  fprintf(stderr,"Error: ");
  va_start(ap,fmt);
  vfprintf(stderr,fmt,ap);
  va_end(ap);
  fprintf(stderr,NL);
}

static void header() {
  const char*fmt;
  switch(opt_syntax) {
  default:
  case SYNTAX_ASM:
    fmt=
      "; %s"NL
      ";"NL
      "; %s: %s"NL
      "; %s: %s"NL
      "; %s: %s"NL
      NL;
    break;
  case SYNTAX_C:
    fmt=
      "/*"NL
      " * %s"NL
      " *"NL
      " * %s: %s"NL
      " * %s: %s"NL
      " * %s: %s"NL
      " */"NL
      NL;
    break;
  }
  printf(
    fmt,
    (char*)FileComment,
    (char*)SPDXFileType,(char*)FileType,
    (char*)SPDXFileCopyrightText,(char*)FileCopyrightText,
    (char*)SPDXLicenseIdentifier,(char*)FileLicenseIdentifier);
}

static void dump_tab(const int*tab,int size,int columns,const char*prefix,
bool comma,bool lastcomma) {
  const char*eos=comma?",":"";
  int n=0;
  bool last;
  do {
    int i=0;
    do {
      const char*s;
      last=(n==(size-1));
      s=last?((comma&&lastcomma)?eos:""):((n+1)%columns)?",":eos;
      printf("%s%d%s",(char*)(i?"":prefix),(int)tab[n],(char*)s);
      n++; i++;
    } while((!last)&&(i<columns));
    printf(NL);
  } while(!last);
}

static void gen_periods() {
  const char*prefix;
  bool comma;
  int tab[OCTAVES*SEMITONES],i,ft;
  switch(opt_syntax) {
  default:
  case SYNTAX_C: prefix=padding; comma=true; break;
  case SYNTAX_ASM: prefix=padding "dw "; comma=false; break;
  }
  for(ft=0;ft<=15;ft++) {
    float d=(float)((ft<=7)?ft:ft-16)/8;
    for(i=0;i<OCTAVES*SEMITONES;i++)
      tab[i]=(PERIOD_MAX*32/pow(2,(((float)i+d)/SEMITONES))+16)/32;
    dump_tab(tab,SEMITONES*OCTAVES,SEMITONES,prefix,comma,ft<15);
  }
}

/* format: unsigned fixed point 1.15 */
static void gen_finetune() {
  const char*prefix;
  bool comma;
  int tab[16],i;
  switch(opt_syntax) {
  default:
  case SYNTAX_C: prefix=padding; comma=true; break;
  case SYNTAX_ASM: prefix=padding "dw "; comma=false; break;
  }
  for(i=0;i<=15;i++)
    tab[i]=(32768/pow(2,(((float)((i^8)-8)/8)/SEMITONES)));
  dump_tab(tab,16,8,prefix,comma,false);
}

/* format: unsigned fixed point 1.16 minus 1 to fit in 16 bits */
static void gen_semitones() {
  const char*prefix;
  bool comma;
  int tab[16],i;
  switch(opt_syntax) {
  default:
  case SYNTAX_C: prefix=padding; comma=true; break;
  case SYNTAX_ASM: prefix=padding "dw "; comma=false; break;
  }
  for(i=0;i<16;i++) tab[i]=65536/pow(2,((float)i/SEMITONES))-1;
  dump_tab(tab,16,SEMITONES,prefix,comma,false);
}

/* format: signed char, range: -32..32 */
static void gen_sinewave() {
  const char*prefix;
  bool comma;
  int tab[64],i;
  switch(opt_syntax) {
  default:
  case SYNTAX_C: prefix=padding; comma=true; break;
  case SYNTAX_ASM: prefix=padding "db "; comma=false; break;
  }
  for(i=0;i<64;i++) tab[i]=32*sin(((float)i*M_PI/32));
  dump_tab(tab,64,16,prefix,comma,false);
}

/* format: unsigned fixed point 1.15 */
static void gen_vibrato() {
  const char*prefix;
  bool comma;
  int tab[65],i;
  switch(opt_syntax) {
  default:
  case SYNTAX_C: prefix=padding; comma=true; break;
  case SYNTAX_ASM: prefix=padding "dw "; comma=false; break;
  }
  for(i=0;i<65;i++) tab[i]=32768*pow(2,((float)(i-32)/32)/SEMITONES);
  dump_tab(&tab[0],32,8,prefix,comma,true);
  dump_tab(&tab[32],1,8,prefix,comma,true);
  dump_tab(&tab[33],32,8,prefix,comma,false);
}

static const struct tab_t {
  char*name,*comment;
  void (*gen)(); /* table generator */
} tables[]={
  { "periods","note periods multiplied by fine-tune value",gen_periods },
  { "semitones","semitones coefficients",gen_semitones },
  { "finetune","fine-tune coefficients",gen_finetune },
  { "sinewave","sine wave",gen_sinewave },
  { "vibrato","note periods coefficients for vibrato effect",gen_vibrato },
  { NULL,NULL,NULL } /* stop marker */
};

static void show_usage(const char*argv0) {
  const struct tab_t*t=tables;
  printf(
    "Usage:"NL
    "  %s TABLE [SYNTAX]"NL
    "where:"NL
    "  TABLE - name of the table:"NL,
    argv0);
  do {
    printf(
      "    `%s' - %s"NL,
      (char*)t->name,
      (char*)t->comment);
    t++;
  } while(t->name);
  printf(
    "  SYNTAX - output syntax: `asm' or `c'. Default is `c'."NL);
}

static bool gen_tab(const char*s) {
  const struct tab_t*t=tables;
  do {
    if(!strcmp(s,t->name)) { (*(t->gen))(); return false; }
    t++;
  } while(t->name);
  return true;
}

int main(int argc,char**argv) {
  if((argc!=2)&&(argc!=3)) { show_usage(argv[0]); return 0; }
  if(argc==3) {
    if(!strcmp(argv[2],"asm")) opt_syntax=SYNTAX_ASM;
    else if(!strcmp(argv[2],"c")) opt_syntax=SYNTAX_C;
    else {
      error("Bad syntax specified");
      show_usage(argv[0]);
      return 1;
    }
  }
  header();
  if(gen_tab(argv[1])) {
    error("Not implemented");
    return 1;
  }
  return 0;
}
