/*
 * interp.c - low quality linear interpolation table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdlib.h> /* malloc(), free() */
#include "Defines.h"
#include "common.h"
#include "interp.h"

#ifdef __WATCOMC__
i8 (*Interptab)[2UL*INTERP_STEPS*256]=NULL;
#else /* !defined(__WATCOMC__) */
i8 (*Interptab)[2*INTERP_STEPS*256]=NULL;
#endif /* !defined(__WATCOMC__) */

/*
 * Linear interpolation table:
 *   s=INTERP_STEPS
 *   x=0->s-1
 *   y=-128->127
 *   i=(y+128)^128
 *   Interptab[0][x][i]=y*(s-x)/s
 *   Interptab[1][x][i]=y*x/s
 * or
 *   a=y*x/s
 *   Interptab[0][x][i]=y-a
 *   Interptab[1][x][i]=a
 *
 * Returns `false' on success, `true' on error
 */
bool init_Interptab() {
  i8*t,dy=INTERP_STEPS;
  i16 Y=-128*INTERP_STEPS,y;
  u8 n;
  if(!(Interptab=malloc(sizeof(*Interptab)))) return true;
  t=(i8*)Interptab;
  do {
    y=0; n=128; do { *t=y/INTERP_STEPS; y+=dy; t++; n--; } while(n);
    y=Y; n=128; do { *t=y/INTERP_STEPS; y+=dy; t++; n--; } while(n);
    Y+=128; dy--;
  } while(dy);
  /* dy=0,Y=0; */
  do {
    y=0; n=128; do { *t=y/INTERP_STEPS; y+=dy; t++; n--; } while(n);
    y=Y; n=128; do { *t=y/INTERP_STEPS; y+=dy; t++; n--; } while(n);
    Y-=128; dy++;
  } while(dy<INTERP_STEPS);
  return false;
}

void free_Interptab() {
  if(Interptab) {
    free(Interptab);
    Interptab=NULL;
  }
}
