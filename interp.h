/*
 * interp.h - low quality linear interpolation table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef INTERP_H
#define INTERP_H

#include <stdbool.h>
#include "Defines.h"
#include "common.h"

#define INTERP_BITS 3
#define INTERP_STEPS (1<<INTERP_BITS)

#ifdef __WATCOMC__
extern i8 (*Interptab)[2UL*INTERP_STEPS*256];
#else /* !defined(__WATCOMC__) */
extern i8 (*Interptab)[2*INTERP_STEPS*256];
#endif /* !defined(__WATCOMC__) */
extern bool init_Interptab();
extern void free_Interptab();

#endif /* !INTERP_H */
