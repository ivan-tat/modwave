/*
 * interp2.c - low quality cubic Lagrange interpolation table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdlib.h> /* malloc(), free() */
#include "Defines.h"
#include "common.h"
#include "interp2.h"

#ifdef __WATCOMC__
i16 (*Interptab2)[3UL*INTERP2_STEPS*256]=NULL;
#else /* !defined(__WATCOMC__) */
i16 (*Interptab2)[3*INTERP2_STEPS*256]=NULL;
#endif /* !defined(__WATCOMC__) */

/*
 * Cubic Lagrange interpolation table:
 *   s=INTERP2_STEPS
 *   x=0->s-1
 *   y=-128->127
 *   i=(y+128)^128
 *   Interptab2[0][x][i]=y*(s-x)*(s*2-x)/2/s/s
 *   Interptab2[1][x][i]=y*x*(s*2-x)/s/s
 *   Interptab2[2][x][i]=-y*x*(s-x)/2/s/s
 * or
 *   c=y*x
 *   b=(c*x/s-c)/2/s
 *   a=y+b-c/s
 *   Interptab2[0][x][i]=a
 *   Interptab2[1][x][i]=y-a-b
 *   Interptab2[2][x][i]=b
 *
 * Returns `false' on success, `true' on error
 */
bool init_Interptab2() {
  i16*t;
  i32 y;
  u8 x,n;
  if(!(Interptab2=malloc(sizeof(*Interptab2)))) return true;
  t=(i16*)Interptab2;
  x=INTERP2_STEPS;
  do {
    i32 dy=(i32)x*(INTERP2_STEPS+x);
    y=0;
    n=128; do { *t=y/(2<<(INTERP2_BITS*2)); y+=dy; t++; n--; } while(n);
    y=-128*dy;
    n=128; do { *t=y/(2<<(INTERP2_BITS*2)); y+=dy; t++; n--; } while(n);
    x--;
  } while(x);
  /* x=0 */
  do {
    i32 dy=(i32)x*(2*INTERP2_STEPS-x);
    y=0;
    n=128; do { *t=y/(1<<(INTERP2_BITS*2)); y+=dy; t++; n--; } while(n);
    y=-128*dy;
    n=128; do { *t=y/(1<<(INTERP2_BITS*2)); y+=dy; t++; n--; } while(n);
    x++;
  } while(x<INTERP2_STEPS);
  /* x=INTERP2_STEPS */
  do {
    i32 dy=-(i32)x*(INTERP2_STEPS-x);
    y=0;
    n=128; do { *t=y/(2<<(INTERP2_BITS*2)); y+=dy; t++; n--; } while(n);
    y=-128*dy;
    n=128; do { *t=y/(2<<(INTERP2_BITS*2)); y+=dy; t++; n--; } while(n);
    x--;
  } while(x);
  return false;
}

void free_Interptab2() {
  if(Interptab2) {
    free(Interptab2);
    Interptab2=NULL;
  }
}
