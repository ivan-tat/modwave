/*
 * interp2.h - low quality cubic Lagrange interpolation table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef INTERP2_H
#define INTERP2_H

#include <stdbool.h>
#include "Defines.h"
#include "common.h"

#define INTERP2_BITS 3
#define INTERP2_STEPS (1<<INTERP2_BITS)

#ifdef __WATCOMC__
extern i16 (*Interptab2)[3UL*INTERP2_STEPS*256];
#else /* !defined(__WATCOMC__) */
extern i16 (*Interptab2)[3*INTERP2_STEPS*256];
#endif /* !defined(__WATCOMC__) */
extern bool init_Interptab2();
extern void free_Interptab2();

#endif /* !INTERP2_H */
