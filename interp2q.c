/*
 * interp2q.c - normal quality cubic Lagrange interpolation table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdlib.h> /* malloc(), free() */
#include "Defines.h"
#include "common.h"
#include "interp2q.h"

#ifdef __WATCOMC__
i16 (*Interptab2_q)[3UL*INTERP2_Q_STEPS*256]=NULL;
#else /* !defined(__WATCOMC__) */
i16 (*Interptab2_q)[3*INTERP2_Q_STEPS*256]=NULL;
#endif /* !defined(__WATCOMC__) */

/*
 * Cubic Lagrange interpolation table:
 *   s=INTERP2_Q_STEPS
 *   x=0->s-1
 *   y=-128->127
 *   i=(y+128)^128
 *   Interptab2_q[0][x][i]=y*(s-x)*(s*2-x)/2/s/s
 *   Interptab2_q[1][x][i]=y*x*(s*2-x)/s/s
 *   Interptab2_q[2][x][i]=-y*x*(s-x)/2/s/s
 * or
 *   c=y*x
 *   b=(c*x/s-c)/2/s
 *   a=y+b-c/s
 *   Interptab2_q[0][x][i]=a
 *   Interptab2_q[1][x][i]=y-a-b
 *   Interptab2_q[2][x][i]=b
 *
 * Returns `false' on success, `true' on error
 */
bool init_Interptab2_q() {
  i16*t;
  i32 y;
  u8 x,n;
  if(!(Interptab2_q=malloc(sizeof(*Interptab2_q)))) return true;
  t=(i16*)Interptab2_q;
  x=INTERP2_Q_STEPS;
  do {
    i32 dy=(i32)x*(INTERP2_Q_STEPS+x);
    y=0;
    n=128; do { *t=y/(2<<(INTERP2_Q_BITS*2)); y+=dy; t++; n--; } while(n);
    y=-128*dy;
    n=128; do { *t=y/(2<<(INTERP2_Q_BITS*2)); y+=dy; t++; n--; } while(n);
    x--;
  } while(x);
  /* x=0 */
  do {
    i32 dy=(i32)x*(2*INTERP2_Q_STEPS-x);
    y=0;
    n=128; do { *t=y/(1<<(INTERP2_Q_BITS*2)); y+=dy; t++; n--; } while(n);
    y=-128*dy;
    n=128; do { *t=y/(1<<(INTERP2_Q_BITS*2)); y+=dy; t++; n--; } while(n);
    x++;
  } while(x<INTERP2_Q_STEPS);
  /* x=INTERP2_Q_STEPS */
  do {
    i32 dy=-(i32)x*(INTERP2_Q_STEPS-x);
    y=0;
    n=128; do { *t=y/(2<<(INTERP2_Q_BITS*2)); y+=dy; t++; n--; } while(n);
    y=-128*dy;
    n=128; do { *t=y/(2<<(INTERP2_Q_BITS*2)); y+=dy; t++; n--; } while(n);
    x--;
  } while(x);
  return false;
}

void free_Interptab2_q() {
  if(Interptab2_q) {
    free(Interptab2_q);
    Interptab2_q=NULL;
  }
}
