/*
 * interp2q.h - normal quality cubic Lagrange interpolation table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef INTERP2Q_H
#define INTERP2Q_H

#include <stdbool.h>
#include <stdlib.h> /* malloc(), free() */
#include "Defines.h"
#include "common.h"

#ifdef __WATCOMC__
# define INTERP2_Q_BITS 5 /* Fit in 64K segment limit under DOS */
#else /* !defines(__WATCOMC__) */
# define INTERP2_Q_BITS 6
#endif /* !defines(__WATCOMC__) */
#define INTERP2_Q_STEPS (1<<INTERP2_Q_BITS)

#ifdef __WATCOMC__
extern i16 (*Interptab2_q)[3UL*INTERP2_Q_STEPS*256];
#else /* !defined(__WATCOMC__) */
extern i16 (*Interptab2_q)[3*INTERP2_Q_STEPS*256];
#endif /* !defined(__WATCOMC__) */
extern bool init_Interptab2_q();
extern void free_Interptab2_q();

#endif /* !INTERP2Q_H */
