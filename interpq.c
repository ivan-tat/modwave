/*
 * interpq.c - normal quality linear interpolation table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdlib.h> /* malloc(), free() */
#include "Defines.h"
#include "common.h"
#include "interpq.h"

#ifdef __WATCOMC__
i8 (*Interptab_q)[2UL*INTERP_Q_STEPS*256];
#else /* !defined(__WATCOMC__) */
i8 (*Interptab_q)[2*INTERP_Q_STEPS*256];
#endif /* !defined(__WATCOMC__) */

/*
 * Linear interpolation table:
 *   s=INTERP_Q_STEPS
 *   x=0->s-1
 *   y=-128->127
 *   i=(y+128)^128
 *   Interptab_q[0][x][i]=y*(s-x)/s
 *   Interptab_q[1][x][i]=y*x/s
 * or
 *   a=y*x/s
 *   Interptab_q[0][x][i]=y-a
 *   Interptab_q[1][x][i]=a
 *
 * Returns `false' on success, `true' on error
 */
bool init_Interptab_q() {
  i8*t,dy=INTERP_Q_STEPS;
  i16 Y=-128*INTERP_Q_STEPS,y;
  u8 n;
  if(!(Interptab_q=malloc(sizeof(*Interptab_q)))) return true;
  t=(i8*)Interptab_q;
  do {
    y=0; n=128; do { *t=y/INTERP_Q_STEPS; y+=dy; t++; n--; } while(n);
    y=Y; n=128; do { *t=y/INTERP_Q_STEPS; y+=dy; t++; n--; } while(n);
    Y+=128; dy--;
  } while(dy);
  /* dy=0,Y=0; */
  do {
    y=0; n=128; do { *t=y/INTERP_Q_STEPS; y+=dy; t++; n--; } while(n);
    y=Y; n=128; do { *t=y/INTERP_Q_STEPS; y+=dy; t++; n--; } while(n);
    Y-=128; dy++;
  } while(dy<INTERP_Q_STEPS);
  return false;
}

void free_Interptab_q() {
  if(Interptab_q) {
    free(Interptab_q);
    Interptab_q=NULL;
  }
}
