/*
 * interpq.h - normal quality linear interpolation table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef INTERPQ_H
#define INTERPQ_H

#include <stdbool.h>
#include "Defines.h"
#include "common.h"

#define INTERP_Q_BITS 6
#define INTERP_Q_STEPS (1<<INTERP_Q_BITS)

#ifdef __WATCOMC__
extern i8 (*Interptab_q)[2UL*INTERP_Q_STEPS*256];
#else /* !defined(__WATCOMC__) */
extern i8 (*Interptab_q)[2*INTERP_Q_STEPS*256];
#endif /* !defined(__WATCOMC__) */
extern bool init_Interptab_q();
extern void free_Interptab_q();

#endif /* !INTERPQ_H */
