/*
 * mem.c - memory-related routines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include "debug.h"
#include "mem.h"

/****************************************************************************/

#if DEBUG

void DBG_MALLOC_ERR(long req) {
  DBG_PRINTF("malloc() error: requested=0x%lX"NL,(long)req);
}

void DBG_REALLOC_ERR(long cur,long req) {
  DBG_PRINTF("realloc() error: current=0x%lX, requested=0x%lX"NL,
    (long)cur,(long)req);
}

#endif /* !DEBUG */

/****************************************************************************/

/* Returns `true' on error */
bool _Malloc(size_t size,void**ptr) {
  if(!(*ptr=malloc(size))) {
    if(DEBUG) { DBG_MALLOC_ERR(size); }
    return true;
  }
  return false;
}

/****************************************************************************/

/* Returns `true' on error */
bool _Realloc(void**ptr,
#if !DEBUG
  UNUSED_PARAM
#endif /* !DEBUG */
  size_t old,size_t size) {
  void*p=realloc(*ptr,size);
  UNUSED_HINT(old);

  if(!p) {
    if(DEBUG) { DBG_REALLOC_ERR(old,size); }
    return true;
  }

  *ptr=p;
  return false;
}

/****************************************************************************/

void _Free(void**ptr) {
  if(*ptr) {
    free(*ptr);
    *ptr=NULL;
  }
}
