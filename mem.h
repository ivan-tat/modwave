/*
 * mem.h - memory-related routines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef MEM_H
#define MEM_H

#include <stdbool.h>
#include <stdlib.h>
#include "Defines.h"
#include "common.h"

#if DEBUG

extern void DBG_MALLOC_ERR(long req);
extern void DBG_REALLOC_ERR(long cur,long req);

#else /* !DEBUG */

# define DBG_MALLOC_ERR(req)
# define DBG_REALLOC_ERR(cur,req)

#endif /* !DEBUG */

/* Returns `true' on error */
extern bool _Malloc(size_t size,void**ptr);

/* Returns `true' on error */
extern bool _Realloc(void**ptr,
#if !DEBUG
  UNUSED_PARAM
#endif /* !DEBUG */
  size_t old,size_t size);

void _Free(void**ptr);

#endif /* !MEM_H */
