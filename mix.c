/*
 * mix.c - low quality (no interpolation)
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdio.h>
#include "Defines.h"
#include "common.h"
#include "mix.h"
#include "play.h"
#include "vol.h"

/*
 * d = sample data
 * s = sample step
 * p = sample position
 * a = sample amplitude at a given position
 * v = single channel volume
 * v0,v1 = volume for left and right channels
 * vo = single channel volume table offset
 * vo0,vo1 = volume table offsets for left and right channels
 */

#pragma pack(push,1)
union _vo_u { struct { i8 l; u8 h; } b; i16 w; };
#pragma pack(pop)

#define INIT_SMP                                                             \
  d=c->smpdata;                                                              \
  s.d[0]=c->smpstep;                                                         \
  s.d[1]=0;                                                                  \
  p.pos=c->smppos;                                                           \
  p.w[3]=0;

#define GET_SMP                                                              \
  d[p.pos.i]

#define IS_VOL_BOTH                                                          \
  c->fvol[0]&&c->fvol[1]

#define INIT_VOL(x)                                                          \
  x=c->vol;

#define INIT_VOL_LEFT_OR_RIGHT(x)                                            \
  if(c->fvol[0]) x=c->fvol[0]; else { x=c->fvol[1]; buf++; }

#define INIT_VOL_BOTH(a,b)                                                   \
  a=c->fvol[0];                                                              \
  b=c->fvol[1];

#define MIX_MONO                                                             \
  buf[0]+=(i16)a*v>>SONG_VOL_BITS;

#define MIX_STEREO                                                           \
  buf[0]+=(i16)a*v0>>SONG_VOL_BITS;                                          \
  buf[1]+=(i16)a*v1>>SONG_VOL_BITS;

#define MIX_MONO_VT                                                          \
  vo.b.l=a;                                                                  \
  buf[0]+=(*Voltab)[vo.w];

#define MIX_STEREO_VT                                                        \
  vo0.b.l=a;                                                                 \
  vo1.b.l=a;                                                                 \
  buf[0]+=(*Voltab)[vo0.w];                                                  \
  buf[1]+=(*Voltab)[vo1.w];

#define NEXT_POS(n)                                                          \
  buf+=n;                                                                    \
  p.q+=s.q;

void mix_sample_mono(const struct chn_t*c,u16 count,i16*buf) {
  const i8*d;
  union smppos64_u s,p;
  u8 v;
  INIT_SMP
  INIT_VOL(v)
  do {
    i8 a=GET_SMP;
    MIX_MONO
    NEXT_POS(1)
    count--;
  } while(count);
}

void mix_sample_stereo(const struct chn_t*c,u16 count,i16*buf) {
  const i8*d;
  union smppos64_u s,p;
  INIT_SMP
  if(IS_VOL_BOTH) {
    u8 v0,v1;
    INIT_VOL_BOTH(v0,v1)
    do {
      i8 a=GET_SMP;
      MIX_STEREO
      NEXT_POS(2)
      count--;
    } while(count);
  } else {
    u8 v;
    INIT_VOL_LEFT_OR_RIGHT(v)
    do {
      i8 a=GET_SMP;
      MIX_MONO
      NEXT_POS(2)
      count--;
    } while(count);
  }
}

void mix_sample_mono_vt(const struct chn_t*c,u16 count,i16*buf) {
  const i8*d;
  union smppos64_u s,p;
  union _vo_u vo;
  INIT_SMP
  INIT_VOL(vo.b.h)
  do {
    i8 a=GET_SMP;
    MIX_MONO_VT
    NEXT_POS(1)
    count--;
  } while(count);
}

void mix_sample_stereo_vt(const struct chn_t*c,u16 count,i16*buf) {
  const i8*d;
  union smppos64_u s,p;
  INIT_SMP
  if(IS_VOL_BOTH) {
    union _vo_u vo0,vo1;
    INIT_VOL_BOTH(vo0.b.h,vo1.b.h)
    do {
      i8 a=GET_SMP;
      MIX_STEREO_VT
      NEXT_POS(2)
      count--;
    } while(count);
  } else {
    union _vo_u vo;
    INIT_VOL_LEFT_OR_RIGHT(vo.b.h)
    do {
      i8 a=GET_SMP;
      MIX_MONO_VT
      NEXT_POS(2)
      count--;
    } while(count);
  }
}
