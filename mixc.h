/*
 * mixc.h - low quality cubic Lagrange interpolation
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef MIXC_H
#define MIXC_H

#include "Defines.h"
#include "common.h"
#include "play.h"

/* Here `count' is a number of samples per channel to mix */

extern void mix_sample_cubic_mono(const struct chn_t*c,u16 count,i16*buf);
extern void mix_sample_cubic_mono_vt(const struct chn_t*c,u16 count,i16*buf);
extern void mix_sample_cubic_stereo(const struct chn_t*c,u16 count,i16*buf);
extern void mix_sample_cubic_stereo_vt(const struct chn_t*c,u16 count,i16*buf);

#endif /* !MIXC_H */
