/*
 * mixcq.h - normal quality cubic Lagrange interpolation
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef MIXCQ_H
#define MIXCQ_H

#include "Defines.h"
#include "common.h"
#include "play.h"

/* Here `count' is a number of samples per channel to mix */

extern void mix_sample_cubic_mono_q(const struct chn_t*c,u16 count,i16*buf);
extern void mix_sample_cubic_mono_vt_q(const struct chn_t*c,u16 count,i16*buf);
extern void mix_sample_cubic_stereo_q(const struct chn_t*c,u16 count,i16*buf);
extern void mix_sample_cubic_stereo_vt_q(const struct chn_t*c,u16 count,i16*buf);

#endif /* !MIXCQ_H */
