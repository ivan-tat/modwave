/*
 * mixl.h - low quality linear interpolation
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef MIXL_H
#define MIXL_H

#include "Defines.h"
#include "common.h"
#include "play.h"

/* Here `count' is a number of samples per channel to mix */

extern void mix_sample_linear_mono(const struct chn_t*c,u16 count,i16*buf);
extern void mix_sample_linear_mono_vt(const struct chn_t*c,u16 count,i16*buf);
extern void mix_sample_linear_stereo(const struct chn_t*c,u16 count,i16*buf);
extern void mix_sample_linear_stereo_vt(const struct chn_t*c,u16 count,i16*buf);

#endif /* !MIXL_H */
