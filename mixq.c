/*
 * mixq.c - normal quality (no interpolation)
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdio.h>
#include "Defines.h"
#include "common.h"
#include "mixq.h"
#include "play.h"
#include "volq.h"

/*
 * d = sample data
 * s = sample step
 * p = sample position
 * a = sample amplitude at a given position
 * v = single channel volume
 * v0,v1 = volume for left and right channels
 * vo = single channel volume table offset
 * vo0,vo1 = volume table offsets for left and right channels
 * ovs = output volume shift
 */

#pragma pack(push,1)
union _vo_u { struct { i8 l; u8 h; } b; i16 w; };
#pragma pack(pop)

#define INIT_SMP                                                             \
  d=c->smpdata;                                                              \
  s.d[0]=c->smpstep;                                                         \
  s.d[1]=0;                                                                  \
  p.pos=c->smppos;                                                           \
  p.w[3]=0;

#define GET_SMP                                                              \
  d[p.pos.i]

#define IS_VOL_BOTH                                                          \
  c->fvol[0]&&c->fvol[1]

#define INIT_VOL(x)                                                          \
  x=c->vol;

#define INIT_VOL_LEFT_OR_RIGHT(x)                                            \
  if(c->fvol[0]) x=c->fvol[0]; else { x=c->fvol[1]; buf++; }

#define INIT_VOL_BOTH(a,b)                                                   \
  a=c->fvol[0];                                                              \
  b=c->fvol[1];

#define MIX_MONO(ovs)                                                        \
  buf[0]+=((i16)a*v<<(8-SONG_VOL_BITS))>>ovs;

#define MIX_STEREO(ovs)                                                      \
  buf[0]+=((i16)a*v0<<(8-SONG_VOL_BITS))>>ovs;                               \
  buf[1]+=((i16)a*v1<<(8-SONG_VOL_BITS))>>ovs;

#define MIX_MONO_VT                                                          \
  vo.b.l=a;                                                                  \
  buf[0]+=(*Voltab_q)[vo.w];

#define MIX_STEREO_VT                                                        \
  vo0.b.l=a;                                                                 \
  vo1.b.l=a;                                                                 \
  buf[0]+=(*Voltab_q)[vo0.w];                                                \
  buf[1]+=(*Voltab_q)[vo1.w];

#define NEXT_POS(n)                                                          \
  buf+=n;                                                                    \
  p.q+=s.q;

#define DO_MIX                                                               \
  switch(play.ovs) {                                                         \
  default:                                                                   \
  case 0: MIX(0); break;                                                     \
  case 1: MIX(1); break;                                                     \
  case 2: MIX(2); break;                                                     \
  case 3: MIX(3); break;                                                     \
  case 4: MIX(4); break;                                                     \
  case 5: MIX(5); break;                                                     \
  }

void mix_sample_mono_q(const struct chn_t*c,u16 count,i16*buf) {
  const i8*d;
  union smppos64_u s,p;
  u8 v;
  INIT_SMP
  INIT_VOL(v)
#define MIX(ovs)                                                             \
  do {                                                                       \
    i8 a=GET_SMP;                                                            \
    MIX_MONO(ovs)                                                            \
    NEXT_POS(1)                                                              \
    count--;                                                                 \
  } while(count)
  DO_MIX
#undef MIX
}

void mix_sample_stereo_q(const struct chn_t*c,u16 count,i16*buf) {
  const i8*d;
  union smppos64_u s,p;
  INIT_SMP
  if(IS_VOL_BOTH) {
    u8 v0,v1;
    INIT_VOL_BOTH(v0,v1)
#define MIX(ovs)                                                             \
    do {                                                                     \
      i8 a=GET_SMP;                                                          \
      MIX_STEREO(ovs)                                                        \
      NEXT_POS(2)                                                            \
      count--;                                                               \
    } while(count)
    DO_MIX
#undef MIX
  } else {
    u8 v;
    INIT_VOL_LEFT_OR_RIGHT(v)
#define MIX(ovs)                                                             \
    do {                                                                     \
      i8 a=GET_SMP;                                                          \
      MIX_MONO(ovs)                                                          \
      NEXT_POS(2)                                                            \
      count--;                                                               \
    } while(count)
    DO_MIX
#undef MIX
  }
}

void mix_sample_mono_vt_q(const struct chn_t*c,u16 count,i16*buf) {
  const i8*d;
  union smppos64_u s,p;
  union _vo_u vo;
  INIT_SMP
  INIT_VOL(vo.b.h)
  do {
    i8 a=GET_SMP;
    MIX_MONO_VT
    NEXT_POS(1)
    count--;
  } while(count);
}

void mix_sample_stereo_vt_q(const struct chn_t*c,u16 count,i16*buf) {
  const i8*d;
  union smppos64_u s,p;
  INIT_SMP
  if(IS_VOL_BOTH) {
    union _vo_u vo0,vo1;
    INIT_VOL_BOTH(vo0.b.h,vo1.b.h)
    do {
      i8 a=GET_SMP;
      MIX_STEREO_VT
      NEXT_POS(2)
      count--;
    } while(count);
  } else {
    union _vo_u vo;
    INIT_VOL_LEFT_OR_RIGHT(vo.b.h)
    do {
      i8 a=GET_SMP;
      MIX_MONO_VT
      NEXT_POS(2)
      count--;
    } while(count);
  }
}
