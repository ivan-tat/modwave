/*
 * mod.c - Amiga MOD music file loader
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdlib.h> /* malloc(), free() */
#include <string.h> /* memset() */
#include "Defines.h"
#include "common.h"
#include "debug.h"
#include "errors.h"
#include "file.h"
#include "mem.h"
#include "mod.h"
#include "song.h"
#include "syscalls.h"
#include "tables.h"

#define MOD_START_NOTE 9 /* `A' at lowest octave */
#define MOD_NOTES 87
#define MOD_ORD_LEN 128
#define MOD_PATS 256 /* maximum patterns count */
#define MOD_PAT_LEN 64
#define MOD_TUNE_MAX 15
#define MOD_VOL_MAX 64

/* Input file format */
#define MODFMT_OLD 0
#define MODFMT_GENERIC 1
#define MODFMT_DIGITAL_TRACKER 2
#define MODFMT_SOUND_TRACKER 3 /* 4 channels Sound Tracker 2.6 with tracks */

/* Input file flags */
#define MODFL_STARTREKKER (1<<0) /* 8 channels StarTrekker patterns */

#pragma pack(push,1)
struct _mod_title_t {
  char title[20]; /* HINT: not used */
};
struct _mod_ins_t {
  char title[22]; /* HINT: not used */
  u16 len;  /* words count, big-endian form (0 if no instrument) */
  u8 tune;  /* 0->15 (signed 4-bits integer: -8->+7) */
  u8 vol;   /* 0->64 */
  u16 lbeg; /* loop begin, big-endian form (-1 if no loop) */
  u16 llen; /* loop length, big-endian form (0 or 1 if no loop) */
};
struct _mod_info_t {
  u8 len;
  u8 unused;
  u8 order[MOD_ORD_LEN];
};
/* Sound Tracker 2.6 */
struct _mod_st_info_t {
  u8 order_end;
  u8 numtracks;
  union {
    u8 tracks[4]; /* HINT: not used */
    u32 order;
  } order[MOD_ORD_LEN];
};
#pragma pack(pop)

struct _mod_fmt_t {
  u8 format;
  u8 flags;
  u8 num_insts; /* number of instruments */
  u8 num_chans; /* number of channels */
  u16 off_info; /* information offset */
  u16 hdr_size; /* header size */
  u16 off_pats; /* patterns offset */
};

#define _mod_hdr_get_insts(h)                                                \
  ((struct _mod_ins_t*)((u8*)(h)+sizeof(struct _mod_title_t)))

#define _mod_hdr_get_info(h,fmt)                                             \
  ((struct _mod_info_t*)((u8*)(h)+(fmt).off_info))

#define _mod_hdr_get_st_info(h,fmt)                                          \
  ((struct _mod_st_info_t*)((u8*)(h)+(fmt).off_info))

/* Amiga's constants with base C-note period = 428. Only for loading */
static const u16 _mod_periods[MOD_NOTES]={
  4064,3840,3624,
  3424,3232,3048,2880,2712,2560,2416,2280,2152,2032,1920,1812,
  1712,1616,1524,1440,1356,1280,1208,1140,1076,1016,960,906,
  856,808,762,720,678,640,604,570,538,508,480,453,
  428,404,381,360,339,320,302,285,269,254,240,226,
  214,202,190,180,170,160,151,143,135,127,120,113,
  107,101,95,90,85,80,75,71,67,63,60,56,
  53,50,47,45,42,40,37,35,33,31,30,28
};

/****************************************************************************/

#if DEBUG_LOAD

static void DBG_DUMP_RESULT(bool result) {
  DBG_PRINTF("result=%hhu"NL,(u8)result);
}

static void DBG_DUMP_FORMAT(const struct _mod_fmt_t*fmt) {
  DBG_PRINTF("format=%hhu, flags=0x%hhX, num_insts=%hhu, num_chans=%hhu, "
    "off_info=0x%hX, hdr_size=0x%hX, off_pats=0x%hX"NL,
    (u8)fmt->format,(u8)fmt->flags,(u8)fmt->num_insts,(u8)fmt->num_chans,
    (u16)fmt->off_info,(u16)fmt->hdr_size,(u16)fmt->off_pats);
}

static void DBG_DUMP_ID(u32 id) {
  u8 n=4;
  DBG_PRINTF("ID=0x%08X (",(u32)id);
  do {
    if((id&0xFF)<32) { DBG_PRINTF("[%hhu]",(u8)id); }
    else if((id&0xFF)<127) { DBG_PRINTF("['%c']",(u8)id); }
    else { DBG_PRINTF("[0x%hhX]",(u8)id); }
    id>>=8; n--;
  } while(n);
  DBG_LOG(")"NL);
}

static void DBG_DUMP_INS_NUM(const struct ins_t*i) {
  DBG_PRINTF("num=%hhu: ",(u8)get_instrument_index(i)+1);
}

static void DBG_DUMP_SAMPLE_LOOP(const struct ins_t*i) {
  DBG_PRINTF("lbeg=0x%lX, llen=0x%lX, lend=0x%lX",
    (u32)i->lbeg,(u32)i->llen,(u32)i->lbeg+i->llen);
}

#else /* !DEBUG_LOAD */

# define DBG_DUMP_RESULT(result)
# define DBG_DUMP_FORMAT(fmt)
# define DBG_DUMP_ID(id)
# define DBG_DUMP_INS_NUM(i)
# define DBG_DUMP_SAMPLE_LOOP(i)

#endif /* !DEBUG_LOAD */

/****************************************************************************/

static ERR _mod_read_id(int fd,const struct _mod_fmt_t*fmt,u32*id) {
  ERR err=E_OK;

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_FORMAT(fmt);
  }
  if(_Lseek(fd,fmt->hdr_size,SEEK_SET)) goto _read_err;
  if(_Read(fd,id,4)) goto _read_err;
  goto _exit;
_read_err: err=E_FREAD;
_exit:
  if(DEBUG_LOAD) {
    DBG_DUMP_ERR(err);
    DBG_LEAVE();
  }
  return err;
}

/****************************************************************************/

/* In: `format' and `num_insts' members of `fmt' must be set */
static void _mod_update_struct(struct _mod_fmt_t*fmt) {
  fmt->off_info=
    sizeof(struct _mod_title_t)+sizeof(struct _mod_ins_t)*fmt->num_insts;
  fmt->hdr_size=fmt->off_info;

  switch(fmt->format) {
  case MODFMT_OLD:
  default:
    fmt->hdr_size+=sizeof(struct _mod_info_t);
    fmt->off_pats=fmt->hdr_size;
    break;
  case MODFMT_GENERIC:
    fmt->hdr_size+=sizeof(struct _mod_info_t);
    fmt->off_pats=fmt->hdr_size;
    /* skip ID (4 bytes) */
    fmt->off_pats+=4;
    break;
  case MODFMT_DIGITAL_TRACKER:
    fmt->hdr_size+=sizeof(struct _mod_info_t);
    fmt->off_pats=fmt->hdr_size;
    /* skip ID (4 bytes) and extra 4 bytes (usually 0,64,0,0) */
    fmt->off_pats+=8;
    break;
  case MODFMT_SOUND_TRACKER:
    fmt->hdr_size+=sizeof(struct _mod_st_info_t);
    fmt->off_pats=fmt->hdr_size;
    /* skip ID (4 bytes) */
    fmt->off_pats+=4;
    break;
  }
}

/****************************************************************************/

static void _mod_set_format(u8 format,u8 flags,u8 chans,struct _mod_fmt_t*fmt) {
  if(DEBUG_LOAD) { DBG_ENTER(); }
  fmt->format=format;
  fmt->flags=flags;
  switch(format) {
  case MODFMT_OLD:
  default:
    fmt->num_insts=15;
    fmt->num_chans=4;
    break;
  case MODFMT_GENERIC:
  case MODFMT_DIGITAL_TRACKER:
    fmt->num_insts=31;
    fmt->num_chans=chans;
    break;
  case MODFMT_SOUND_TRACKER:
    fmt->num_insts=31;
    fmt->num_chans=4;
    break;
  }
  _mod_update_struct(fmt);
  if(DEBUG_LOAD) {
    DBG_DUMP_FORMAT(fmt);
    DBG_LEAVE();
  }
}

/****************************************************************************/

/* Returns `false' on success */
static bool _mod_is_st_id(u32 id) {
  bool result;
  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_ID(id);
  }
  result = ((id==MK_U32('M','T','N',0))
  ||        (id==MK_U32('I','T','1','0')))?false:true;
  if(DEBUG_LOAD) {
    DBG_DUMP_RESULT(result);
    DBG_LEAVE();
  }
  return result;
}

/****************************************************************************/

/* Returns `false' on success and updates `fmt' structure */
static bool _mod_set_format_by_id(u32 id,struct _mod_fmt_t*fmt) {
  bool result=true;
  u8 f=MODFMT_GENERIC;
  u8 fl=0;
  u8 c;

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_ID(id);
  }
  if((id==MK_U32('M','.','K','.'))
  || (id==MK_U32('M','!','K','!'))
  || (id==MK_U32('M','&','K','!'))
  || (id==MK_U32('N','.','T','.'))
  || (id==MK_U32('N','S','M','S'))
  || (id==MK_U32('L','A','R','D'))
  || (id==MK_U32('P','A','T','T'))
  || (id==MK_U32('E','X','O','4'))
  || (id==MK_U32('F','E','S','T'))
  || (id==MK_U32('F','L','T','4'))) {
    c=4;
    goto _found;
  }
  if(((id&0xFFFF0000)==MK_U32(0,0,'C','H'))
  || ((id&0xFFFF0000)==MK_U32(0,0,'C','N'))) {
    /* "xxCH", "xxCN" (xx=decimal number from 10 to 32) */
    u8 a=(id&0xFF),b=(id>>8)&0xFF;
    if((a>='1')&&(a<='3')&&(b>='0')&&(b<='9')) {
      c=(a-'0')*10+(b-'0'); /* c=10->39 */
      if(c<=32) goto _found;
    }
    goto _exit;
  }
  if((id&0xFFFFFF00)==MK_U32(0,'C','H','N')) {
    /* "xCHN" (x='2','4','5','6','7','8','9') */
    c=(id&0xFF)-'0';
    if((c==2)||((c>=4)&&(c<=9))) goto _found;
    goto _exit;
  }
  if(id==MK_U32('C','D','6','1')) {
    c=6;
    goto _found;
  }
  if((id&0x00FFFFFF)==MK_U32('F','A','0',0)) {
    /* "FA0x" (x='4','6','8') */
    c=(id>>24)-'0';
    if((c==4)||(c==6)||(c==8)) {
      f=MODFMT_DIGITAL_TRACKER;
      goto _found;
    }
    goto _exit;
  }
  if((id==MK_U32('C','D','8','1'))
  || (id==MK_U32('O','C','T','A'))
  || (id==MK_U32('O','K','T','A'))) {
    c=8;
    goto _found;
  }
  if((id==MK_U32('E','X','O','8'))
  || (id==MK_U32('F','L','T','8'))) {
    fl=MODFL_STARTREKKER;
    c=8;
    goto _found;
  }
  if((id&0x00FFFFFF)==MK_U32('T','D','Z',0)) {
    /* "TDZx" (x='1','2','3','4') */
    c=(id>>24)-'0';
    if((c>=1)&&(c<=4)) goto _found;
  }
  goto _exit;
_found: result=false; _mod_set_format(f,fl,c,fmt);
_exit:
  if(DEBUG_LOAD) {
    DBG_DUMP_FORMAT(fmt);
    DBG_DUMP_RESULT(result);
    DBG_LEAVE();
  }
  return result;
}

/****************************************************************************/

static ERR _mod_load_header(int fd,struct _mod_fmt_t*fmt,void**h) {
  ERR err=E_OK;

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_FORMAT(fmt);
  }
  if(_Malloc(fmt->hdr_size,h)) goto _malloc_err;
  if(_Lseek(fd,0,SEEK_SET)) goto _read_err;
  if(_Read(fd,*h,fmt->hdr_size)) goto _read_err;
  if(fmt->flags&MODFL_STARTREKKER) {
    u8*order,i;
    order=_mod_hdr_get_info(*h,*fmt)->order;
    /* check if it's really an ordinary 4 channels file */
    for(i=0;i<MOD_ORD_LEN;i++)
      if(order[i]&1) {
        fmt->flags&=~MODFL_STARTREKKER;
        fmt->num_chans=4;
        break;
      }
    /* convert indexes from StarTrekker */
    if(fmt->flags&MODFL_STARTREKKER) for(i=0;i<MOD_ORD_LEN;i++) order[i]>>=1;
  }
  goto _exit;
_malloc_err: err=E_MALLOC; goto _exit;
_read_err: err=E_FREAD;
_exit:
  if(DEBUG_LOAD) {
    DBG_DUMP_FORMAT(fmt);
    DBG_DUMP_ERR(err);
    DBG_LEAVE();
  }
  return err;
}

/****************************************************************************/

/* Returns `false' on success */
static bool _mod_check_instruments(const void*h,const struct _mod_fmt_t*fmt) {
  bool result=true;
  u8 n=fmt->num_insts;
  const struct _mod_ins_t*p=_mod_hdr_get_insts(h);

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_FORMAT(fmt);
  }
  while(n) {
    u16 len=(p->len!=256)?SWAPU16(p->len):0;
    if(len) {
      if((p->vol>MOD_VOL_MAX)||(p->tune>MOD_TUNE_MAX)) goto _exit;
    }
    p++; n--;
  }
  result=false;
_exit:
  if(DEBUG_LOAD) {
    DBG_DUMP_RESULT(result);
    DBG_LEAVE();
  }
  return result;
}

/****************************************************************************/

static void _mod_adjust_sample_loop(struct ins_t*i) {
  if(DEBUG_LOAD) {
    DBG_DUMP_INS_NUM(i);
    DBG_PRINTF("len=0x%lX, ",(u32)i->len);
  }
  /* check sample's loop */
  if(i->llen) {
    if(DEBUG_LOAD) { DBG_DUMP_SAMPLE_LOOP(i); }
    if(i->lbeg+i->llen>i->len) {
      if(i->lbeg<i->len)
        i->llen=i->len-i->lbeg;
      else {
        i->lbeg=0;
        i->llen=0;
      }
      if(DEBUG_LOAD) {
        DBG_LOG(" - bad loop"NL
          "^fixed: ");
        DBG_DUMP_SAMPLE_LOOP(i);
      }
    }
  } else {
    i->lbeg=0;
    if(DEBUG_LOAD) { DBG_LOG("no loop"); }
  }
  if(DEBUG_LOAD) { DBG_LOG(NL); }
}

static ERR _mod_import_instruments(const void*h,const struct _mod_fmt_t*fmt) {
  ERR err=E_OK;
  u8 n=fmt->num_insts;
  const struct _mod_ins_t*m=_mod_hdr_get_insts(h);
  struct ins_t*i;

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_FORMAT(fmt);
  }
  song.num_instruments=n;
  song.num_samples=0;
  i=song.instruments;
  while(n) {
    i->data=NULL;
    i->len =(m->len !=256)?(u32)SWAPU16(m->len )<<1:0;
    i->lbeg=(m->lbeg!=256)?(u32)SWAPU16(m->lbeg)<<1:0;
    i->llen=(m->llen!=256)?(u32)SWAPU16(m->llen)<<1:0;
    /* check sample's length */
    if(i->len) {
      /* check sample's loop */
      _mod_adjust_sample_loop(i);
      i->vol=MIN(m->vol,MOD_VOL_MAX);
      i->tune=MIN(m->tune,MOD_TUNE_MAX);
      song.num_samples++;
    } else {
      i->lbeg=0;
      i->llen=0;
      i->vol=0;
      i->tune=0;
      if(DEBUG_LOAD) { DBG_DUMP_INS_NUM(i); DBG_LOG("no sample"NL); }
    }
    m++; i++; n--;
  }
  if(DEBUG_LOAD) {
    DBG_DUMP_ERR(err);
    DBG_LEAVE();
  }
  return err;
}

/****************************************************************************/

#define EXTRA_SAMPLE_LEN 2 /* 1 for linear, 2 for cubic */

static ERR _mod_load_samples(int fd) {
  ERR err=E_OK;
  struct ins_t*i=song.instruments;
  u8 n=song.num_instruments;
  bool stop=false;

  if(DEBUG_LOAD) { DBG_ENTER(); }
  song.num_samples=0;
  while(n) {
    if(i->len) {
      if(DEBUG_LOAD) { DBG_DUMP_INS_NUM(i); }
      if(stop) {
        if(DEBUG_LOAD) { DBG_LOG("skipped"NL); }
        i->len=0;
      } else {
#if DEBUG_LOAD
        off_t off;
#endif /* DEBUG_LOAD */
        ssize_t sz;
        if(_Malloc(i->len+EXTRA_SAMPLE_LEN,(void**)&(i->data)))
          goto _malloc_err;
#if DEBUG_LOAD
        off=lseek(fd,0,SEEK_CUR);
        DBG_PRINTF("cur_off=0x%X, size=0x%X, next_off=0x%X"NL,
          (u32)off,(u32)i->len,(u32)off+i->len);
#endif /* DEBUG_LOAD */
        sz=read(fd,i->data,i->len);
        if((u32)sz==i->len) song.num_samples++;
        else if(sz<=0) {
          if(DEBUG_LOAD) {
            DBG_READ_ERR(fd,i->len,sz);
            DBG_LOG("skipped (no data)"NL);
          }
          free(i->data);
          i->data=NULL;
          i->len=0;
          stop=true;
        } else if((u32)sz<i->len) {
          if(DEBUG_LOAD) {
            DBG_READ_ERR(fd,i->len,sz);
            DBG_LOG("partly loaded"NL);
          }
          if(_Realloc((void**)&(i->data),i->len+EXTRA_SAMPLE_LEN,
            sz+EXTRA_SAMPLE_LEN)) {
            if(DEBUG_LOAD) { DBG_LOG("stopped"NL); }
            goto _malloc_err;
          }
          i->len=sz;
          _mod_adjust_sample_loop(i);
          song.num_samples++;
          stop=true;
        } else {
          if(DEBUG_LOAD) {
            DBG_READ_ERR(fd,i->len,sz);
            DBG_LOG("stopped"NL);
          }
          goto _read_err;
        }
        if(i->data) {
          /* Non-destructive filling of sample's tail
             for correct interpolation */
          if(i->llen&&(i->lbeg+i->llen==i->len)) {
            if(EXTRA_SAMPLE_LEN)
              memcpy(i->data+i->len,i->data+i->lbeg,EXTRA_SAMPLE_LEN);
          } else {
            if(EXTRA_SAMPLE_LEN)
              memset(i->data+i->len,0,EXTRA_SAMPLE_LEN);
          }
        }
      }
    }
    i++; n--;
  }
  goto _exit;
_malloc_err: err=E_MALLOC; goto _exit;
_read_err: err=E_FREAD;
_exit:
  if(DEBUG_LOAD) {
    DBG_DUMP_ERR(err);
    DBG_LEAVE();
  }
  return err;
}

/****************************************************************************/

/* Returns `false' on success */
static bool _mod_check_order(const void*h,const struct _mod_fmt_t*fmt) {
  bool result=true;
  u8 len;

  if(DEBUG_LOAD) { DBG_ENTER(); }
  len=_mod_hdr_get_info(h,*fmt)->len;
  if(DEBUG_LOAD) { DBG_PRINTF("len=%hhu"NL,(u8)len); }
  if((!len)||(len>MOD_ORD_LEN)) goto _exit;
  result=false;
_exit:
  if(DEBUG_LOAD) {
    DBG_DUMP_RESULT(result);
    DBG_LEAVE();
  }
  return result;
}

/****************************************************************************/

/* Returns `false' on success */
static bool _mod_check_st_order(const void*h,const struct _mod_fmt_t*fmt) {
  bool result=true;
  const struct _mod_st_info_t*hi;
  u8 order_end,numtracks,i;
  const u8*order;

  if(DEBUG_LOAD) { DBG_ENTER(); }
  hi=_mod_hdr_get_st_info(h,*fmt);
  order_end=hi->order_end;
  if(order_end>=MOD_ORD_LEN) goto _exit;
  numtracks=hi->numtracks;
  if(!numtracks) goto _exit;
  order=(const u8*)(hi->order);
  for(i=0;i<=order_end;i++) {
    u8 j;
    for(j=0;j<4;j++)
      if(order[j]>=numtracks) goto _exit;
    order+=4;
  }
  result=false;
_exit:
  if(DEBUG_LOAD) {
    DBG_DUMP_RESULT(result);
    DBG_LEAVE();
  }
  return result;
}

/****************************************************************************/

static ERR _mod_import_order(const void*h,struct _mod_fmt_t*fmt) {
  ERR err=E_OK;
  u8 i;
  const u8*order;

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_FORMAT(fmt);
  }
  song.order_len=_mod_hdr_get_info(h,*fmt)->len;
  order=_mod_hdr_get_info(h,*fmt)->order;
  if(DEBUG_LOAD) { DBG_PRINTF("order_len=%hhu"NL,(u8)song.order_len); }
  if(DEBUG_DUMP_ORDER) {
    DBG_LOG("order=");
    for(i=0;i<MOD_ORD_LEN;i++) DBG_PRINTF("%hhu ",(u8)order[i]);
    DBG_LOG(NL);
  }
  if(!song.order_len) goto _nopats;
  if(song.order_len>MOD_ORD_LEN) goto _badlen;
  /* get order length */
  song.num_patterns=0;
  for(i=0;i<song.order_len;i++) {
    i16 p=order[i];
    if(song.num_patterns<p) song.num_patterns=p;
    song.order[i]=p;
  }
  DBG_PRINTF("num_patterns=%hu (%s)"NL,
    (u16)song.num_patterns+1,(char*)"playable");
  /* find extra patterns if any */
  for(i=song.order_len;i<MOD_ORD_LEN;i++) {
    u8 p=order[i];
    if(song.num_patterns<p) song.num_patterns=p;
  }
  song.num_patterns++;
  DBG_PRINTF("num_patterns=%hu (%s)"NL,
    (u16)song.num_patterns,(char*)"all");
  goto _exit;
_nopats: err=E_NOPATS; goto _exit;
_badlen: err=E_ORDLEN;
_exit:
  if(DEBUG_LOAD) {
    DBG_DUMP_ERR(err);
    DBG_LEAVE();
  }
  return err;
}

/****************************************************************************/

static void _mod_unpack_st_pattern(const u8*tracks,u32 order,u8*dest) {
  #pragma pack(push,1)
  union {
    u8 tracks[4];
    u32 order;
  } tracks_order;
  #pragma pack(pop)
  const u8*t[4];
  u8 i,*p;
  u16 o;

  tracks_order.order=order;
  t[0]=tracks+(tracks_order.tracks[0]*MOD_PAT_LEN<<2);
  t[1]=tracks+(tracks_order.tracks[1]*MOD_PAT_LEN<<2);
  t[2]=tracks+(tracks_order.tracks[2]*MOD_PAT_LEN<<2);
  t[3]=tracks+(tracks_order.tracks[3]*MOD_PAT_LEN<<2);
  o=0;
  p=dest;
  for(i=MOD_PAT_LEN;i;i--) {
    memcpy(p+(0<<2),t[0]+o,1<<2);
    memcpy(p+(1<<2),t[1]+o,1<<2);
    memcpy(p+(2<<2),t[2]+o,1<<2);
    memcpy(p+(3<<2),t[3]+o,1<<2);
    o+=1<<2;
    p+=4<<2;
  }
}

/****************************************************************************/

static void _mod_convert_pattern(u8*p) {
  u16 e=(u16)song.num_channels*MOD_PAT_LEN; /* events counter */
  do {
    u16 period=(((u16)(p[0]&0xF))<<8)+p[1];
    u8 ins=(p[0]&0x10)+((p[2]>>4)&0xF),note=0;
    if(period) {
      u8 i;
      for(i=0;i<MOD_NOTES;i++)
        if(period>=_mod_periods[i]) { note=i+MOD_START_NOTE+1; break; }
      if(!note) note=MOD_START_NOTE+MOD_NOTES; /* highest */
    }
    p[0]=note;
    p[1]=ins;
    p[2]&=0xF; /* leave effect only */
    if(p[2]==FX_BREAK-1) { /* convert row number */
      u8 a=p[3]&0xF,b=p[3]>>4,x;
      if((a<=9)&&(b<=(MOD_PAT_LEN/10))&&((x=a+(b*10))<MOD_PAT_LEN)) p[3]=x;
      else { p[2]=0; p[3]=0; /* remove effect */ }
    }
    if(!((!p[2])&&(!p[3]))) p[2]++;
    p+=4; e--;
  } while(e);
}

/****************************************************************************/

static ERR _mod_import_st_order_and_patterns(int fd,const void*h,
  struct _mod_fmt_t*fmt) {

  ERR err=E_OK;
  u8*tracks=NULL;
  struct _mod_st_info_t*hi;
  u16 i,order_end,numtracks;
  const u8*order;
  struct pat_t*pat;
  u16 size;

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_FORMAT(fmt);
  }
  song.num_channels=4;
  song.num_patterns=0;
  song.pat_rowsize=(u16)song.num_channels<<2;
  size=song.pat_rowsize*MOD_PAT_LEN;
  hi=_mod_hdr_get_st_info(h,*fmt);
  order_end=hi->order_end;
  if(order_end>=MOD_ORD_LEN) goto _badlen;
  numtracks=hi->numtracks;
  if(!numtracks) goto _nopats;
  order=(const u8*)(hi->order);
  for(i=0;i<=order_end;i++) {
    u8 j;
    for(j=0;j<song.num_channels;j++)
      if(order[j]>=numtracks) goto _badord;
    order+=song.num_channels;
  }
  /* read tracks */
  if(_Malloc(song.num_channels*MOD_PAT_LEN*numtracks,(void**)&tracks))
    goto _malloc_err;
  if(_Lseek(fd,fmt->off_pats,SEEK_SET)) goto _read_err;
  if(_Read(fd,tracks,
    (unsigned long)song.num_channels*MOD_PAT_LEN*numtracks)) goto _read_err;
  song.order_len=order_end+1;
  pat=song.patterns;
  for(i=0;i<=order_end;i++) {
    bool f=true; /* flag to add a new pattern */
    if(i) { /* try to find already converted similar pattern */
      u16 j;
      for(j=0;j<i;j++) {
        if(hi->order[j].order==hi->order[i].order) {
          song.order[i]=song.order[j];
          f=false;
          break;
        }
      }
    }
    if(f) {
      song.order[i]=song.num_patterns;
      pat->format=PATFMT_MOD_RAW;
      pat->size=size;
      if(_Malloc(size,(void**)&(pat->data))) goto _malloc_err;
      song.num_patterns++;
      _mod_unpack_st_pattern(tracks,hi->order[i].order,pat->data);
      _mod_convert_pattern(pat->data);
      if(pack_pattern(pat)==1) goto _malloc_err;
      pat++;
    }
  }
  goto _exit;
_nopats: err=E_NOPATS; goto _exit;
_badlen: err=E_ORDLEN; goto _exit;
_badord: err=E_BADORD;
  if(DEBUG_LOAD) {
    DBG_PRINTF("numtracks=%hu, pos=%hhu, tracks=%hhu,%hhu,%hhu,%hhu"NL,
      (u16)numtracks,(u8)i,(u8)*order,(u8)order[1],(u8)order[2],(u8)order[3]);
  }
  goto _exit;
_malloc_err: err=E_MALLOC; goto _exit;
_read_err: err=E_FREAD;
_exit:
  if(tracks) free(tracks);
  if(DEBUG_LOAD) {
    DBG_DUMP_ERR(err);
    DBG_LEAVE();
  }
  return err;
}

/****************************************************************************/

static ERR _mod_load_patterns(int fd,const struct _mod_fmt_t*fmt) {
  ERR err=E_OK;

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_FORMAT(fmt);
    DBG_PRINTF(
      "order_len=%hhu, num_patterns=%hu"NL,
      (u8)song.order_len,(u16)song.num_patterns);
  }
  if(_Lseek(fd,fmt->off_pats,SEEK_SET)) goto _read_err;
  song.num_channels=fmt->num_chans; /* FIXME: is it needed here? */
  if(song.order_len) {
    struct pat_t*pat=song.patterns;
    u16 size,i;
    u8 used[(MOD_PATS+7)/8];
    i32 skip=0;
    song.pat_rowsize=(u16)song.num_channels<<2;
    size=song.pat_rowsize*MOD_PAT_LEN;
    /* collect patterns usage information */
    memset(used,0,(MOD_PATS+7)/8);
    for(i=0;i<song.order_len;i++) {
      i16 p=song.order[i];
      if(SONG_ORDER_SKIP!=p) used[p/8]|=1<<(p%8);
    }
    /* read patterns */
    for(i=0;i<song.num_patterns;i++) {
#if DEBUG_LOAD
      off_t off;
#endif /* DEBUG_LOAD */
      if(used[i/8]&(1<<(i%8))) {
        /* read pattern */
        pat->format=PATFMT_MOD_RAW;
        pat->size=size;
        if(_Malloc(size,(void**)&(pat->data))) goto _malloc_err;
        if(skip) {
          if(_Lseek(fd,skip,SEEK_CUR)) goto _read_err;
          skip=0;
        }
#if DEBUG_LOAD
        off=lseek(fd,0,SEEK_CUR);
        DBG_PRINTF(
          "num=%hhu, cur_off=0x%X, size=0x%X, next_off=0x%X - %s"NL,
          (u8)i,(u32)off,(u32)size,(u32)off+size,(char*)"used");
#endif /* DEBUG_LOAD */
        if(_Read(fd,pat->data,size)) goto _read_err;
        if(fmt->flags&MODFL_STARTREKKER) {
          /* reorder pattern's events */
          u8*p,*s,*d,j;
          if(_Malloc(size,(void**)&p)) goto _malloc_err;
          s=pat->data;
          d=p;
          for(j=0;j<MOD_PAT_LEN;j++) {
            memcpy(d,s,4*4);
            memcpy(d+4*4,s+MOD_PAT_LEN*4*4,4*4);
            s+=4*4;
            d+=8*4;
          }
          memcpy(pat->data,p,size);
          free(p);
        }
        _mod_convert_pattern(pat->data);
        if(pack_pattern(pat)==1) goto _malloc_err;
      } else {
#if DEBUG_LOAD
        off=lseek(fd,0,SEEK_CUR)+skip;
        DBG_PRINTF(
          "num=%hhu, cur_off=0x%X, size=0x%X, next_off=0x%X - %s"NL,
          (u8)i,(u32)off,(u32)size,(u32)off+size,(char*)"skipped");
#endif /* DEBUG_LOAD */
        skip+=size; /* ignore pattern */
      }
      pat++;
    }
    if((err==E_OK)&&skip)
      if(_Lseek(fd,skip,SEEK_CUR)) goto _read_err;
  }
  goto _exit;
_read_err: err=E_FREAD; goto _exit;
_malloc_err: err=E_MALLOC;
_exit:
  if(DEBUG_LOAD) {
    DBG_DUMP_ERR(err);
    DBG_LEAVE();
  }
  return err;
}

/****************************************************************************/

/* Sets `result' to true on success */
static ERR _mod_check_format(int fd,u8 format,bool*result) {
  ERR err=E_OK;
  void*h=NULL;
  struct _mod_fmt_t fmt;
  off_t fs; /* file's size */

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_PRINTF("format=%hhu"NL,(u8)format);
  }
  _mod_set_format(format,0,0,&fmt);
  fs=lseek(fd,0,SEEK_END);
  if(fs==-1) {
    if(DEBUG_LOAD) { DBG_LSEEK_ERR(fd,0,SEEK_END,fs); }
    goto _read_err;
  }
  if(fs>=fmt.hdr_size) {
    if(format!=MODFMT_OLD) {
      u32 id;
      if((err=_mod_read_id(fd,&fmt,&id))!=E_OK) goto _exit;
      if((format!=MODFMT_SOUND_TRACKER)
      && (_mod_set_format_by_id(id,&fmt))) goto _failed;
    }
    if((err=_mod_load_header(fd,&fmt,&h))!=E_OK) goto _exit;
    if(_mod_check_instruments(h,&fmt)) goto _failed;
    if(fmt.format==MODFMT_SOUND_TRACKER) {
      if(_mod_check_st_order(h,&fmt)) goto _failed;
    } else {
      if(_mod_check_order(h,&fmt)) goto _failed;
    }
  } else goto _failed;
  *result=true;
  goto _exit;
_read_err: err=E_FREAD; goto _exit;
_failed: *result=false;
_exit:
  if(h) _Free(&h);
  if(DEBUG_LOAD) {
    DBG_DUMP_ERR(err);
    DBG_DUMP_RESULT(*result);
    DBG_LEAVE();
  }
  return err;
}

/****************************************************************************/

ERR is_mod(const char*filename,bool*result) {
  ERR err=E_OK;
  int fd;

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_FILENAME(filename);
  }
  *result=false;
  if(_Open(filename,O_RDONLY,&fd)) goto _open_err;
  if((err=_mod_check_format(fd,MODFMT_SOUND_TRACKER,result))!=E_OK) goto _exit;
  if((err=_mod_check_format(fd,MODFMT_GENERIC,result))!=E_OK) goto _exit;
  if(*result) goto _exit;
  if((err=_mod_check_format(fd,MODFMT_OLD,result))!=E_OK) goto _exit;
  if(*result) goto _exit;
  goto _exit;
_open_err: err=E_FOPEN;
_exit:
  if(fd!=-1) _Close(&fd);
  if(DEBUG_LOAD) {
    DBG_DUMP_ERR(err);
    DBG_DUMP_RESULT(*result);
    DBG_LEAVE();
  }
  return err;
}

/****************************************************************************/

ERR mod_load(const char*filename) {
  ERR err=E_OK;
  int fd;
  struct _mod_fmt_t fmt;
  void*h=NULL;
  u32 id;

  if(DEBUG_LOAD) {
    DBG_ENTER();
    DBG_DUMP_FILENAME(filename);
  }
  song_clear();
  if(_Open(filename,O_RDONLY,&fd)) goto _open_err;

  /* check Sound Tracker 2.6 format */
  _mod_set_format(MODFMT_SOUND_TRACKER,0,0,&fmt);
  if((err=_mod_read_id(fd,&fmt,&id))==E_OK)
    if(!_mod_is_st_id(id)) goto _ok;

  /* check new format */
  _mod_set_format(MODFMT_GENERIC,0,0,&fmt);
  if((err=_mod_read_id(fd,&fmt,&id))==E_OK)
    if(!_mod_set_format_by_id(id,&fmt)) goto _ok;

  /* force old format */
  _mod_set_format(MODFMT_OLD,0,0,&fmt);

_ok:
  if((err=_mod_load_header(fd,&fmt,&h))!=E_OK) goto _stop;
  if((err=_mod_import_instruments(h,&fmt))!=E_OK) goto _stop;

  if(fmt.format==MODFMT_SOUND_TRACKER) {
    if((err=_mod_import_st_order_and_patterns(fd,h,&fmt))!=E_OK) goto _stop;
    _Free(&h);
  } else {
    if((err=_mod_import_order(h,&fmt))!=E_OK) goto _stop;
    _Free(&h);
    if((err=_mod_load_patterns(fd,&fmt))!=E_OK) goto _stop;
  }
  if((err=_mod_load_samples(fd))!=E_OK) goto _stop;
  goto _stop;
_open_err: err=E_FOPEN;
_stop:
  if(h) _Free(&h);
  if(fd!=-1) _Close(&fd);
  if(err!=E_OK) song_free();
  if(DEBUG_LOAD) {
    DBG_DUMP_ERR(err);
    DBG_LEAVE();
  }
  return err;
}
