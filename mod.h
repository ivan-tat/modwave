/*
 * mod.h - Amiga MOD music file loader
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef MOD_H
#define MOD_H

#include <stdbool.h>
#include "Defines.h"
#include "common.h"
#include "errors.h"

/* Returns `true' on success */
extern ERR is_mod(const char*filename,bool*result);

extern ERR mod_load(const char*filename);

#endif /* !MOD_H */
