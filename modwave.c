/*
 * modwave.c - main executable file
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <errno.h> /* errno */
#include <stdarg.h> /* va_list, va_start(), va_end() */
#include <stdio.h> /* printf(), fprintf(), vfprintf() */
#include <stdlib.h> /* atexit(), strtol() */
#include <string.h> /* strcmp() */
#include "Defines.h"
#include "common.h"
#include "errors.h"
#include "debug.h"
#include "amp.h"
#include "ampq.h"
#include "interp.h"
#include "interpq.h"
#include "interp2.h"
#include "interp2q.h"
#include "mix.h"
#include "mixq.h"
#include "mixl.h"
#include "mixlq.h"
#include "mixc.h"
#include "mixcq.h"
#include "mod.h"
#include "play.h"
#include "song.h"
#include "tables.h"
#include "vol.h"
#include "volq.h"
#include "wav.h"

#define NAME "modwave"
#define DESCRIPTION "MOD to WAV file converter (CLI)"
#define COPYRIGHT "Author: 2022, 2023 Ivan Tatarinov <ivan-tat" "@" "ya" "." "ru>"
#define LICENSE "License: Public domain. See: <https://unlicense.org>"
#define HOMEPAGE "https://gitlab.com/ivan-tat/modwave"

#define MIXBUF_LEN_MIN 1024
#define MIXBUF_LEN_MAX 16384

static bool opt_h=false;
static amiga_t opt_a=AMIGA_PAL;
static u32 opt_r=44100;
static u8 opt_c=2;
static u8 opt_b=16;
static u8 opt_i=2;
static u16 opt_v=0;
static u16 opt_mb=8192;
static u8 opt_mq=1;
static bool opt_vt=true;
static bool opt_at=true;
static char*opt_fi=NULL;
static char*opt_fo=NULL;

static i16 *mix_buf=NULL; /* mixing buffer */
static u16 mix_buf_len;
static u8 *out_buf=NULL; /* output buffer (both 8 and 16 bits) */
static u16 out_buf_size;

/****************************************************************************/

static void show_usage() {
  u16 base,max;
  printf("%s"NL,(char*)
    NAME " version " VERSION " - " DESCRIPTION);
  printf(
#if defined(__MINGW64__)
# if defined(__MINGW64_MAJOR_VERSION) && defined(__MINGW64_MINOR_VERSION)
    "Compiled with MinGW64 %hhu.%hhu on %s"NL,
    (u8)__MINGW64_MAJOR_VERSION,(u8)__MINGW64_MINOR_VERSION,
# elif defined(__MINGW32_MAJOR_VERSION) && defined(__MINGW32_MINOR_VERSION)
    "Compiled with MinGW64 %hhu.%hhu on %s"NL,
    (u8)__MINGW32_MAJOR_VERSION,(u8)__MINGW32_MINOR_VERSION,
# else
    "Compiled with MinGW64 %s on %s"NL,
    (char*)__VERSION__,
# endif
#elif defined(__MINGW32__)
# if defined(__MINGW32_MAJOR_VERSION) && defined(__MINGW32_MINOR_VERSION)
    "Compiled with MinGW32 %hhu.%hhu on %s"NL,
    (u8)__MINGW32_MAJOR_VERSION,(u8)__MINGW32_MINOR_VERSION,
# else
    "Compiled with MinGW32 %s on %s"NL,
    (char*)__VERSION__,
# endif
#elif defined(__GNUC__)
    "Compiled with GCC %s on %s"NL,
    (char*)__VERSION__,
#elif defined(__WATCOMC__)
    "Compiled with Watcom C %hhu.%hhu on %s"NL,
    (u8)((u16)__WATCOMC__/100),(u8)((u16)__WATCOMC__%100),
#else
    "Compiled on %s"NL,
#endif
    (char*)__DATE__ ", " __TIME__);
  printf("%s",(char*)
    COPYRIGHT NL
    LICENSE NL
    "Home page: <" HOMEPAGE ">"NL
    NL);
  printf("%s",(char*)
    "Usage:"NL
    "  " NAME " [options] [--] INPUT OUTPUT"NL
    "here:"NL
    "  options  any combination of options (see below)"NL
    "  --       stop parsing options and treat all arguments as filenames"NL
    "  INPUT    input Amiga MOD music file"NL
    "  OUTPUT   output Microsoft PCM Wave audio file"NL);
  printf("%s",(char*)
    "options [current value]:"NL
    "  -h    show help and exit"NL);
  printf(
    "  -a N  set Amiga base clock: 0=PAL,1=NTSC [%hhu]"NL,
    (u8)opt_a);
  printf(
#ifdef __WATCOMC__
    "  -r N  set output rate in Hertz: %lu-%lu [%lu]"NL,
#else /* !defined(__WATCOMC__) */
    "  -r N  set output rate in Hertz: %u-%u [%u]"NL,
#endif /* !defined(__WATCOMC__) */
    (u32)RATE_MIN,(u32)RATE_MAX,(u32)opt_r);
  printf(
    "  -c N  set output channels: 1,2 [%hhu]"NL,
    (u8)opt_c);
  printf(
    "  -b N  set output bits: 8,16 [%hhu]"NL,
    (u8)opt_b);
  printf(
    "  -i N  set sample interpolation: 0=no,1=linear,2=cubic [%hhu]"NL,
    (u8)opt_i);
  if(opt_mq) {
    base=AMP_Q_BASE;
    max=AMP_Q_MAX;
  } else {
    base=AMP_BASE;
    max=AMP_MAX;
  }
  printf(
    "  -v N  set output volume: 0-%hu (0=auto,%hu=100%%) [%hu]"NL,
    (u16)max,(u16)base,(u16)opt_v);
  printf(
    "  -mb N set mixing buffer length: %hu-%hu [%hu]"NL,
    (u16)MIXBUF_LEN_MIN,(u16)MIXBUF_LEN_MAX,(u16)opt_mb);
  printf(
    "  -mq N set mixing quality: 0=low,1=normal [%hhu]"NL,
    (u8)(opt_mq?1:0));
  printf(
    "  -vt N use volume table: 0=no,1=yes [%hhu]"NL,
    (u8)(opt_vt?1:0));
  printf(
    "  -at N use amplification table: 0=no,1=yes [%hhu]"NL,
    (u8)(opt_at?1:0));
}

static const char *HELP_HINT="Use `-h' to get help.";

/****************************************************************************/

static void error_novalue(const char*name) {
  error("Missing value for option `%s'. %s",(char*)name,(char*)HELP_HINT);
}

static void error_badopt(const char*name) {
  error("Unknown option specified: `%s'. %s",(char*)name,(char*)HELP_HINT);
}

static void error_extrafile(const char*arg) {
  error("Extra filename specified: `%s'. %s",(char*)arg,(char*)HELP_HINT);
}

static void error_no(const char*name) {
  error("No %s specified. %s",(char*)name,(char*)HELP_HINT);
}

static void error_initfailed(const char*name) {
  error("Failed to initialize %s",(char*)name);
}

/****************************************************************************/

/* Returns `false' on success */
static bool read_value(i32*var,const char*name,i32 min,i32 max,const char*arg) {
  char*endptr;
  i32 x;
  errno=0;
  x=strtol(arg,&endptr,0);
  if(errno||(*endptr!='\0')) {
    error("Invalid number `%s' given for option `%s'.",
      (char*)arg,(char*)name);
    return true;
  }
  if((x<min)||(x>max)) {
    error("Value `%s' (that is %ld) is out of range [%ld;%ld] for option `%s'.",
      (char*)arg,(long)x,(long)min,(long)max,(char*)name);
    return true;
  }
  *var=x;
  return false;
}

/****************************************************************************/

/* Returns `false' on success */
static bool parse_args(int argc,char **argv) {
  int i=1;
  i32 x;
  bool stopopts=false;
  while(i<argc) {
    bool f=false; /* filename flag */
    if(stopopts) f=true;
    else {
      if(!strcmp(argv[i],"-h")) { opt_h=true; i++;  }
      else if(!strcmp(argv[i],"-a")) {
        i++; if(i==argc) { error_novalue("-a"); return true; }
        if(read_value(&x,"-a",0,AMIGA_MAX,argv[i])) return true;
        opt_a=x; i++;
      } else if(!strcmp(argv[i],"-r")) {
        i++; if(i==argc) { error_novalue("-r"); return true; }
        if(read_value(&x,"-r",RATE_MIN,RATE_MAX,argv[i])) return true;
        opt_r=x; i++;
      } else if(!strcmp(argv[i],"-c")) {
        i++; if(i==argc) { error_novalue("-c"); return true; }
        if(read_value(&x,"-c",1,2,argv[i])) return true;
        opt_c=x; i++;
      } else if(!strcmp(argv[i],"-b")) {
        i++; if(i==argc) { error_novalue("-b"); return true; }
        if(read_value(&x,"-b",8,16,argv[i])) return true;
        if((x!=8)&&(x!=16)) {
          error("Option `%s' accepts only values 8 or 16","-b");
          return true;
        }
        opt_b=x; i++;
      } else if(!strcmp(argv[i],"-i")) {
        i++; if(i==argc) { error_novalue("-i"); return true; }
        if(read_value(&x,"-i",0,2,argv[i])) return true;
        opt_i=x; i++;
      } else if(!strcmp(argv[i],"-v")) {
        i++; if(i==argc) { error_novalue("-v"); return true; }
        if(read_value(&x,"-v",0,opt_mq?AMP_Q_MAX:AMP_MAX,argv[i])) return true;
        opt_v=x; i++;
      } else if(!strcmp(argv[i],"-mq")) {
        i++; if(i==argc) { error_novalue("-mq"); return true; }
        if(read_value(&x,"-mq",0,1,argv[i])) return true;
        opt_mq=x; i++;
      } else if(!strcmp(argv[i],"-mb")) {
        i++; if(i==argc) { error_novalue("-mb"); return true; }
        if(read_value(&x,"-mb",MIXBUF_LEN_MIN,MIXBUF_LEN_MAX,argv[i])) return true;
        opt_mb=x; i++;
      } else if(!strcmp(argv[i],"-vt")) {
        i++; if(i==argc) { error_novalue("-vt"); return true; }
        if(read_value(&x,"-vt",0,1,argv[i])) return true;
        opt_vt=x; i++;
      } else if(!strcmp(argv[i],"-at")) {
        i++; if(i==argc) { error_novalue("-at"); return true; }
        if(read_value(&x,"-at",0,1,argv[i])) return true;
        opt_at=x; i++;
      } else if(!strcmp(argv[i],"--")) { stopopts=true; i++; }
      else if(argv[i][0]=='-') { error_badopt(argv[i]); return true; }
      else f=true;
    }
    if(f) {
      if(!opt_fi) opt_fi=argv[i];
      else if(!opt_fo) opt_fo=argv[i];
      else { error_extrafile(argv[i]); return true; }
      i++;
    }
  }
  return false;
}

/****************************************************************************/

static bool init_mixbuf(u16 len) {
  if(!(mix_buf=malloc(sizeof(i16)*len))) return true;
  mix_buf_len=len;
  return false;
}

static void free_mixbuf() {
  if(mix_buf) {
    free(mix_buf);
    mix_buf=NULL;
  }
}

/****************************************************************************/

static bool init_outbuf(u16 size) {
  if(!(out_buf=malloc(size))) return true;
  out_buf_size=size;
  return false;
}

static void free_outbuf() {
  if(out_buf) {
    free(out_buf);
    out_buf=NULL;
  }
}

/****************************************************************************/

static int write_song(const char*filename,u16 amp,bool at) {
  union {
    ERR wave;
    int status;
  } err;
  u16 mixcount=(play.opts&PO_STEREO)?mix_buf_len>>1:mix_buf_len;
  err.wave=wave_create(filename,play.rate,
    (play.opts&PO_STEREO)?2:1,
    (play.opts&PO_16BITS)?16:8
    );
  if(err.wave!=E_OK) {
    error("%s",(char*)get_error(err.wave));
    err.status=-1;
    goto _stop;
  }
  play_start();
  while(play.flags&PF_PLAYING) {
    u16 count,len,size;
    count=play_fill(mix_buf,mixcount); if(!count) break;
    len=count;
    if(play.opts&PO_STEREO) len<<=1;
    if(play.mq) {
      if(play.opts&PO_16BITS) {
        if(at)
          amplify2_at_q(mix_buf,len,(i16*)out_buf);
        else
          amplify2_q(mix_buf,len,amp,(i16*)out_buf);
      } else {
        if(at)
          amplify_at_q(mix_buf,len,(u8*)out_buf);
        else
          amplify_q(mix_buf,len,amp,(u8*)out_buf);
      }
    } else {
      if(play.opts&PO_16BITS) {
        if(at)
          amplify2_at(mix_buf,len,(i16*)out_buf);
        else
          amplify2(mix_buf,len,amp,(i16*)out_buf);
      } else {
        if(at)
          amplify_at(mix_buf,len,(u8*)out_buf);
        else
          amplify(mix_buf,len,amp,(u8*)out_buf);
      }
    }
    size=len;
    if(play.opts&PO_16BITS) size<<=1;
    err.wave=wave_write(out_buf,size);
    if(err.wave!=E_OK) {
      error("%s",(char*)get_error(err.wave));
      err.status=-1;
      goto _stop;
    }
#if 0
    if(wave_written()>=1024*1024*1024) {
      error("1GB OF WAVE DATA LIMIT REACHED");
      err.status=-1;
      goto _stop;
    }
#endif /* disabled */
  }
  err.status=0;
_stop:
  wave_close();
  return err.status;
}

/****************************************************************************/

#ifdef __WATCOMC__
void exit_program() {
#else /* !defined(__WATCOMC__) */
static void exit_program() {
#endif /* !defined(__WATCOMC__) */
  play_free();
  song_free();
  if(mix_buf) free_mixbuf();
  if(out_buf) free_outbuf();
  if(Interptab) free_Interptab();
  if(Interptab_q) free_Interptab_q();
  if(Interptab2) free_Interptab2();
  if(Interptab2_q) free_Interptab2_q();
  if(Voltab) free_Voltab();
  if(Voltab_q) free_Voltab_q();
  if(Amptab) free_Amptab();
  if(Amptab2) free_Amptab2();
  if(Amptab_q) free_Amptab_q();
}

/****************************************************************************/

int main(int argc,char **argv) {
  u8 st=1; /* status */
  union { ERR mod; bool b; } err;
  u8 ovs;
  u16 amp;
  if(atexit(exit_program)) return 1;
  if(parse_args(argc,argv)) return 1;
  if(opt_h) { show_usage(); return 0; }
  if(!opt_fi) { error_no("input filename"); return 1; }
  if(!opt_fo) { error_no("output filename"); return 1; }
#if DEBUG
  DBG_PRINTF("opt_r=%u"NL,(u32)opt_r);
  DBG_PRINTF("opt_c=%hhu"NL,(u8)opt_c);
  DBG_PRINTF("opt_b=%hhu"NL,(u8)opt_b);
  DBG_PRINTF("opt_i=%hhu"NL,(u8)opt_i);
  DBG_PRINTF("opt_v=%hu"NL,(u16)opt_v);
  DBG_PRINTF("opt_mb=%hu"NL,(u16)opt_mb);
  DBG_PRINTF("opt_mq=%hhu"NL,(u8)opt_mq);
  DBG_PRINTF("opt_vt=%hhu"NL,(u8)opt_vt);
  DBG_PRINTF("opt_at=%hhu"NL,(u8)opt_at);
  DBG_PRINTF("opt_fi=%s"NL,(char*)opt_fi);
  DBG_PRINTF("opt_fo=%s"NL,(char*)opt_fo);
#endif /* DEBUG */
  if((err.mod=mod_load(opt_fi))!=E_OK) {
    error("%s",get_error(err.mod));
    goto _err;
  }
  DBG_DUMP_SONG();
  if(init_mixbuf(opt_mb)) {
    error_initfailed("mixing buffer");
    goto _err;
  }
  if(init_outbuf((opt_b==16)?opt_mb<<1:opt_mb)) {
    error_initfailed("output buffer");
    goto _err;
  }
  switch(opt_i) {
  default:
  case 0: err.b=false; break;
  case 1: if(opt_mq) err.b=init_Interptab_q(); else err.b=init_Interptab(); break;
  case 2: if(opt_mq) err.b=init_Interptab2_q(); else err.b=init_Interptab2(); break;
  }
  if(err.b) {
    error_initfailed("interpolation table");
    goto _err;
  }
  if(!opt_v) {
    u16 v=opt_mq?AMP_Q_BASE:AMP_BASE;
    switch(song.num_channels) {
    case 1: opt_v=v; break;
    case 2: opt_v=(opt_c==2)?v:v/4*3; break;
    default: opt_v=v/4*3; break;
    }
  }
  if(opt_mq) {
    ovs=calc_ovs(song.num_channels);
    if(opt_vt) err.b=init_Voltab_q(ovs); else err.b=false;
  } else {
    ovs=0;
    if(opt_vt) err.b=init_Voltab(); else err.b=false;
  }
  if(err.b) {
    error_initfailed("volume table");
    goto _err;
  }
  amp=opt_v*opt_c;
  DBG_PRINTF("amp=%hu"NL,(u16)amp);
  if(opt_at) {
    if(opt_mq) err.b=init_Amptab_q(amp);
    else {
      if(opt_b==16) err.b=init_Amptab2(amp); else err.b=init_Amptab(amp);
    }
    if(err.b) {
      error_initfailed("amplification table");
      goto _err;
    }
  }
  if(play_init(opt_a,opt_r,opt_c==2,opt_b==16,opt_i,opt_vt,opt_mq,ovs,false)) {
    error_initfailed("player");
    goto _err;
  }
  if(write_song(opt_fo,amp,opt_at)) goto _err;
  st=0;
_err:
  play_free();
  song_free();
  free_mixbuf();
  free_outbuf();
  switch(opt_i) {
  default:
  case 0: break;
  case 1: if(opt_mq) free_Interptab_q(); else free_Interptab(); break;
  case 2: if(opt_mq) free_Interptab2_q(); else free_Interptab2(); break;
  }
  if(opt_mq) {
    if(opt_vt) free_Voltab_q();
  } else {
    if(opt_vt) free_Voltab();
  }
  if(opt_at) {
    if(opt_mq) free_Amptab_q();
    else {
      if(opt_b==16) free_Amptab2(); else free_Amptab();
    }
  }
  return st;
}
