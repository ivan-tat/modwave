/*
 * play.c - playing routines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdbool.h>
#include <stdio.h> /* printf() */
#include <stdlib.h> /* rand() */
#include <string.h> /* memset() */
#include "Defines.h"
#include "common.h"
#include "debug.h"
#include "mix.h"
#include "mixq.h"
#include "mixl.h"
#include "mixlq.h"
#include "mixc.h"
#include "mixcq.h"
#include "play.h"
#include "song.h"
#include "tables.h"
#include "volq.h"

/*
 * Amiga machines features:
 *  PAL  clock = 7093789.2 Hz
 *  NTSC clock = 7159090.5 Hz
 *  C2 note period for both types = 428
 * Actual values (doubled period used to calculate final frequency):
 *  C2 note PAL  frequency = 7093789.2/2/428 = 8287.1369 Hz
 *  C2 note NTSC frequency = 7159090.5/2/428 = 8363.4235 Hz
 * Using 4 times finer values in fixed point 32.16 format for sample's step
 * calculation (divide it with output sample rate and 4 times finer note
 * period):
 *  PAL  value = 4*7093789.2/2*65536 = 929797138022.4 = D87C3A6666h
 *  NTSC value = 4*7159090.5/2*65536 = 938356310016   = DA7A650000h
 */

#define NOTEBASE_PAL 0xD87C3A6666
#define NOTEBASE_NTSC 0xDA7A650000

struct play_t play;

/****************************************************************************/

#if DEBUG_PLAY

u8 get_channel_number(const struct chn_t*c) {
  return (size_t)(c-play.channels)/(&play.channels[1]-play.channels);
}

static void DBG_DUMP_ROW_EVENTS() {
  int i=0;
  if(play.rowflags&RF_NEXTROW) {
    DBG_LOG("NextRow");
    i++;
  }
  if(play.rowflags&RF_NEXTPOS) {
    if(i) DBG_LOG(" ");
    DBG_LOG("NextPos");
    i++;
  }
  if(play.rowflags&RF_SETROW) {
    if(i) DBG_LOG(" ");
    DBG_PRINTF("SetRow=%hhu",(u8)play.newrow);
    i++;
  }
  if(play.rowflags&RF_SETPOS) {
    if(i) DBG_LOG(" ");
    DBG_PRINTF("SetPos=%hhu",(u8)play.newpos);
    i++;
  }
  if(play.rowflags&RF_SETTEMPO) {
    if(i) DBG_LOG(" ");
    DBG_PRINTF("SetTempo=%hhu",(u8)play.newtempo);
    i++;
  }
  if(play.rowflags&RF_SETTPR) {
    if(i) DBG_LOG(" ");
    DBG_PRINTF("SetTPR=%hhu",(u8)play.newtpr);
    i++;
  }
  if(!i) DBG_LOG("-");
}

static void DBG_DUMP_STATE(const char*prefix) {
  DBG_PRINTF("%sposition: [",(char*)prefix);
  if(play.pos>=0) DBG_PRINTF("O%03hhu",(u8)play.pos); else DBG_LOG("O---");
  DBG_PRINTF("/%03hhu:",(u8)song.order_len);
  if(play.patnum>=0) DBG_PRINTF("P%03hhu:",(u8)play.patnum);
  else DBG_LOG("P---:");
  if(play.row>=0) {
    DBG_PRINTF("R%02hhu(",(u8)play.row);
    if(play.patdly) DBG_PRINTF("+%hhX):",(u8)play.patdly);
    else DBG_LOG("--):");
  } else DBG_LOG("R--(--):");
  DBG_PRINTF("T%03hhu/%03hhu (0x%04hX.%02hhX/0x%04hX.%02hhX) tempo=%03hhu)",
    (u8)play.tick,
    (u8)play.tpr,
    (u16)play.sptleft,
    (u8)play.sptleftf,
    (u16)play.spt,
    (u8)play.sptf,
    (u8)play.tempo);
  DBG_LOG("] global flags: [");
  DBG_LOG(play.flags&PF_PLAYING?"P":"-");
  if(play.flags&PF_PLOOP) {
    DBG_PRINTF(" PL(R%02hhu,L%02hhu)",(u8)play.ploopto,(u8)play.ploops);
  } else {
    DBG_LOG(" --");
  }
  DBG_LOG((play.opts&PO_LOOP)?" L":" -");
  DBG_LOG("] tick flags: [");
  DBG_LOG((play.rowflags&RF_FIRSTTICK)?"FT":"--");
  DBG_LOG("] row events: [");
  DBG_DUMP_ROW_EVENTS();
  DBG_LOG("]"NL);
}

static void DBG_DUMP_CHN_FLAGS(const struct chn_t*c) {
  DBG_LOG((c->flags&CF_PLAYING)?"P":"-");
  DBG_LOG((c->flags&CF_CONTFX)?" CE":" --");
  DBG_LOG((c->flags&CF_SLTNFX)?" SLTN":" ----");
  DBG_LOG((c->flags&CF_NEWNOTE)?" NN":" --");
}

static void DBG_DUMP_CHN_STATE(const struct chn_t*c) {
  char s[4];
  if(c->smpnum) DBG_PRINTF("%02hhu",(u8)c->insnum); else DBG_LOG("--");
  DBG_PRINTF(" %s/%04hX/%04hX %02hhu",
    (char*)DBG_GET_NOTE_NAME(s,c->note),
    (u16)c->period,
    (u16)play.oldperiod,
    (u8)c->vol);
  if(c->fx) DBG_PRINTF(" %hhX%02hhX",(u8)c->fx-1,(u8)c->parm);
  else DBG_LOG(" ---");
}

static void DBG_DUMP_CHN_SAMPLE(const struct chn_t*c) {
  if(c->smpnum) DBG_PRINTF("N%02hhu",(u8)c->smpnum); else DBG_LOG("N--");
  if(c->smpdata&&c->smpllen)
    DBG_PRINTF(" L%05X-%05X",(u32)c->smplbeg,(u32)c->smplbeg+c->smpllen);
  else DBG_LOG(" L-----------");
  if(c->smpdata)
    DBG_PRINTF(" T%04hX S%04hX.%04hX P%05X.%04hX/%05X",
      (u16)c->period,
      (u16)((u32)c->smpstep>>16),
      (u16)c->smpstep&0xFFFF,
      (u32)c->smppos.i,
      (u16)c->smppos.f,
      (u32)c->smplen);
  else DBG_LOG(" T---- S----.---- P-----.----/-----");
}

static void DBG_DUMP_CHN_EVENTS() {
  char s[4];
  if(play.newins) DBG_PRINTF("%02hhu",(u8)play.newins); else DBG_LOG("--");
  DBG_PRINTF(" %s",(char*)DBG_GET_NOTE_NAME(s,play.newnote));
  if(play.newfx) DBG_PRINTF(" %hhX%02hhX",(u8)play.newfx-1,(u8)play.newparm);
  else DBG_LOG(" ---");
}

static void DBG_DUMP_FX(const struct chn_t*c) {
  DBG_DUMP_FX_PARM(c->fx,c->parm);
  switch(c->fx) {
  default: case 0: /* N/A */
  case FX_ARP: /* DBG_DUMP_FX */
  case FX_SLUP: /* DBG_DUMP_FX */
  case FX_SLDN: /* DBG_DUMP_FX */
  case FX_PAN: /* DBG_DUMP_FX */
  case FX_SAMOFS: /* DBG_DUMP_FX */
  case FX_VOLSL: /* DBG_DUMP_FX */
  case FX_JUMP: /* DBG_DUMP_FX */
  case FX_VOL: /* DBG_DUMP_FX */
  case FX_BREAK: /* DBG_DUMP_FX */
  case FX_SPEED: /* DBG_DUMP_FX */
    break;
  case FX_SLTN: /* DBG_DUMP_FX */
  case FX_SLTNVOLSL: /* DBG_DUMP_FX */
    DBG_PRINTF(": dstperiod=%04hX",(u16)c->dstperiod);
    break;
  case FX_VIB: /* DBG_DUMP_FX */
  case FX_VIBVOLSL: /* DBG_DUMP_FX */
    DBG_PRINTF(": dstperiod=%04hX vibwave=%hhX vibpos=%02hhX vibparm=%02hhX",
      (u16)c->dstperiod,
      (u8)c->vibwave,
      (u8)c->vibpos,
      (u8)c->vibparm);
    break;
  case FX_TREM: /* DBG_DUMP_FX */
    DBG_PRINTF(": basevol=%hhu tremwave=%hhX trempos=%02hhX tremparm=%02hhX",
      (u8)c->tremvol,
      (u8)c->tremwave,
      (u8)c->trempos,
      (u8)c->tremparm);
    break;
  case FX_MULTI: /* DBG_DUMP_FX */
    switch(c->parm>>4) {
    default: /* N/A */
    case FX_MULTI_0: /* DBG_DUMP_FX */
    case FX_MULTI_FSLUP: /* DBG_DUMP_FX */
    case FX_MULTI_FSLDN: /* DBG_DUMP_FX */
    case FX_MULTI_3: /* DBG_DUMP_FX */
    case FX_MULTI_VIBWAVE: /* DBG_DUMP_FX */
    case FX_MULTI_FINETUNE: /* DBG_DUMP_FX */
    case FX_MULTI_PATLOOP: /* DBG_DUMP_FX */
    case FX_MULTI_TREMWAVE: /* DBG_DUMP_FX */
    case FX_MULTI_8: /* DBG_DUMP_FX */
    case FX_MULTI_FVOLSLUP: /* DBG_DUMP_FX */
    case FX_MULTI_FVOLSLDN: /* DBG_DUMP_FX */
    case FX_MULTI_PATDLY: /* DBG_DUMP_FX */
    case FX_MULTI_15: /* DBG_DUMP_FX */
      break;
    case FX_MULTI_RETRIG: /* DBG_DUMP_FX */
    case FX_MULTI_NOTECUT: /* DBG_DUMP_FX */
    case FX_MULTI_NOTEDLY: /* DBG_DUMP_FX */
      DBG_PRINTF(": counter=%hhu",(u8)c->dlyticks);
      break;
    }
    break;
  }
}

static void DBG_DUMP_CHN(const struct chn_t*c,const char*prefix) {
  DBG_PRINTF("%schannel #%hhu events: [",
    (char*)prefix,
    (u8)get_channel_number(c));
  DBG_DUMP_CHN_EVENTS();
  DBG_LOG("] flags: [");
  DBG_DUMP_CHN_FLAGS(c);
  DBG_LOG("] state: [");
  DBG_DUMP_CHN_STATE(c);
  DBG_LOG("] sample: [");
  DBG_DUMP_CHN_SAMPLE(c);
  DBG_LOG("] effect: [");
  DBG_DUMP_FX(c);
  DBG_LOG("]"NL);
}

static void DBG_DUMP_CHN_STATE_AND_EVENTS(const struct chn_t*c,
const char*prefix) {
  DBG_PRINTF("%schannel #%hhu flags: [",
    (char*)prefix,
    (u8)get_channel_number(c));
  DBG_DUMP_CHN_FLAGS(c);
  DBG_LOG("] state: [");
  DBG_DUMP_CHN_STATE(c);
  DBG_LOG("] effect: [");
  DBG_DUMP_FX(c);
  DBG_LOG("] events: [");
  DBG_DUMP_CHN_EVENTS();
  DBG_LOG("/");
  DBG_DUMP_FX_PARM(play.newfx,play.newparm);
  DBG_LOG("]"NL);
}

static void DBG_DUMP_PERIOD_STEP(u16 period,u8 tune,u32 step) {
  DBG_PRINTF("p=0x%04hX ft=%hhd s=%04hX.%04hX"NL,
    (u16)period,
    (i8)(tune^8)-8,
    (u16)((u32)step>>16),
    (u16)step);
}

static void DBG_FX_ACTION(u8 fx,u8 parm,const char*action) {
  DBG_PRINTF("%s: %s"NL,(char*)DBG_GET_FX_NAME(fx,parm),(char*)action);
}

static void DBG_MIXSMP(const struct chn_t*c,u16 count) {
  u32 end=c->smpllen?c->smplbeg+c->smpllen:c->smplen;
  union smppos64_u n;

  n.pos=c->smppos; n.w[3]=0;
  n.q+=c->smpstep*(count-1);
  if(n.pos.i>=end) {
    DBG_PRINTF(
      "ERROR: last position in sample is outside %s, "
      "slen=%08X sloop=%08X-%08X "
      "start=%08X.%04hX step=%04hX.%04hX count=%04hX last=%08X.%04hX"NL,
      (char*)(c->smpllen?"loop":"end"),
      (u32)c->smplen,
      (u32)c->smplbeg,
      (u32)c->smplbeg+c->smpllen,
      (u32)c->smppos.i,
      (u16)c->smppos.f,
      (u16)(c->smpstep>>16),
      (u16)(c->smpstep&0xFFFF),
      (u16)count,
      (u32)n.pos.i,
      (u16)n.pos.f);
  }
}

#else /* !DEBUG_PLAY */

#define get_channel_number(c) 0
#define DBG_DUMP_STATE(x)
#define DBG_DUMP_CHN(a,b)
#define DBG_DUMP_CHN_STATE_AND_EVENTS(a,b)
#define DBG_DUMP_CHN_SAMPLE(x)
#define DBG_DUMP_PERIOD_STEP(a,b,c)
#define DBG_FX_ACTION(a,b,c)
#define DBG_MIXSMP(c,count)

#endif /* !DEBUG_PLAY */

/****************************************************************************/

static void set_rate(amiga_t clock,u32 rate) {
  u64 base,x;
  u8 i;
  switch(clock) {
  default:
  case AMIGA_PAL: base=NOTEBASE_PAL; break;
  case AMIGA_NTSC: base=NOTEBASE_NTSC; break;
  }
  play.rate=rate;
  play.sptbase=(rate<<8)*5/2;
  play.Notebase[0]=(u64)base/rate;
  x=(base<<15)/rate;
  for(i=1;i<16;i++) play.Notebase[i]=x/Finetune[i];
}

/****************************************************************************/

static void set_default_panning() {
  /* set default panning positions */
  if(play.opts&PO_STEREO) {
    struct chn_t*c=play.channels;
    if(song.num_channels==1) c->pan=SONG_PAN_C;
    else {
      u8 i;
      for(i=1;i<=song.num_channels;i++) {
        c->pan=(i&2)?SONG_PAN_C+(SONG_PAN_C/8*5):SONG_PAN_C-(SONG_PAN_C/8*5);
        c++;
      }
    }
  } else {
    struct chn_t*c=play.channels;
    u8 n;
    for(n=song.num_channels;n;n--) { c->pan=SONG_PAN_C; c++; }
  }
}

/****************************************************************************/

/* mixprocs[mq][interp][vt][stereo] */
static void (*_mixprocs[2][INTERP_MAX+1][2][2])(
  const struct chn_t*c,u16 count,i16*buf)={
  {
    {
      { mix_sample_mono,
        mix_sample_stereo },
      { mix_sample_mono_vt,
        mix_sample_stereo_vt }
    }, {
      { mix_sample_linear_mono,
        mix_sample_linear_stereo },
      { mix_sample_linear_mono_vt,
        mix_sample_linear_stereo_vt }
    }, {
      { mix_sample_cubic_mono,
        mix_sample_cubic_stereo },
      { mix_sample_cubic_mono_vt,
        mix_sample_cubic_stereo_vt }
    }
  }, {
    {
      { mix_sample_mono_q,
        mix_sample_stereo_q },
      { mix_sample_mono_vt_q,
        mix_sample_stereo_vt_q }
    }, {
      { mix_sample_linear_mono_q,
        mix_sample_linear_stereo_q },
      { mix_sample_linear_mono_vt_q,
        mix_sample_linear_stereo_vt_q }
    }, {
      { mix_sample_cubic_mono_q,
        mix_sample_cubic_stereo_q },
      { mix_sample_cubic_mono_vt_q,
        mix_sample_cubic_stereo_vt_q }
    }
  }
};

static void set_mixproc() {
  play.mixproc=
    _mixprocs[play.mq][play.interp][play.vt?1:0][(play.opts&PO_STEREO)?1:0];
}

/****************************************************************************/

/* In: (play.opts&PO_STEREO)!=0 */
static void update_fvol(struct chn_t*c) {
  if(c->vol) {
    switch(c->pan) {
    case SONG_PAN_L: c->fvol[1]=0; break;
    case SONG_PAN_C: c->fvol[1]=c->vol>>1; break;
    case SONG_PAN_R: c->fvol[1]=c->vol; break;
    default:         c->fvol[1]=((u16)c->vol*c->pan)/SONG_PAN_R; break;
    }
    c->fvol[0]=c->vol-c->fvol[1];
  } else {
    c->fvol[0]=c->fvol[1]=0;
  }
}

/****************************************************************************/

static void change_vol(struct chn_t*c,i8 vol) {
  vol=(vol<0)?0:(vol>SONG_VOL_MAX)?SONG_VOL_MAX:vol;
  if(c->vol!=vol) {
    c->vol=vol;
    if(play.opts&PO_STEREO) update_fvol(c);
  }
}

/****************************************************************************/

static void change_pan(struct chn_t*c,int pan) {
  pan=(pan<0)?0:(pan>SONG_PAN_R)?SONG_PAN_R:pan;
  if(c->pan!=pan) {
    c->pan=pan;
    if(play.opts&PO_STEREO) update_fvol(c);
  }
}

/****************************************************************************/

static void set_sample(struct chn_t*c,u8 smpnum) {
  bool set=false;

  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_PRINTF("smpnum=%hhu"NL,(u8)smpnum);
  }
  c->smpnum=smpnum;
  if(smpnum) {
    struct ins_t*i=&song.instruments[smpnum-1];
    if(i->data) {
      c->smptune=i->tune;
      c->smplen=i->len;
      c->smplbeg=i->lbeg;
      c->smpllen=i->llen;
      c->smpdata=i->data;
      set=true;
    }
  }
  if(!set) {
    c->smptune=0;
    c->smplen=0;
    c->smplbeg=0;
    c->smpllen=0;
    c->smpdata=NULL;
  }
  if(DEBUG_PLAY) {
    DBG_LOG("sample: [");
    DBG_DUMP_CHN_SAMPLE(c);
    DBG_LOG("]"NL);
    DBG_LEAVE();
  }
}

/****************************************************************************/

/* In: 0<=st<=15 */
static u16 calc_semitone_period(UNUSED_PARAM const struct chn_t*c,u16 period,
u16 limit,u8 st) {
  UNUSED_HINT(c);
  if(st) period=(u32)(((u32)Semitones[st]+1)*period)>>16;
  if(period<limit) period=0;
  return period;
}

/****************************************************************************/

/* In: period>0 */
static u32 calc_period_step(const struct chn_t*c,u16 period) {
  u32 step;

  if(DEBUG_PLAY) { DBG_ENTER(); }
  step=play.Notebase[c->smptune]/period;
  if(DEBUG_PLAY) {
    DBG_DUMP_PERIOD_STEP(period,c->smptune,step);
    DBG_LEAVE();
  }
  return step;
}

/****************************************************************************/

/* Play note flags */
#define PNF_I       (1<<0) /* set instrument */
#define PNF_N       (1<<1) /* set note */
#define PNF_S       (1<<2) /* set sample */
#define PNF_UPDATE  (1<<3) /* update sample step */
#define PNF_REWIND  (1<<4) /* rewind sample */
#define PNF_ADJPOS  (1<<5) /* adjust sample position */
#define PNF_START   (1<<6) /* start play */
#define PNF_STOP    (1<<7) /* stop play */

/* In: play.newins||play.newnote */
static void play_note(struct chn_t*c) {
  u8 insnum,smpnum; /* new or active */
  u8 f=0; /* flags */

  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"-> ");
  }

  /* Check new instrument */
  if(play.newins) {
    insnum=play.newins;
    if((!c->insnum)||(c->insnum!=insnum)) {
      c->insnum=insnum;
      f|=PNF_I;
    }
    change_vol(c,song.instruments[insnum-1].vol);
  } else {
    insnum=c->insnum;
  }

  /* Check new note */
  if(play.newnote) {
    c->note=play.newnote;
    c->period=Note_periods[play.newnote-1];
    f|=PNF_N;
  }

  /* Check new/active sample */
  smpnum=insnum;
  if(!(smpnum&&song.instruments[smpnum-1].data)) smpnum=0;

  if(play.newins) {
    if(play.newnote) {
      /* I+,N+ */
      if(c->flags&CF_PLAYING) {
        if(smpnum) {
          if(smpnum==c->smpnum) {
            /* P+,I+,N+: S+,S==s - Rewind (play new note) if no slide */
            if(!(c->flags&CF_SLTNFX)) f|=PNF_REWIND|PNF_UPDATE;
          } else {
            /* P+,I+,N+: S+,S!=s - Play new note: set it and rewind */
            f|=PNF_S|PNF_REWIND|PNF_UPDATE;
          }
        } else {
          /* P+,I+,N+: S- - Stop it */
          f|=PNF_STOP;
        }
      } else {
        if(smpnum) {
          /* P-,I+,N+: S+ - Play new note: set it and play */
          f|=PNF_S|PNF_START|PNF_UPDATE;
        } else {
          /* P-,I+,N+: S- - Do nothing */
        }
      }
    } else {
      /* I+,N- */
      if(c->flags&CF_PLAYING) {
        if(smpnum) {
          if(smpnum==c->smpnum) {
            /* P+,I+,N-: S+,S==s - Update sample step if no slide to note */
            if(!(c->flags&CF_SLTNFX)) f|=PNF_UPDATE;
          } else {
            /* P+,I+,N-: S+,S!=s - Set it, no rewind (play new sample) */
            f|=PNF_S;
            if(c->smppos.i>=song.instruments[smpnum-1].len) {
              f|=PNF_STOP; /* Stop it */
            } else {
              /* Continue playing new sample from current position */
              f|=PNF_ADJPOS;
              /* Update sample step if no slide to note */
              if(!(c->flags&CF_SLTNFX)) f|=PNF_UPDATE;
            }
          }
        } else {
          /* P+,I+,N-: S- - Stop it */
          f|=PNF_STOP;
        }
      } else {
        if(smpnum) {
          if(smpnum==c->smpnum) {
            /* P-,I+,N-: S+,S==s - Do nothing */
          } else {
            /* P-,I+,N-: S+,S!=s - Set it, no rewind */
            f|=PNF_S|PNF_ADJPOS;
          }
        } else {
          /* P-,I+,N-: S- - Do nothing */
        }
      }
    }
  } else {
    if(play.newnote) {
      /* I-,N+ */
      if(c->flags&CF_PLAYING) {
        /* P+,I-,N+: s+ - Rewind if no slide */
        if(!(c->flags&CF_SLTNFX)) f|=PNF_REWIND|PNF_UPDATE;
      } else {
        /* P-,I-,N+ - Use active instrument/sample */
        if(smpnum) {
          /* P-,I-,N+: s+ - Play new note */
          f|=PNF_START|PNF_UPDATE;
        } else {
          /* P-,I-,N+: s- - Do nothing */
        }
      }
    }
  }

  if(f&PNF_S) set_sample(c,smpnum);
  if(f&PNF_ADJPOS) {
    if(c->smpllen) {
      u32 lend=c->smplbeg+c->smpllen;
      if(c->smppos.i>=lend)
        c->smppos.i=c->smplbeg+((c->smppos.i-lend)%c->smpllen);
    } else
      if(c->smppos.i>=c->smplen)
        f=(f&~(PNF_REWIND|PNF_ADJPOS|PNF_START))|PNF_STOP; /* Stop it */
  }
  if(f&(PNF_REWIND|PNF_START)) { c->smppos.f=0; c->smppos.i=0; }
  if(f&PNF_START) c->flags|=CF_PLAYING;
  else if(f&PNF_STOP) c->flags&=~CF_PLAYING;
  if(f&PNF_UPDATE) c->smpstep=calc_period_step(c,c->period);
  if(f&(PNF_N|PNF_UPDATE)) c->flags|=CF_NEWNOTE; else c->flags&=~CF_NEWNOTE;

  if(DEBUG_PLAY) {
    DBG_PRINTF("type=%s flags=[%c%c%c%c%c%c%c%c]"NL,
      (char*)((c->flags&CF_SLTNFX)?"slide":"normal"),
      (u8)((f&PNF_I)?'I':'-'),
      (u8)((f&PNF_N)?'N':'-'),
      (u8)((f&PNF_S)?'S':'-'),
      (u8)((f&PNF_REWIND)?'r':'-'),
      (u8)((f&PNF_ADJPOS)?'p':'-'),
      (u8)((f&PNF_START)?'R':'-'),
      (u8)((f&PNF_STOP)?'s':'-'),
      (u8)((f&PNF_UPDATE)?'U':'-'));
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"<- ");
    DBG_LEAVE();
  }
}

/****************************************************************************/

static void apply_row_effects() {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_STATE("-> ");
  }
  if(play.rowflags&(RF_SETTEMPO|RF_SETTPR)) {
    if(play.rowflags&RF_SETTEMPO) {
      u8 x=MAX(play.newtempo,SONG_TEMPO_MIN);
      if(play.tempo!=x) {
        u32 spt=play.sptbase/x;
        play.tempo=x;
        play.sptf=spt&0xFF;
        play.spt=spt>>8;
        /*play.rowflags&=~RF_SETTEMPO;*/
      }
    }
    if(play.rowflags&RF_SETTPR) {
      if(!play.newtpr) {
        play.flags&=~PF_PLAYING; /* stop */
        if(DEBUG_PLAY) { DBG_LOG("stop"NL); }
        goto _exit;
      }
      play.tpr=MIN(play.newtpr,SONG_TPR_MAX);
      /*play.rowflags&=~RF_SETTPR;*/
    }
    play.rowflags&=~(RF_SETTEMPO|RF_SETTPR); /* remove events */
  }
  if(DEBUG_PLAY) { DBG_DUMP_STATE("<- "); }
_exit:
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

#define set_fx(c) { c->fx=play.newfx; c->parm=play.newparm; }

/****************************************************************************/

static void do_slup(struct chn_t*c,u16 limit) {
  i16 x=c->period-((u16)c->slspeed<<2);

  if(DEBUG_PLAY) { DBG_ENTER(); }
  if(x<limit) x=limit;
  if(DEBUG_PLAY) {
    DBG_PRINTF("period=%04hX limit=%04hX x=%04hX"NL,
      (u16)c->period,
      (u16)limit,
      (u16)x);
  }
  if(c->period!=x) {
    c->period=x;
    c->smpstep=calc_period_step(c,x);
  }
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void do_sldn(struct chn_t*c,u16 limit) {
  u16 x=c->period+((u16)c->slspeed<<2);

  if(DEBUG_PLAY) { DBG_ENTER(); }
  if(x>limit) x=limit;
  if(DEBUG_PLAY) {
    DBG_PRINTF("period=%04hX limit=%04hX x=%04hX"NL,
      (u16)c->period,
      (u16)limit,
      (u16)x);
  }
  if(c->period!=x) {
    c->period=x;
    c->smpstep=calc_period_step(c,x);
  }
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void init_sltn(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  play.oldperiod=c->period;
  if(c->flags&CF_SLTNFX)
    c->period=c->dstperiod; /* effect is active: restore original period */
  else
    c->flags|=CF_SLTNFX; /* effect is not active: try to start */
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void start_sltn(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->dstperiod=c->period;
  if(play.oldperiod) c->period=play.oldperiod; /* restore old period */
  c->smpstep=calc_period_step(c,c->period);
  if(DEBUG_PLAY) {
    DBG_PRINTF("period=%04hX dstperiod=%04hX"NL,
      (u16)c->period,
      (u16)c->dstperiod);
    DBG_LEAVE();
  }
}

static void do_sltn(struct chn_t*c) {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_PRINTF("period=%04hX dstperiod=%04hX"NL,
      (u16)c->period,
      (u16)c->dstperiod);
  }
  if(c->period!=c->dstperiod) {
    if(c->period<c->dstperiod) do_sldn(c,c->dstperiod);
    else do_slup(c,c->dstperiod);
  }
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void stop_sltn(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->flags&=~CF_SLTNFX;
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static i8 get_wave_amplitude(u8 wf,u8 pos) {
  switch(wf&WF_WAVEMASK) {
  default:
  case WF_SINE: return Sinewave[pos&63];
  case WF_RAMPDN: return 32-(pos&63);
  case WF_SQUARE: return 32-(pos&32);
  case WF_RANDOM: return 32-(rand()&63);
  }
}

static void start_vib(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->dstperiod=c->period;
  if(c->flags&CF_NEWNOTE) { if(!(c->vibwave&WF_NORETRIG)) c->vibpos=0; }
  else c->flags|=CF_CONTFX;
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void do_vib(struct chn_t*c) {
  i16 x=c->dstperiod,d=get_wave_amplitude(c->vibwave,c->vibpos);

  if(DEBUG_PLAY) { DBG_ENTER(); }
  if(c->vibparm&0xF) {
    d=(d*(c->vibparm&0xF))>>2;
    x-=d;
    if(x<PERIOD_MIN) x=PERIOD_MIN; else if(x>PERIOD_MAX) x=PERIOD_MAX;
  } else
    if(DEBUG_PLAY) d=0;
  if(DEBUG_PLAY) {
    DBG_PRINTF("dstperiod=%04hX vibpos=%hhu scaled_d=%hhd period=%04hX"NL,
      (u16)c->dstperiod,
      (u8)c->vibpos&63,
      (i8)d,
      (u16)x);
  }
  c->vibpos+=c->vibparm>>4;
  c->period=x;
  c->smpstep=calc_period_step(c,x);
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void stop_vib(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->period=c->dstperiod;
  if((c->flags&CF_PLAYING)&&c->period)
    c->smpstep=calc_period_step(c,c->period);
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void start_trem(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->tremvol=c->vol;
  if(c->flags&CF_NEWNOTE) { if(!(c->tremwave&WF_NORETRIG)) c->trempos=0; }
  else c->flags|=CF_CONTFX;
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void do_trem(struct chn_t*c) {
  i8 d=get_wave_amplitude(c->tremwave,c->trempos);

  if(DEBUG_PLAY) { DBG_ENTER(); }
  if(c->tremparm&0xF) {
    d=((i16)d*(c->tremparm&0xF))>>4;
    change_vol(c,c->tremvol+d);
  } else
    if(DEBUG_PLAY) d=0;
  if(DEBUG_PLAY) {
    DBG_PRINTF("basevol=%hhu trempos=%hhu scaled_d=%hhd newvol=%hhd"NL,
      (u8)c->tremvol,
      (u8)c->trempos&63,
      (i8)d,
      (i8)c->tremvol+d);
  }
  c->trempos+=c->tremparm>>4;
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void stop_trem(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->vol=c->tremvol;
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

static void do_volsl(struct chn_t*c) {
  u8 speed;
  if(DEBUG_PLAY) { DBG_ENTER(); }
  if((speed=c->parm>>4)!=0) change_vol(c,c->vol+speed);
  else if((speed=c->parm&0x0F)!=0) change_vol(c,c->vol-speed);
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/* In: c->flags&CF_PLAYING && c->dlyticks>0 && (c->parm&0xF)>0 */
static void do_retrig(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->dlyticks--;
  if(!c->dlyticks) { c->smppos.f=c->smppos.i=0; c->dlyticks=c->parm&0xF; }
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/* In: c->dlyticks>0 */
static void do_notecut(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->dlyticks--;
  if(!c->dlyticks) {
    if(c->vol) {
      c->vol=0;
      if(play.opts&PO_STEREO) update_fvol(c);
    }
  }
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/* In: c->dlyticks>0 */
static void do_notedly(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->dlyticks--;
  if(!c->dlyticks) {
    play.newins=c->dlyins;
    play.newnote=c->dlynote;
    play_note(c);
  }
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

/* In: play.newfx>0 */
static void initfx(struct chn_t*c) {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"-> ");
  }
  switch(play.newfx) {
  default: /* N/A */
  case FX_ARP: /* init (N/A) */
  case FX_SLUP: /* init (N/A) */
  case FX_SLDN: /* init (N/A) */
  case FX_VIB: /* init (N/A) */
  case FX_VIBVOLSL: /* init (N/A) */
  case FX_TREM: /* init (N/A) */
  case FX_PAN: /* init (N/A) */
  case FX_SAMOFS: /* init (N/A) */
  case FX_VOLSL: /* init (N/A) */
  case FX_VOL: /* init (N/A) */
    if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"no action"); }
    break;
  case FX_SLTN: /* init (prepare) */
  case FX_SLTNVOLSL: /* init (prepare) */
    init_sltn(c);
    if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"handled"); }
    break;
  case FX_JUMP: /* init (row) */
    set_fx(c);
    if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"started"); }
    play.newfx=0; /* remove event */
    break;
  case FX_BREAK: /* init (row) */
    set_fx(c);
    if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"started"); }
    play.newfx=0; /* remove event */
    break;
  case FX_MULTI: /* init */
    switch(play.newparm>>4) {
    default: /* N/A */
    case FX_MULTI_0: /* init (N/A) */
    case FX_MULTI_FSLUP: /* init (N/A) */
    case FX_MULTI_FSLDN: /* init (N/A) */
    case FX_MULTI_3: /* init (N/A) */
    case FX_MULTI_8: /* init (N/A) */
    case FX_MULTI_FVOLSLUP: /* init (N/A) */
    case FX_MULTI_FVOLSLDN: /* init (N/A) */
    case FX_MULTI_NOTECUT: /* init (N/A) */
    case FX_MULTI_FINETUNE: /* init (N/A) */
    case FX_MULTI_RETRIG: /* init (N/A) */
    case FX_MULTI_15: /* init (N/A) */
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"no action"); }
      break;
    case FX_MULTI_VIBWAVE: /* init (immediate) */
      c->vibwave=play.newparm&(WF_WAVEMASK|WF_NORETRIG);
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"started"); }
      play.newfx=0; /* remove event */
      break;
    case FX_MULTI_PATLOOP: /* init (immediate or on row end)*/
      if(!(play.rowflags&RF_PLOOP)) {
        if(!(play.newparm&0xF)) {
          play.ploopto=play.row+1;
          set_fx(c);
          if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"started"); }
        } else if(play.ploopto) {
          if(!(play.flags&PF_PLOOP)) {
            play.flags|=PF_PLOOP;
            play.ploops=(play.newparm&0xF)+1;
          }
          set_fx(c);
          if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"started"); }
        } else
          if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      play.newfx=0; /* remove event */
      break;
    case FX_MULTI_TREMWAVE: /* init (immediate) */
      c->tremwave=play.newparm&(WF_WAVEMASK|WF_NORETRIG);
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"started"); }
      play.newfx=0; /* remove event */
      break;
    case FX_MULTI_NOTEDLY: /* init (prepare) */
      if(play.newnote||play.newins) {
        c->dlyticks=(play.newparm&0xF)+1;
        c->dlynote=play.newnote;
        c->dlyins=play.newins;
        play.newnote=play.newins=0; /* remove events */
        set_fx(c);
        if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"started"); }
        play.newfx=0; /* remove event */
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      break;
    case FX_MULTI_PATDLY: /* init (row) */
      if(play.patdly<(play.newparm&0xF)) play.patdly=(play.newparm&0xF);
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"started"); }
      play.newfx=0; /* remove event */
      break;
    }
    break;
  case FX_SPEED: /* init (row) */
    if(play.newparm>=SONG_TEMPO_MIN) {
      play.newtempo=play.newparm;
      play.rowflags|=RF_SETTEMPO;
    } else {
      play.newtpr=play.newparm;
      play.rowflags|=RF_SETTPR;
    }
    set_fx(c);
    if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"started"); }
    play.newfx=0; /* remove event */
    break;
  }
  if(DEBUG_PLAY) {
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"<- ");
    DBG_LEAVE();
  }
}

/****************************************************************************/

/* In: play.newfx>0 */
static void startfx(struct chn_t*c) {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"-> ");
  }
  switch(play.newfx) {
  default: /* N/A */
  case FX_JUMP: /* start (N/A) */
  case FX_BREAK: /* start (N/A) */
  case FX_SPEED: /* start (N/A) */
    if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"no action"); }
    break;
  case FX_ARP: /* start */
    if(c->flags&CF_PLAYING) {
      c->arppos=0;
      if((c->arpparm!=play.newparm)
      || (c->arpins!=c->insnum)
      || (c->arpsmp!=c->smpnum)
      || (c->arptune!=c->smptune)
      || (c->arpperiod[0]!=c->period)) {
        u16 limit; /* minimal fine-tuned period (top note limit) */
        /* save state */
        c->arpvol=c->vol;
        c->arpparm=play.newparm;
        c->arpins=c->insnum;
        c->arpsmp=c->smpnum;
        c->arptune=c->smptune;
        /* save note #0 */
        c->arpperiod[0]=c->period;
        c->arpstep[0]=c->smpstep;
        limit=Note_periods[c->smptune*SONG_NOTES+SONG_NOTES-1];
        /* setup note #1 */
        if(play.newparm>>4) {
          c->arpperiod[1]=
            calc_semitone_period(c,c->arpperiod[0],limit,play.newparm>>4);
          if(c->arpperiod[1])
            c->arpstep[1]=calc_period_step(c,c->arpperiod[1]);
        } else {
          c->arpperiod[1]=c->arpperiod[0];
          c->arpstep[1]=c->arpstep[0];
        }
        /* setup note #2 */
        if(play.newparm&0xF) {
          c->arpperiod[2]=
            calc_semitone_period(c,c->arpperiod[0],limit,play.newparm&0xF);
          if(c->arpperiod[2])
            c->arpstep[2]=calc_period_step(c,c->arpperiod[2]);
        } else {
          c->arpperiod[2]=c->arpperiod[0];
          c->arpstep[2]=c->arpstep[0];
        }
      }
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
    break;
  case FX_SLUP: /* start */
  case FX_SLDN: /* start */
    if(!play.newparm) play.newparm=c->slspeed;
    if(c->flags&CF_PLAYING) {
      c->slspeed=play.newparm;
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
    break;
  case FX_SLTN: /* start */
    if(!play.newparm) play.newparm=c->slspeed;
    if((c->flags&CF_PLAYING)&&play.newparm) {
      c->slspeed=play.newparm;
      start_sltn(c);
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    } else {
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      if(c->flags&CF_SLTNFX) stop_sltn(c);
      c->fx=0;
    }
    break;
  case FX_VIB: /* start */
    if(!(play.newparm&0xF0)) play.newparm|=c->vibparm&0xF0;
    if(!(play.newparm&0x0F)) play.newparm|=c->vibparm&0x0F;
    if(c->flags&CF_PLAYING) {
      c->vibparm=play.newparm;
      start_vib(c);
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    } else {
      if(c->flags&CF_CONTFX) {
        c->flags&=~CF_CONTFX;
        stop_vib(c);
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
      }
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      c->fx=0;
    }
    break;
  case FX_SLTNVOLSL: /* start */
    if((c->flags&CF_PLAYING)&&(c->flags&CF_SLTNFX)) {
      start_sltn(c);
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    } else {
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      stop_sltn(c);
      c->fx=0;
    }
    break;
  case FX_VIBVOLSL: /* start */
    if(c->flags&CF_PLAYING) {
      start_vib(c);
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    } else {
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      stop_vib(c);
      c->fx=0;
    }
    break;
  case FX_TREM: /* start */
    if(!(play.newparm&0xF0)) play.newparm|=c->tremparm&0xF0;
    if(!(play.newparm&0x0F)) play.newparm|=c->tremparm&0x0F;
    if(c->flags&CF_PLAYING) {
      c->tremparm=play.newparm;
      start_trem(c);
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    } else {
      if(c->flags&CF_CONTFX) {
        c->flags&=~CF_CONTFX;
        stop_trem(c);
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
      }
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      c->fx=0;
    }
    break;
  case FX_PAN: /* start */
    change_pan(c,(play.newparm<=SONG_PAN_R)?play.newparm:SONG_PAN_C);
    set_fx(c);
    if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    break;
  case FX_SAMOFS: /* start */
    if(c->flags&CF_PLAYING) {
      u32 ofs=(u32)play.newparm<<8;
      if(c->smpllen) { /* Looped */
        u32 lend=c->smplbeg+c->smpllen;
        if(ofs>=lend) ofs=c->smplbeg+(lend-ofs)%c->smpllen;
        c->smppos.f=0;
        c->smppos.i=ofs;
      } else { /* Not looped */
        if(ofs<c->smplen) {
          c->smppos.f=0;
          c->smppos.i=ofs;
        } else
          c->flags&=~CF_PLAYING;
      }
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
    break;
  case FX_VOLSL: /* start */
    if(c->flags&CF_PLAYING) {
      set_fx(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
    break;
  case FX_VOL: /* start */
    change_vol(c,play.newparm);
    set_fx(c);
    if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
    break;
  case FX_MULTI: /* start */
    switch(play.newparm>>4) {
    default: /* N/A */
    case FX_MULTI_0: /* start (N/A) */
    case FX_MULTI_3: /* start (N/A) */
    case FX_MULTI_VIBWAVE: /* start (N/A) */
    case FX_MULTI_PATLOOP: /* start (N/A) */
    case FX_MULTI_TREMWAVE: /* start (N/A) */
    case FX_MULTI_8: /* start (N/A) */
    case FX_MULTI_PATDLY: /* start (N/A) */
    case FX_MULTI_15: /* start (N/A) */
      if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"no action"); }
      break;
    case FX_MULTI_FSLUP: /* start */
    case FX_MULTI_FSLDN: /* start */
      if(c->flags&CF_PLAYING) {
        c->slspeed=play.newparm&0xF;
        set_fx(c);
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
        if((play.newparm>>4)==FX_MULTI_FSLUP) do_slup(c,PERIOD_MIN);
        else do_sldn(c,PERIOD_MAX);
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      break;
    case FX_MULTI_FINETUNE: /* start */
      if(c->smpnum) {
        if(c->smptune!=(play.newparm&0xF)) {
          c->smptune=play.newparm&0xF;
          if(c->flags&CF_PLAYING) c->smpstep=calc_period_step(c,c->period);
        }
        set_fx(c);
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      break;
    case FX_MULTI_RETRIG: /* start */
      if((c->flags&CF_PLAYING)&&(play.newparm&0xF)) {
        if(c->flags&CF_CONTFX) c->parm=play.newparm; else set_fx(c);
        if(c->flags&CF_NEWNOTE) c->dlyticks=(play.newparm&0xF);
        else {
#if 1 /* Amiga */
          c->dlyticks=1; /* last tick */
#else /* Schism Tracker */
          if(!(c->flags&CF_CONTFX)) c->dlyticks=1; /* last tick */
#endif
          do_retrig(c);
        }
        if(c->flags&CF_CONTFX) {
          if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"continued"); }
        } else
          if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
      } else {
        if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
        c->fx=0;
      }
      break;
    case FX_MULTI_FVOLSLUP: /* start */
      if(c->flags&CF_PLAYING) {
        change_vol(c,c->vol+(play.newparm&0xF));
        set_fx(c);
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      break;
    case FX_MULTI_FVOLSLDN: /* start */
      if(c->flags&CF_PLAYING) {
        change_vol(c,c->vol-(play.newparm&0xF));
        set_fx(c);
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      break;
    case FX_MULTI_NOTECUT: /* start */
      if(c->flags&CF_PLAYING) {
        c->dlyticks=(play.newparm&0xF)+1;
        do_notecut(c);
        set_fx(c);
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(play.newfx,play.newparm,"ignored"); }
      break;
    case FX_MULTI_NOTEDLY: /* start */
      do_notedly(c); /* dlyticks>0! */
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"started"); }
      break;
    }
    break;
  }
  /* play.newfx, play.newparm: unused now */
  play.newfx=0;
  if(DEBUG_PLAY) {
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"<- ");
    DBG_LEAVE();
  }
}

/****************************************************************************/

/* In: c->fx>0 */
static void tickfx(struct chn_t*c) {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"-> ");
  }
  switch(c->fx) {
  default: /* N/A */
  case FX_PAN: /* tick (N/A) */
  case FX_SAMOFS: /* tick (N/A) */
  case FX_JUMP: /* tick (N/A) */
  case FX_VOL: /* tick (N/A) */
  case FX_BREAK: /* tick (N/A) */
  case FX_SPEED: /* tick (N/A) */
    if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"no action"); }
    break;
  case FX_ARP: /* tick */
    if(c->flags&CF_PLAYING) {
      c->arppos++;
      if(c->arppos>2) { /* new cycle */
        c->arppos=0;
        change_vol(c,c->arpvol); /* restore in case it was muted earlier */
        c->period=c->arpperiod[0];
        c->smpstep=c->arpstep[0];
      } else if(c->arpperiod[c->arppos]) {
        c->period=c->arpperiod[c->arppos];
        c->smpstep=c->arpstep[c->arppos];
      } else
        change_vol(c,0); /* out of bounds: silently play current note */
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    break;
  case FX_SLUP: /* tick */
    if(c->flags&CF_PLAYING) {
      do_slup(c,PERIOD_MIN);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    break;
  case FX_SLDN: /* tick */
    if(c->flags&CF_PLAYING) {
      do_sldn(c,PERIOD_MAX);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    break;
  case FX_SLTN: /* tick */
    if(c->flags&CF_PLAYING) {
      do_sltn(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    break;
  case FX_VIB: /* tick */
    if(c->flags&CF_PLAYING) {
      do_vib(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    break;
  case FX_SLTNVOLSL: /* tick */
    if(c->flags&CF_PLAYING) {
      do_sltn(c);
      do_volsl(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    break;
  case FX_VIBVOLSL: /* tick */
    if(c->flags&CF_PLAYING) {
      do_vib(c);
      do_volsl(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    break;
  case FX_TREM: /* tick */
    if(c->flags&CF_PLAYING) {
      do_trem(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    break;
  case FX_VOLSL: /* tick */
    if(c->flags&CF_PLAYING) {
      do_volsl(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    break;
  case FX_MULTI: /* tick */
    switch(c->parm>>4) {
    default: /* N/A */
    case FX_MULTI_0: /* tick (N/A) */
    case FX_MULTI_FSLUP: /* tick (N/A) */
    case FX_MULTI_FSLDN: /* tick (N/A) */
    case FX_MULTI_3: /* tick (N/A) */
    case FX_MULTI_VIBWAVE: /* tick (N/A) */
    case FX_MULTI_FINETUNE: /* tick (N/A) */
    case FX_MULTI_PATLOOP: /* tick (N/A) */
    case FX_MULTI_TREMWAVE: /* tick (N/A) */
    case FX_MULTI_8: /* tick (N/A) */
    case FX_MULTI_FVOLSLUP: /* tick (N/A) */
    case FX_MULTI_FVOLSLDN: /* tick (N/A) */
    case FX_MULTI_PATDLY: /* tick (N/A) */
    case FX_MULTI_15: /* tick (N/A) */
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"no action"); }
      break;
    case FX_MULTI_RETRIG: /* tick */
      if(c->flags&CF_PLAYING) {
        if(c->dlyticks) {
          do_retrig(c);
          if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
        } else
          if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
      break;
    case FX_MULTI_NOTECUT: /* tick */
      if(c->flags&CF_PLAYING) {
        if(c->dlyticks) {
          do_notecut(c);
          if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
        } else
          if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
      break;
    case FX_MULTI_NOTEDLY: /* tick */
      if(c->dlyticks) {
        do_notedly(c);
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
      break;
    }
  }
  if(DEBUG_PLAY) {
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"<- ");
    DBG_LEAVE();
  }
}

/****************************************************************************/

/* In: c->fx>0 */
static void endrowfx(struct chn_t*c) {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_PRINTF("[CHANNEL %hhu] [END EFFECT: %s]"NL,
      (u8)get_channel_number(c),
      (char*)DBG_GET_FX_NAME(c->fx,c->parm));
  }
  switch(c->fx) {
  default: /* N/A */
  case FX_ARP: /* end row (N/A) */
  case FX_SLUP: /* end row (N/A) */
  case FX_SLDN: /* end row (N/A) */
  case FX_SLTN: /* end row (N/A) */
  case FX_VIB: /* end row (N/A) */
  case FX_SLTNVOLSL: /* end row (N/A) */
  case FX_VIBVOLSL: /* end row (N/A) */
  case FX_TREM: /* end row (N/A) */
  case FX_PAN: /* end row (N/A) */
  case FX_SAMOFS: /* end row (N/A) */
  case FX_VOLSL: /* end row (N/A) */
  case FX_VOL: /* end row (N/A) */
    if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"no action"); }
    break;
  case FX_JUMP: /* end row */
    if(c->parm<song.order_len) {
      play.rowflags|=RF_SETPOS;
      play.newpos=c->parm;
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    c->fx=0;
    break;
  case FX_BREAK: /* end row */
    if(c->parm<SONG_PATTERN_LEN) {
      play.rowflags|=RF_NEXTPOS|RF_SETROW;
      play.newrow=c->parm;
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
    } else
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
    c->fx=0;
    break;
  case FX_MULTI: /* end row */
    switch(c->parm>>4) {
    default: /* N/A */
    case FX_MULTI_0: /* end row (N/A) */
    case FX_MULTI_FSLUP: /* end row (N/A) */
    case FX_MULTI_FSLDN: /* end row (N/A) */
    case FX_MULTI_3: /* end row (N/A) */
    case FX_MULTI_VIBWAVE: /* end row (N/A) */
    case FX_MULTI_FINETUNE: /* end row (N/A) */
    case FX_MULTI_TREMWAVE: /* end row (N/A) */
    case FX_MULTI_8: /* end row (N/A) */
    case FX_MULTI_RETRIG: /* end row (N/A) */
    case FX_MULTI_FVOLSLUP: /* end row (N/A) */
    case FX_MULTI_FVOLSLDN: /* end row (N/A) */
    case FX_MULTI_NOTECUT: /* end row (N/A) */
    case FX_MULTI_NOTEDLY: /* end row (N/A) */
    case FX_MULTI_15: /* end row (N/A) */
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"no action"); }
      break;
    case FX_MULTI_PATLOOP: /* end row */
      if(c->parm&0xF) {
        play.rowflags|=RF_PLOOP; /* loops count already set, just set flag */
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"handled"); }
      } else
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"ignored"); }
      c->fx=0;
      break;
    case FX_MULTI_PATDLY: /* end row */
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
      c->fx=0;
      break;
    }
    break;
  case FX_SPEED: /* end row */
    if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
    c->fx=0;
    break;
  }
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

/* In: c->fx>0 */
static void touchfx(struct chn_t*c) {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"-> ");
  }
  switch(c->fx) {
  default: /* N/A */
    if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"no action"); }
    break;
  case FX_PAN: /* touch (stop) */
  case FX_SAMOFS: /* touch (stop) */
  case FX_JUMP: /* touch (stop) */
  case FX_VOL: /* touch (stop) */
  case FX_BREAK: /* touch (stop) */
  case FX_SPEED: /* touch (stop) */
    if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
    c->fx=0;
    break;
  case FX_ARP: /* touch */
    change_vol(c,c->arpvol);
    c->period=c->arpperiod[0];
    c->smpstep=c->arpstep[0];
    if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
    c->fx=0;
    break;
  case FX_SLUP: /* touch */
  case FX_SLDN: /* touch */
    if((play.newfx==c->fx)&&(c->flags&CF_PLAYING)) {
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"continued"); }
    } else {
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
      c->fx=0;
    }
    break;
  case FX_SLTN: /* touch */
  case FX_SLTNVOLSL: /* touch */
    if((c->flags&CF_PLAYING)
    && ((play.newfx==FX_SLTN)||(play.newfx==FX_SLTNVOLSL))) {
      c->flags|=CF_CONTFX; /* not used actually */
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"continued"); }
    } else {
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
      stop_sltn(c);
      c->fx=0;
    }
    break;
  case FX_VIB: /* touch */
  case FX_VIBVOLSL: /* touch */
    if((c->flags&CF_PLAYING)
    && ((play.newfx==FX_VIB)||(play.newfx==FX_VIBVOLSL))) {
      c->flags|=CF_CONTFX;
      c->period=c->dstperiod;
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"continued"); }
    } else {
      stop_vib(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
      c->fx=0;
    }
    break;
  case FX_TREM: /* touch */
    if((c->flags&CF_PLAYING)&&(play.newfx==FX_TREM)) {
      c->flags|=CF_CONTFX;
      c->vol=c->tremvol;
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"continued"); }
    } else {
      stop_trem(c);
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
      c->fx=0;
    }
    break;
  case FX_VOLSL: /* touch */
    if((play.newfx==c->fx)&&(c->flags&CF_PLAYING)) {
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"continued"); }
    } else {
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
      c->fx=0;
    }
    break;
  case FX_MULTI: /* touch */
    switch(c->parm>>4) {
    default: /* N/A */
    case FX_MULTI_0: /* touch (N/A) */
    case FX_MULTI_3: /* touch (N/A) */
    case FX_MULTI_8: /* touch (N/A) */
    case FX_MULTI_15: /* touch (N/A) */
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"no action"); }
      break;
    case FX_MULTI_FSLUP: /* touch */
    case FX_MULTI_FSLDN: /* touch */
      if((play.newfx==c->fx)&&(play.newparm&0xF)&&(c->flags&CF_PLAYING)) {
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"continued"); }
      } else {
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
        c->fx=0;
      }
      break;
    case FX_MULTI_VIBWAVE: /* touch (stop) */
    case FX_MULTI_FINETUNE: /* touch (stop) */
    case FX_MULTI_PATLOOP: /* touch (stop) */
    case FX_MULTI_TREMWAVE: /* touch (stop) */
    case FX_MULTI_NOTECUT: /* touch (stop) */
    case FX_MULTI_PATDLY: /* touch (stop) */
      if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
      c->fx=0;
      break;
    case FX_MULTI_RETRIG: /* touch */
      if((play.newfx==c->fx)&&(play.newparm&0xF)&&(c->flags&CF_PLAYING)) {
        c->flags|=CF_CONTFX;
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"continued"); }
      } else {
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
        c->fx=0;
      }
      break;
    case FX_MULTI_FVOLSLUP: /* touch */
    case FX_MULTI_FVOLSLDN: /* touch */
      if((play.newfx==c->fx)&&(play.newparm&0xF)&&(c->flags&CF_PLAYING)) {
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"continued"); }
      } else {
        if(DEBUG_PLAY) { DBG_FX_ACTION(c->fx,c->parm,"stopped"); }
        c->fx=0;
      }
      break;
    }
    break;
  }
  if(DEBUG_PLAY) {
    DBG_DUMP_CHN_STATE_AND_EVENTS(c,"<- ");
    DBG_LEAVE();
  }
}

/****************************************************************************/

static void play_event(struct chn_t*c) {
  if(DEBUG_PLAY) { DBG_ENTER(); }
  c->flags&=~(CF_CONTFX|CF_NEWNOTE); /* stop previous effect by default */
  /* dispatch new events */
  if(DEBUG_PLAY) {
    DBG_DUMP_STATE("-> ");
    DBG_DUMP_CHN(c,"-> ");
  }
  /* check if we can continue previous effect or stop it */
  if(c->fx) touchfx(c);
  else
    if(DEBUG_PLAY) { DBG_LOG("touch fx skipped (no current effect)"NL); }
  /* initialize immediate, per-row or special effects */
  if(play.newfx) initfx(c);
  else
    if(DEBUG_PLAY) { DBG_LOG("init fx skipped (no new effect)"NL); }
  /* check both instrument and note to start playing a sample */
  if(play.newins||play.newnote) play_note(c);
  /*play.newins=0;*/ /* remove events */
  /*play.newnote=0;*/
  /* check if we must apply effects after starting to play */
  if(play.newfx) startfx(c);
  else
    if(DEBUG_PLAY) { DBG_LOG("start fx skipped (no new effect)"NL); }
  /* `c->flags&CF_NEWNOTE': unused now */
  if(DEBUG_PLAY) {
    DBG_DUMP_CHN(c,"<- ");
    DBG_DUMP_STATE("<- ");
    DBG_LEAVE();
  }
}

/****************************************************************************/

static void read_row_mod_raw() {
  struct chn_t*c=play.channels;
  u8 n=song.num_channels;
  u8*p=get_pattern_row(play.pat,play.row);

  if(DEBUG_PLAY) { DBG_ENTER(); }

  do {
    /* read pattern's event */
    play.newnote=*p;
    play.newins=p[1];
    play.newfx=p[2];
    play.newparm=p[3];
    p+=4;
    play_event(c);
    c++; n--;
  } while(n);

  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

static void read_row_mod_packed36() {
  struct chn_t*c=play.channels;
  u8 n=song.num_channels;
  u8*p=get_pattern_row(play.pat,play.row);
  u8 prev_note=0,prev_ins=0,prev_fx=0,prev_parm=0;
  u8 skip=0,repeat=0;

  if(DEBUG_PLAY) { DBG_ENTER(); }

  do {
    /* read pattern's event */
    if(skip) { skip--; play.newnote=play.newins=play.newfx=play.newparm=0; }
    else if(repeat) {
      repeat--;
      play.newnote=prev_note;
      play.newins=prev_ins;
      play.newfx=prev_fx;
      play.newparm=prev_parm;
    } else {
      bool save,readins;
      if(*p&128) { play.newnote=0; save=readins=true; }
      else if(*p<32) {
        if(*p) skip=*p; else skip=SONG_CHANNELS_MAX;
        skip--;
        play.newnote=play.newins=play.newfx=play.newparm=0; p++;
        save=readins=false;
      } else if(*p<32+36) {
        play.newnote=*p-32+36+1; p++;
        play.newins=play.newfx=play.newparm=0;
        save=true; readins=false;
      } else if(*p<32+36*2) {
        play.newnote=*p-32-36+36+1; p++;
        save=readins=true;
      } else {
        repeat=*p-32-36*2; p++;
        play.newnote=prev_note;
        play.newins=prev_ins;
        play.newfx=prev_fx;
        play.newparm=prev_parm;
        save=readins=false;
      }
      if(readins) {
        bool readfx;
        if(*p&64) { play.newins=(*p&31)+1; readfx=*p&32; p++; }
        else { play.newins=0; readfx=true;}
        if(readfx) { play.newfx=(*p&15)+1; play.newparm=p[1]; p+=2; }
        else play.newfx=play.newparm=0;
      }
      if(save) {
        prev_note=play.newnote;
        prev_ins=play.newins;
        prev_fx=play.newfx;
        prev_parm=play.newparm;
      }
    }
    play_event(c);
    c++; n--;
  } while(n);

  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

static void read_row_mod_packed96() {
  struct chn_t*c=play.channels;
  u8 n=song.num_channels;
  u8*p=get_pattern_row(play.pat,play.row);
  u8 prev_note=0,prev_ins=0,prev_fx=0,prev_parm=0;
  u8 skip=0,repeat=0;

  if(DEBUG_PLAY) { DBG_ENTER(); }

  do {
    /* read pattern's event */
    if(skip) { skip--; play.newnote=play.newins=play.newfx=play.newparm=0; }
    else if(repeat) {
      repeat--;
      play.newnote=prev_note;
      play.newins=prev_ins;
      play.newfx=prev_fx;
      play.newparm=prev_parm;
    } else {
      bool save,readins;
      if(*p&128) { play.newnote=0; save=readins=true; }
      else if(*p<=2) {
        if(*p==2) { p++; skip=*p; }
        else if(*p) skip=*p;
        else skip=SONG_CHANNELS_MAX;
        skip--; p++;
        play.newnote=play.newins=play.newfx=play.newparm=0;
        save=readins=false;
      } else if(*p<=4) {
        if(*p==4) { p++; repeat=*p-1; } else repeat=0;
        p++;
        play.newnote=prev_note;
        play.newins=prev_ins;
        play.newfx=prev_fx;
        play.newparm=prev_parm;
        save=readins=false;
      } else if(*p<=6) {
        if(*p==6) readins=true; else readins=false;
        save=true; p++;
        play.newnote=*p; p++;
      } else if(*p<=7+60-1) {
        play.newnote=*p-7+24+1; p++;
        play.newins=play.newfx=play.newparm=0;
        save=true; readins=false;
      } else {
        play.newnote=*p-7-60+24+1; p++;
        save=readins=true;
      }
      if(readins) {
        bool readfx;
        if(*p&64) { play.newins=(*p&31)+1; readfx=*p&32; p++; }
        else { play.newins=0; readfx=true;}
        if(readfx) { play.newfx=(*p&15)+1; play.newparm=p[1]; p+=2; }
        else play.newfx=play.newparm=0;
      }
      if(save) {
        prev_note=play.newnote;
        prev_ins=play.newins;
        prev_fx=play.newfx;
        prev_parm=play.newparm;
      }
    }
    play_event(c);
    c++; n--;
  } while(n);

  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

static void tick_row() {
  struct chn_t*c=play.channels;
  u8 n=song.num_channels;

  if(DEBUG_PLAY) { DBG_ENTER(); }
  do {
    if(c->fx) tickfx(c);
    else
      if(DEBUG_PLAY) {
        DBG_PRINTF("channel #%hhu: tick fx skipped (no effect)"NL,
          (u8)get_channel_number(c));
      }
    c++; n--;
  } while(n);
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

static void end_row() {
  struct chn_t*c=play.channels;
  u8 n=song.num_channels;

  if(DEBUG_PLAY) { DBG_ENTER(); }
  do {
    if(c->fx) endrowfx(c);
    else
      if(DEBUG_PLAY) {
        DBG_PRINTF("channel #%hhu: end row fx skipped (no effect)"NL,
          (u8)get_channel_number(c));
      }
    c++; n--;
  } while(n);
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

/* Out: play.flags */
static void set_pos(i16 p) {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_STATE("-> ");
    DBG_PRINTF("pos=%hd"NL,(i16)p);
  }

  /* remove events */
  play.flags&=~PF_PLOOP;
  play.rowflags&=~(RF_PLOOP|RF_SETPOS|RF_NEXTPOS|RF_NEXTROW);

  while((p<song.order_len)&&(SONG_ORDER_SKIP==song.order[p])) p++;

  if(p>=song.order_len) {
    if(!(play.opts&PO_LOOP)) goto _stop;
    p=0;
  } else if(p<=play.pos) {
    if(!(play.opts&PO_LOOP)) goto _stop;
  }

  play.pos=p;
  play.patnum=song.order[play.pos];
  play.pat=&song.patterns[play.patnum];
  play.row=0;
  /* remove `SET ROW' event if already set */
  if((play.rowflags&RF_SETROW)&&(!play.newrow)) play.rowflags&=~RF_SETROW;
  play.ploopto=0; /* no start row set in new pattern */
  goto _exit;
_stop:
  play.flags&=~PF_PLAYING;
  if(DEBUG_PLAY) { DBG_LOG("stop"NL); }
_exit:
  if(DEBUG_PLAY) {
    DBG_DUMP_STATE("<- ");
    DBG_LEAVE();
  }
}

/****************************************************************************/

/* Return `true' if outside of pattern */
static bool set_row(int r) {
  bool st;

  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_STATE("-> ");
    DBG_PRINTF("row=%hd"NL,(i16)r);
  }

  play.rowflags&=~(RF_NEXTROW|RF_SETROW); /* remove events */

  if(r<SONG_PATTERN_LEN) { play.row=r; st=false; } else st=true;

  if(DEBUG_PLAY) {
    DBG_DUMP_STATE("<- ");
    DBG_LEAVE();
  }
  return st;
}

/****************************************************************************/

/* Out: play.flags, play.rowflags */
static void next_row() {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_STATE("-> ");
    DBG_LOG("loop: begin"NL);
  }

  /* next position */
  play.rowflags|=RF_NEXTROW;
  do {
    if(DEBUG_PLAY) {
      DBG_LOG("iteration: begin"NL);
      DBG_DUMP_STATE("*> ");
    }

    if(play.rowflags&RF_PLOOP) {
      bool loop=false;
      play.rowflags&=~RF_PLOOP;
      if(play.flags&PF_PLOOP) {
        play.ploops--;
        if(play.ploops) {
          loop=true;
          if(DEBUG_PLAY) { DBG_LOG("ploop:do"NL); }
        } else {
          play.flags&=~PF_PLOOP;
          /* set start row to next row if possible */
          play.ploopto=(play.row<SONG_PATTERN_LEN-1)?play.row+2:0;
          if(DEBUG_PLAY) { DBG_LOG("ploop:end"NL); }
        }
      } else {
        play.flags|=PF_PLOOP;
        loop=true;
        if(DEBUG_PLAY) { DBG_LOG("ploop:start"NL); }
      }
      if(loop) {
        play.rowflags&=~(RF_SETPOS|RF_NEXTPOS); /* ignore jumps */
        play.rowflags|=RF_SETROW;
        play.newrow=play.ploopto-1;
      }
    }

    if(play.rowflags&RF_SETPOS) {
      set_pos(play.newpos);
      if(!(play.flags&PF_PLAYING)) {
        if(DEBUG_PLAY) { DBG_LOG("stop"NL); }
        goto _exit;
      }
    }

    if(play.rowflags&RF_NEXTPOS) {
      set_pos(play.pos+1);
      if(!(play.flags&PF_PLAYING)) {
        if(DEBUG_PLAY) { DBG_LOG("stop"NL); }
        goto _exit;
      }
    }

    if(play.rowflags&RF_SETROW) set_row(play.newrow);

    if(play.rowflags&RF_NEXTROW) if(set_row(play.row+1)) set_pos(play.pos+1);

    if(DEBUG_PLAY) { DBG_LOG("iteration: end"NL); }
  } while((play.flags&PF_PLAYING)
    &&    (play.rowflags&(RF_NEXTROW|RF_SETROW|RF_NEXTPOS|RF_SETPOS)));
_exit:
  if(DEBUG_PLAY) {
    DBG_DUMP_STATE("<- ");
    DBG_LOG("loop: end"NL);
    DBG_LEAVE();
  }
}

/****************************************************************************/

static void calc_sptleft() {
  u32 spt=(u32)play.sptf+(play.spt<<8)+play.sptleftf;
  play.sptleftf=spt&0xFF;
  play.sptleft=spt>>8;
}

static void next_tick() {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_DUMP_STATE("-> ");
  }

  play.tick--;
  if(!play.tick) {
    if(play.patdly) {
      play.patdly--;
      /* restart ticks counter */
      play.tick=play.tpr;
      calc_sptleft();
    }
  }

  if(play.tick) {
    if(DEBUG_PLAY) { DBG_LOG("next tick"NL); }
    /* do effects on next tick */
    play.rowflags&=~RF_FIRSTTICK;
    tick_row();
  } else {
    if(DEBUG_PLAY) { DBG_LOG("row end"NL); }
    /* trigger current per-row effects, remove single-row effects */
    end_row();
    apply_row_effects(); /* HINT: set on starting to play song only */
    if(!(play.flags&PF_PLAYING)) {
      if(DEBUG_PLAY) { DBG_LOG("stop"NL); }
      goto _exit;
    }
    next_row();
    if(!(play.flags&PF_PLAYING)) {
      if(DEBUG_PLAY) { DBG_LOG("stop"NL); }
      goto _exit;
    }
    play.rowflags|=RF_FIRSTTICK;
    switch(play.pat->format) {
    case PATFMT_MOD_RAW: read_row_mod_raw(); break;
    case PATFMT_MOD_PACKED36: read_row_mod_packed36(); break;
    case PATFMT_MOD_PACKED96: read_row_mod_packed96(); break;
    default: break;
    }
    if(!(play.flags&PF_PLAYING)) {
      if(DEBUG_PLAY) { DBG_LOG("stop"NL); }
      goto _exit;
    }
    apply_row_effects();
    play.tick=play.tpr;
  }
  calc_sptleft();
_exit:
  if(DEBUG_PLAY) {
    DBG_DUMP_STATE("<- ");
    DBG_LEAVE();
  }
}

/****************************************************************************/

u16 calc_sample_steps(const struct chn_t*c,const struct smppos_t next) {
  union smppos64_u p,end;
  p.pos=c->smppos; p.w[3]=0;
  end.pos=next; end.w[3]=0;
  return (u64)(end.q-p.q-1)/c->smpstep+1;
}

u64 calc_sample_pos(const struct chn_t*c,u16 steps) {
  union smppos64_u p;
  p.pos=c->smppos; p.w[3]=0;
  return (u64)p.q+c->smpstep*steps;
}

/* `count' is a number of samples per channel to mix */
static void mix_channel(struct chn_t*c,u16 count,i16*buf) {
  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_PRINTF("count=0x%hX"NL,(u16)count);
  }
  if(c->smpllen) {
    /* mix looped sample into mixing buffer */
    i16*out=buf;
    union smppos64_u lbeg,llen,lend;
    lbeg.pos.f=0; lbeg.pos.i=c->smplbeg; lbeg.w[3]=0;
    llen.pos.f=0; llen.pos.i=c->smpllen; llen.w[3]=0;
    lend.q=lbeg.q+llen.q;
    if(c->vol) {
      u16 left=count;
      do {
        u16 part=left;
        union smppos64_u next;
        next.q=calc_sample_pos(c,part);
        if(next.q>=lend.q) {
          part=calc_sample_steps(c,lend.pos);
          next.q=calc_sample_pos(c,part);
          if(c->smpstep<=llen.q) next.q-=llen.q;
          else next.q=lbeg.q+(next.q-lend.q)%llen.q;
        }
        if(DEBUG_PLAY) { DBG_MIXSMP(c,part); }
        play.mixproc(c,part,out);
        c->smppos=next.pos;
        out+=(play.opts&PO_STEREO)?part<<1:part;
        left-=part;
      } while(left);
    } else {
      /* silent play */
      union smppos64_u next;
      next.q=calc_sample_pos(c,count);
      if(next.q>=lend.q) next.q=lbeg.q+(next.q-lend.q)%llen.q;
      c->smppos=next.pos;
    }
  } else {
    /* mix normal sample into mixing buffer */
    u16 part=count;
    union smppos64_u lend,next;
    bool stop;
    lend.pos.f=0; lend.pos.i=c->smplen; lend.w[3]=0;
    next.q=calc_sample_pos(c,part);
    if((stop=(next.q>=lend.q))!=0) part=calc_sample_steps(c,lend.pos);
    if(c->vol) {
      if(DEBUG_PLAY) { DBG_MIXSMP(c,part); }
      play.mixproc(c,part,buf);
    }
    if(stop) c->flags&=~CF_PLAYING; /* stop playing sample on the channel */
    else c->smppos=next.pos;
  }
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

/* `count' is a number of samples per channel to mix */
static void mix_tick(i16*buf,u16 count) {
  struct chn_t*c;
  u8 n;

  if(DEBUG_PLAY) {
    DBG_ENTER();
    DBG_PRINTF("-> count=0x%hX",(u16)count);
    DBG_DUMP_STATE(" ");
  }
  c=play.channels;
  n=song.num_channels;
  do {
    if(DEBUG_PLAY) {
      DBG_PRINTF("interp=%hhu ",(u8)play.interp);
      DBG_DUMP_CHN(c,"--> ");
    }
    if((c->flags&CF_PLAYING)&&c->smpdata) mix_channel(c,count,buf);
    if(DEBUG_PLAY) {
      DBG_PRINTF("interp=%hhu ",(u8)play.interp);
      DBG_DUMP_CHN(c,"<-- ");
    }
    c++; n--;
  } while(n);
  if(DEBUG_PLAY) { DBG_LEAVE(); }
}

/****************************************************************************/

/*
 * interp=0->INTERP_MAX
 * ovs=0->OVS_Q_MAX
 * Returns 0 on success
 */
bool play_init(amiga_t clock,u32 rate,bool stereo,bool _16bits,u8 interp,
  bool vt,u8 mq,u8 ovs,bool loop) {
  if(clock>AMIGA_MAX) clock=AMIGA_MAX;
  if(rate<RATE_MIN) rate=RATE_MIN; else if(rate>RATE_MAX) rate=RATE_MAX;
  set_rate(clock,rate);
  play.interp=(interp<=INTERP_MAX)?interp:0;
  play.vt=vt;
  play.mq=mq;
  if(mq) play.ovs=(ovs<=OVS_Q_MAX)?ovs:OVS_Q_MAX;
  if(stereo) play.opts|=PO_STEREO; else play.opts&=~PO_STEREO;
  if(_16bits) play.opts|=PO_16BITS; else play.opts&=~PO_16BITS;
  if(loop) play.opts|=PO_LOOP; else play.opts&=~PO_LOOP;
  DBG_PRINTF("rate=%u, loop=%c, sptbase=0x%X, notebase=0x%X"NL,
    (u32)play.rate,
    (u8)((play.opts&PO_LOOP)?'Y':'N'),
    (u32)play.sptbase,
    (u32)play.Notebase[0]);
  DBG_PRINTF("stereo=%c"NL,(u8)((play.opts&PO_STEREO)?'Y':'N'));
  DBG_PRINTF("16bits=%c"NL,(u8)((play.opts&PO_16BITS)?'Y':'N'));
  DBG_PRINTF("interp=%hhu"NL,(u8)play.interp);
  DBG_PRINTF("mq=%hhu"NL,(u8)play.mq);
  if(play.mq) {
    DBG_PRINTF("ovs=%hhu"NL,(u8)play.ovs);
  }
  return 0;
}

/****************************************************************************/

void play_start() {
  /* set initial state */
  song.pat_rowsize=song.num_channels*4;
  play.flags=PF_PLAYING;
  play.rowflags=RF_SETTEMPO|RF_SETTPR|RF_SETPOS|RF_SETROW;
  play.pos=-1;         /* N/A */
  play.patnum=-1;      /* N/A */
  play.pat=NULL;       /* N/A */
  play.row=-1;         /* N/A */
  play.tick=1;         /* last tick */
  play.patdly=0;       /* no delay */
  play.ploopto=0;      /* no start row */
  play.ploops=0;       /* no loop */
  play.sptf=0;         /* N/A */
  play.spt=0;          /* N/A */
  play.sptleftf=0;     /* N/A */
  play.sptleft=0;      /* immediately next tick */
  play.tpr=0;          /* N/A */
  play.newpos=0;       /* start position */
  play.newrow=0;       /* start row */
  play.newtempo=125;   /* default */
  play.newtpr=6;       /* default */
  /* clear channels */
  memset(play.channels,0,&play.channels[song.num_channels]-play.channels);
  set_default_panning();
}

/****************************************************************************/

u16 play_fill(i16*buf,u16 count) {
  i16*p=buf;
  u16 played=0,len=count;

  if(DEBUG_PLAY) { DBG_ENTER(); }
  if(play.opts&PO_STEREO) len<<=1;
  memset(buf,0,len<<1); /* clear buffer */
  set_mixproc();
  while((play.flags&PF_PLAYING)&&count) {
    if(DEBUG_PLAY) {
      DBG_PRINTF("*> count=0x%hX",(u16)count);
      DBG_DUMP_STATE(" ");
    }
    if(play.sptleft) {
      u16 part=MIN(play.sptleft,count);
      len=part;
      mix_tick(p,part);
      if(play.opts&PO_STEREO) len<<=1;
      p+=len;
      played+=part;
      count-=part;
      play.sptleft-=part;
    } else next_tick();
  }
  if(DEBUG_PLAY) { DBG_LEAVE(); }
  return played;
}

/****************************************************************************/

void play_stop() {
  play.flags&=~PF_PLAYING;
}

/****************************************************************************/

void play_free() {
  if(play.flags&PF_PLAYING) play_stop();
}
