/*
 * play.h - playing routines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef PLAY_H
#define PLAY_H

#include <stdbool.h>
#include "common.h"
#include "song.h"

#define INTERP_MAX 2 /* 0=no, 1=linear, 2=cubic */

/* Amiga's machine clock source */
typedef u8 amiga_t;
#define AMIGA_PAL  0
#define AMIGA_NTSC 1
#define AMIGA_MAX  1

/* Channel flags */
#define CF_PLAYING   (1<<0)  /* playing a sample */
#define CF_CONTFX    (1<<1)  /* continue effect */
#define CF_SLTNFX    (1<<2)  /* pitch slide to note effect */
#define CF_NEWNOTE   (1<<3)  /* new note/period/sample */

/* Waveform flags */
#define WF_WAVEMASK 3
#define WF_SINE     0
#define WF_RAMPDN   1
#define WF_SQUARE   2
#define WF_RANDOM   3
#define WF_NORETRIG 4

#pragma pack(push,1)
struct smppos_t {
  u16 f; /* fractional part */
  u32 i; /* integer part */
};

/* For convenience */
union smppos64_u {
  struct smppos_t pos;
  u16 w[4];
  u32 d[2];
  u64 q;
};
#pragma pack(pop)

struct chn_t {
  u8 flags;
  u8 insnum;
  u8 note;
  u16 period;
  u8 pan; /* used for stereo output */
  u8 vol;
  u8 fvol[2]; /* final volume (only used when output is stereo) */
  u8 fx;
  u8 parm;
  u8 smpnum;
  u8 smptune;
  u32 smplen;
  u32 smpllen;
  u32 smplbeg;
  u32 smpstep;
  struct smppos_t smppos;
  i8*smpdata;
  /* shared data */
  u16 dstperiod; /* `Slide to note': target period, `Vibrato': base period */
  u8 dlyticks; /* `Retrigger', `Note cut/delay': number of ticks left */
  /* arpeggio */
  u8 arpvol; /* original, use 0 volume in case new note is out of bounds */
  u8 arpparm; /* optimization for speed */
  u8 arpins; /* optimization for speed */
  u8 arpsmp; /* optimization for speed */
  u8 arptune; /* optimization for speed */
  u8 arppos; /* =0,1,2 */
  u16 arpperiod[3]; /* arpperiod=0 if note is out of bounds, do silent play */
  u32 arpstep[3]; /* undefined if arpperiod=0 */
  /* pitch slides */
  u8 slspeed;
  /* pitch vibrato */
  u8 vibwave;
  u8 vibpos;
  u8 vibparm;
  /* tremolo */
  u8 tremvol;
  u8 tremwave;
  u8 trempos;
  u8 tremparm;
  /* note delay */
  u8 dlyins;
  u8 dlynote;
};

/* Play flags */
#define PF_PLAYING (1<<0)
#define PF_PLOOP   (1<<1)
/* Play options */
#define PO_STEREO  (1<<0)
#define PO_16BITS  (1<<1)
#define PO_LOOP    (1<<2)
/* Row flags */
#define RF_SETTEMPO  (1<<0) /* priority: 0 (first to handle) */
#define RF_SETTPR    (1<<1) /* priority: 0 (first to handle) */
#define RF_PLOOP     (1<<2) /* priority: 1 */
#define RF_SETPOS    (1<<3) /* priority: 2 */
#define RF_NEXTPOS   (1<<4) /* priority: 3 */
#define RF_SETROW    (1<<5) /* priority: 4 */
#define RF_NEXTROW   (1<<6) /* priority: 5 (last to handle) */
#define RF_FIRSTTICK (1<<7) /* flag (set but not used actually) */
/* Output rate range */
#define RATE_MIN 4000
#define RATE_MAX 192000

extern struct play_t {
  /* current state */
  u8 flags;
  u8 opts; /* options */
  u8 rowflags;
  u8 interp; /* 0->INTERP_MAX */
  bool vt; /* use volume table */
  u8 mq; /* mixing quality: 0,1 */
  u8 ovs; /* (mixing quality 1) output volume shift: 0->OVS_Q_MAX */
  u32 rate;
  u32 sptbase; /* depends on `rate', fixed point 24.8 */
  u32 Notebase[16]; /* depends on `rate' and fine-tune */
  u8 tempo;
  u8 tpr; /* ticks per row */
  /* song position */
  i8 pos;
  i16 patnum;
  i8 row;
  u8 patdly;
  u8 ploopto; /* Pattern loop: start row+1 */
  u8 ploops; /* Pattern loop: loops count (0=no loop) */
  u8 tick;
  struct pat_t*pat;
  u16 spt; /* samples per tick = sptbase/tempo (integer part) */
  u16 sptleft; /* samples per tick left (integer part) */
  u8 sptf; /* spt (fractional part) */
  u8 sptleftf; /* sptleft (fractional part) */
  /* new row state (see `rowflags') */
  u8 newrow;
  u8 newpos;
  u8 newtempo;
  u8 newtpr;
  u8 newploop;
  /* old channel state */
  u16 oldperiod; /* for `Slide to note' effect */
  /* new channel state */
  u8 newins;  /* 0=no instrument */
  u8 newnote; /* 0=no note */
  u8 newfx;   /* 0=no effect */
  u8 newparm;
  /* `count' is a number of samples per channel to mix */
  void (*mixproc)(const struct chn_t*c,u16 count,i16*buf);
  struct chn_t channels[SONG_CHANNELS_MAX];
} play;

#if DEBUG_PLAY
extern u8 get_channel_number(const struct chn_t*c);
#endif /* DEBUG_PLAY */

/*
 * interp=0->INTERP_MAX
 * ovs=0->OVS_Q_MAX
 */
extern bool play_init(amiga_t clock,u32 rate,bool stereo,bool _16bits,
  u8 interp,bool vt,u8 mq,u8 ovs,bool loop);
extern void play_start();

/* `count' is a number of samples per channel to mix */
extern u16 play_fill(i16*buf,u16 count);

extern void play_stop();
extern void play_free();

#endif /* !PLAY_H */
