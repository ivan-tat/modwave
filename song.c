/*
 * song.c - internal song representation
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022-2024 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdio.h> /* printf() */
#include <stdlib.h> /* malloc(), free() */
#include <string.h> /* memset(), memcpy(), memcmp() */
#include "Defines.h"
#include "common.h"
#include "debug.h"
#include "mod.h"
#include "song.h"
#include "tables.h"

struct song_t song;

/****************************************************************************/

#if DEBUG

i16 get_pattern_index(const struct pat_t*p) {
  return (p-song.patterns)/(&song.patterns[1]-song.patterns);
}

static const char notenames[24]="C-C#D-D#E-F-F#G-G#A-A#B-";

char*DBG_GET_NOTE_NAME(char*dst,u8 note) {
  if(!note) memcpy(dst,"---",4);
  else if(note<=SONG_NOTES) {
    u8 i=((note-1)%12)*2;
    dst[0]=notenames[i];
    dst[1]=notenames[i+1];
    dst[2]='0'+(note-1)/12;
    dst[3]='\0';
  } else memcpy(dst,"???",4);
  return dst;
}

#endif /* DEBUG */

#if DEBUG_DUMP_PATTERNS

static const char _fx_names[FX_MAX]="0123456789ABCDEF";

static void DBG_DUMP_CHN(u8 note,u8 ins,u8 fx,u8 parm) {
  char s[4];
  if(note) DBG_PRINTF("|%s ",(char*)DBG_GET_NOTE_NAME(s,note));
  else DBG_LOG("|... ");
  if(!ins) DBG_LOG(".. ");
  else if(ins<=song.num_instruments) DBG_PRINTF("%02hhu ",(u8)ins);
  else DBG_LOG("?? ");
  if(!fx) DBG_LOG("...");
  else if(fx<=FX_MAX) DBG_PRINTF("%c%02hhX",(u8)_fx_names[fx-1],(u8)parm);
  else DBG_LOG("???");
}

#endif /* DEBUG_DUMP_PATTERNS */

/****************************************************************************/

bool is_mod_raw36_pattern(const struct pat_t*pat) {
  bool result;
  if(PATFMT_MOD_RAW==pat->format) {
    const u8*p=pat->data;
    u16 count=song.pat_rowsize*SONG_PATTERN_LEN>>2; /* events counter */
    result=true;
    do {
      if(*p&&((*p<=1+35)||(*p>=1+72))) { result=false; break; }
      p+=4; count--;
    } while(count);
  } else result=false;
  return result;
}

/****************************************************************************/

/*
 * Output:
 *   o[i]=0, if row(i) is unique
 *   o[i]=j+1, if row(i) equals to row(j)
 *   0<=j<i<SONG_PATTERN_LEN
 */
static void find_dup_rows(const struct pat_t*pat,u16*o) {
  u8 uniq=1 /* unique rows found so far (first row is always unique) */;
  u8*ri=pat->data+song.pat_rowsize,i; /* current row */
  for(i=1;i<SONG_PATTERN_LEN;i++) {
    u8*rj=pat->data,j; /* unique row */
    u8 n=uniq; /* counter to speed up inner loop */
    for(j=0;j<i;j++) { /* compare current row with all unique rows */
      if(!(j&&o[j-1])) { /* row(j) is unique */
        if(!memcmp(ri,rj,song.pat_rowsize)) { o[i-1]=j+1; break; }
        n--; if(!n) { o[i-1]=0; uniq++; break; }
      }
      rj+=song.pat_rowsize;
    }
    ri+=song.pat_rowsize;
  }
}

/*
 * C=32 (max channels number)
 * N=36 (note range)
 * First byte:
 *   bit 7=0: one of four events
 *     bits 6->0=M, where:
 *       0: end of row
 *       1->C-1: skip M channels (1->C-1)
 *       C->C+N-1: set note to M-C+1 (1->N)
 *       C+N->C+N*2-1: set note to M-C-N+1 (1->N) and read second byte
 *       C+N*2->127: repeat previous channel events M-C-N*2+1 (1->24) times
 *   bit 7=1: bits 6->0 equals to second byte
 * Second byte: set instrument and/or effect
 *   bit 7: ignored
 *   bits 6->5:
 *     11: set instrument to bits 4->0 +1 (1->31) and read effect & parameter
 *     10: set instrument to bits 4->0 +1 (1->31)
 *     0x: set effect to bits 3->0 +1 (1->16) and read parameter
 */
static u8 pack_mod_raw36_pattern(struct pat_t*pat) {
  u8 err=0,*new;
  u16 s=(SONG_PATTERN_LEN-1)*2 /* packed size */;
  if((new=malloc(s+(song.pat_rowsize+1)*SONG_PATTERN_LEN))!=NULL) {
    u8*p=pat->data,r=0;
    find_dup_rows(pat,(u16*)new);
    do {
      if(r&&((u16*)new)[r-1]) { /* row(r) is a reference to unique row */
        u8 i=((u16*)new)[r-1]-1; /* unique row index */
        /* save row offset */
        ((u16*)new)[r-1]=i?((u16*)new)[i-1]:(SONG_PATTERN_LEN-1)*2;
        p+=song.pat_rowsize;
      } else { /* row(r) is a unique row */
        u8 n=song.num_channels,skip=0,repeat=0;
        u8 note=0,ins=0,fx=0,parm=0;
        if(r) ((u16*)new)[r-1]=s; /* save row offset */
        do {
          if(*p||p[1]||p[2]) { /* event occurs */
            if(skip) { new[s]=skip; s++; skip=0; } /* put skip amount */
            if((note|ins|fx)
            && (*p==note)&&(p[1]==ins)&&(p[2]==fx)&&(p[3]==parm)) repeat++;
            else {
              if(repeat) { /* put repeat amount */
                while(repeat>=128-32-36*2) {
                  new[s]=127; s++; repeat-=128-32-36*2;
                }
                if(repeat) {
                  new[s]=32+36*2+repeat-1; s++;
                  repeat=0;
                }
              }
              note=*p; ins=p[1]; fx=p[2];
              if(note) { /* put note */
                /* limit note to fit in range 37->72 */
                if(note<37) note=37; else if(note>72) note=72;
                new[s]=32+((ins||fx)?36:0)+note-37;
                s++;
              }
              if(ins) { /* put instrument */
                new[s]=(ins-1)|64|(note?0:128)|(fx?32:0); s++;
              }
              if(fx) { /* put effect & parameter */
                parm=p[3];
                new[s]=(fx-1)|(ins?0:32+(note?0:128)); new[s+1]=parm; s+=2;
              } else parm=0;
            }
          } else skip++;
          p+=4; n--;
        } while(n);
        if(repeat) { /* put repeat amount */
          while(repeat>=128-32-36*2) {
            new[s]=127; s++; repeat-=128-32-36*2;
          }
          if(repeat) {
            new[s]=32+36*2+repeat-1; s++;
            repeat=0;
          }
        }
        new[s]=0; s++; /* put row end mark */
      }
      r++;
    } while(r<SONG_PATTERN_LEN);
    if(s<pat->size) { /* replace old pattern */
      free(pat->data);
      pat->data=NULL;
      pat->format=PATFMT_MOD_PACKED36;
      pat->size=s;
      if((pat->data=malloc(s))!=NULL) memcpy(pat->data,new,s); else {
        if(DEBUG_SONG) { DBG_MALLOC_ERR(s); }
        err=1;
      }
    } else err=2;
  } else {
    if(DEBUG_SONG) {
      DBG_MALLOC_ERR(s+(song.pat_rowsize+1)*SONG_PATTERN_LEN);
    }
    err=1;
  }
  if(new) free(new);
  return err;
}

/****************************************************************************/

/*
 * A=24 (start note)
 * N=60 (note range)
 * First byte:
 *   bit 7=0: one of four events
 *     bits 6->0=M, where:
 *       0: end of row
 *       1: skip: 1 channel
 *       2: skip: read next byte as counter (1->SONG_CHANNELS_MAX)
 *       3: repeat: 1 time
 *       4: repeat: read next byte as counter (1->SONG_CHANNELS_MAX)
 *       5: note: read next byte as note (1->SONG_NOTES)
 *       6: note: read next byte as note (1->SONG_NOTES) and read second byte
 *       7->7+N-1: set note to A+M-7+1 (A+1->A+N)
 *       7+N->7+N*2: set note to A+M-7-N+1 (A+1->A+N+1) and read second byte
 *   bit 7=1: bits 6->0 equals to second byte
 * Second byte: set instrument and/or effect
 *   bit 7: ignored
 *   bits 6->5:
 *     11: set instrument to bits 4->0 +1 (1->31) and read effect & parameter
 *     10: set instrument to bits 4->0 +1 (1->31)
 *     0x: set effect to bits 3->0 +1 (1->16) and read parameter
 */

static u8 pack_mod_raw96_pattern(struct pat_t*pat) {
  u8 err=0,*new;
  u16 s=(SONG_PATTERN_LEN-1)*2 /* packed size */;
  if((new=malloc(s+(song.pat_rowsize+1)*SONG_PATTERN_LEN))!=NULL) {
    u8*p=pat->data,r=0;
    find_dup_rows(pat,(u16*)new);
    do {
      if(r&&((u16*)new)[r-1]) { /* row(r) is a reference to unique row */
        u8 i=((u16*)new)[r-1]-1; /* unique row index */
        /* save row offset */
        ((u16*)new)[r-1]=i?((u16*)new)[i-1]:(SONG_PATTERN_LEN-1)*2;
        p+=song.pat_rowsize;
      } else { /* row(r) is a unique row */
        u8 n=song.num_channels,skip=0,repeat=0;
        u8 note=0,ins=0,fx=0,parm=0;
        if(r) ((u16*)new)[r-1]=s; /* save row offset */
        do {
          if(*p||p[1]||p[2]) { /* event occurs */
            if(skip) { /* put skip amount */
              if(skip>=2) { new[s]=2; s++; }
              new[s]=skip; s++; skip=0;
            }
            if((note|ins|fx)
            && (*p==note)&&(p[1]==ins)&&(p[2]==fx)&&(p[3]==parm)) repeat++;
            else {
              if(repeat) { /* put repeat amount */
                if(repeat==1) new[s]=3;
                else { new[s]=4; s++; new[s]=repeat; }
                s++; repeat=0;
              }
              note=*p; ins=p[1]; fx=p[2];
              if(note) { /* put note */
                if(ins||fx) {
                  if((note-1>=24)&&(note-1<=24+60)) new[s]=7+60+note-1-24;
                  else { new[s]=6; s++; new[s]=note; }
                } else {
                  if((note-1>=24)&&(note-1<=24+60-1)) new[s]=7+note-1-24;
                  else { new[s]=5; s++; new[s]=note; }
                }
                s++;
              }
              if(ins) { /* put instrument */
                new[s]=(ins-1)|64|(note?0:128)|(fx?32:0); s++;
              }
              if(fx) { /* put effect & parameter */
                parm=p[3];
                new[s]=(fx-1)|(ins?0:32+(note?0:128)); new[s+1]=parm; s+=2;
              } else parm=0;
            }
          } else skip++;
          p+=4; n--;
        } while(n);
        if(repeat) { /* put repeat amount */
          if(repeat==1) new[s]=3;
          else { new[s]=4; s++; new[s]=repeat; }
          s++; repeat=0;
        }
        new[s]=0; s++; /* put row end mark */
      }
      r++;
    } while(r<SONG_PATTERN_LEN);
    if(s<pat->size) { /* replace old pattern */
      free(pat->data);
      pat->data=NULL;
      pat->format=PATFMT_MOD_PACKED96;
      pat->size=s;
      if((pat->data=malloc(s))!=NULL) memcpy(pat->data,new,s); else {
        if(DEBUG_SONG) { DBG_MALLOC_ERR(s); }
        err=1;
      }
    } else err=2;
  } else {
    if(DEBUG_SONG) {
      DBG_MALLOC_ERR(s+(song.pat_rowsize+1)*SONG_PATTERN_LEN);
    }
    err=1;
  }
  if(new) free(new);
  return err;
}

/****************************************************************************/

u8 pack_pattern(struct pat_t*pat) {
  switch(pat->format) {
  case PATFMT_MOD_RAW:
    if(is_mod_raw36_pattern(pat))
      return pack_mod_raw36_pattern(pat);
    else
      return pack_mod_raw96_pattern(pat);
  case PATFMT_MOD_PACKED36:
  case PATFMT_MOD_PACKED96:
    return 0;
  default:
    return 2;
  }
}

/****************************************************************************/

u8*get_pattern_row(const struct pat_t*p,u8 r) {
  u16 o;
  switch(p->format) {
  default:
  case PATFMT_MOD_RAW:
    o=r?song.pat_rowsize*r:0;
    break;
  case PATFMT_MOD_PACKED36:
  case PATFMT_MOD_PACKED96:
    o=r?((u16*)p->data)[r-1]:(SONG_PATTERN_LEN-1)*2;
    break;
  }
  return p->data+o;
}

/****************************************************************************/

#if DEBUG

u8 get_instrument_index(const struct ins_t*p) {
  return (p-song.instruments)/(&song.instruments[1]-song.instruments);
}

#endif /* DEBUG */

/****************************************************************************/

void song_clear() {
  struct ins_t*i;
  struct pat_t*p;
  u16 n;

  if(DEBUG_SONG) { DBG_ENTER(); }
  /* Clear instruments and samples */
  song.num_instruments=0;
  i=song.instruments;
  for(n=SONG_INSTRUMENTS_MAX;n;n--) { i->data=NULL; i++; }
  song.num_samples=0;
  /* Clear patterns */
  song.num_patterns=0;
  song.pat_rowsize=0;
  p=song.patterns;
  for(n=SONG_PATTERNS_MAX;n;n--) { p->data=NULL; p++; }
  /* Clear order */
  song.order_len=0;
  memset(song.order,0,sizeof(song.order));
  if(DEBUG_SONG) { DBG_LEAVE(); }
}

/****************************************************************************/

#if DEBUG_DUMP_INSTRUMENTS

static void DBG_DUMP_INSTRUMENTS() {
  if(song.num_instruments) {
    u8 i;
    struct ins_t*ins=song.instruments;
    for(i=0;i<song.num_instruments;i++) {
      DBG_PRINTF("instrument=%hhu: ",(u8)i+1);
      if(ins->data)
        DBG_PRINTF("data=%p, len=%u, tune=%hhu, vol=%hhu, lbeg=%u, llen=%u"NL,
          (void*)ins->data,
          (u32)ins->len,
          (u8)ins->tune,
          (u8)ins->vol,
          (u32)ins->lbeg,
          (u32)ins->llen);
      else DBG_LOG("<no sample>"NL);
      ins++;
    }
  }
}

#else /* !DEBUG_DUMP_INSTRUMENTS */

# define DBG_DUMP_INSTRUMENTS()

#endif /* DEBUG_DUMP_INSTRUMENTS */

/****************************************************************************/

#if DEBUG_DUMP_SAMPLES

static void DBG_DUMP_SAMPLES() {
  if(song.num_samples) {
    u8 i;
    struct ins_t*ins=song.instruments;
    for(i=0;i<song.num_instruments;i++) {
      DBG_PRINTF("sample=%hhu: ",(u8)i+1);
      if(ins->data) {
        DBG_PRINTF("len=0x%X (%u)"NL,(u32)ins->len,(u32)ins->len);
        DBG_MEMDUMP(ins->data,ins->len);
      } else DBG_LOG("<no sample>"NL);
      ins++;
    }
  }
}

#else /* !DEBUG_DUMP_SAMPLES */

# define DBG_DUMP_SAMPLES()

#endif /* DEBUG_DUMP_SAMPLES */

/****************************************************************************/

#if DEBUG_DUMP_ORDER

static void DBG_DUMP_ORDER() {
  DBG_LOG("order=");
  if(song.order_len) {
    u16 i;
    for(i=0;i<song.order_len;i++) {
      i16 p=song.order[i];
      if(SONG_ORDER_SKIP==p) DBG_LOG("+ "); else DBG_PRINTF("%hd ",(i16)p);
    }
    DBG_LOG(NL);
  } else DBG_LOG("<empty>"NL);
}

#else /* !DEBUG_DUMP_ORDER */

# define DBG_DUMP_ORDER()

#endif /* DEBUG_DUMP_ORDER */

/****************************************************************************/

#if DEBUG_DUMP_PATTERNS_STATS

#define UPDATE_EVENT_STATS(ins,note,fx,parm,iu,nc,fx_all,fx_multi)           \
  if(iu&&ins&&ins<=SONG_INSTRUMENTS_MAX) iu[(ins-1)>>3]|=(1<<((ins-1)&7));   \
  if(nc&&note&&note<=SONG_NOTES) nc[note-1]++;                               \
  if(fx_all&&fx&&fx<=FX_MAX) fx_all[fx-1]++;                                 \
  if(fx_multi&&fx==FX_MULTI) fx_multi[parm>>4]++;

static void _dump_events_mod_raw(const struct pat_t*p,u8*iu,u32*nc,u32*fx_all,
  u32*fx_multi) {

  u8 r;
  for(r=0;r<SONG_PATTERN_LEN;r++) {
    u8*d=get_pattern_row(p,r);
    u8 c;
    for(c=song.num_channels;c;c--) {
      u8 note,ins,fx,parm;
      note=*d; ins=d[1]; fx=d[2]; parm=d[3];
      UPDATE_EVENT_STATS(ins,note,fx,parm,iu,nc,fx_all,fx_multi);
      d+=4;
    }
  }
}

static void _dump_events_mod_packed36(const struct pat_t*p,u8*iu,u32*nc,
  u32*fx_all,u32*fx_multi) {

  u8 r;
  for(r=0;r<SONG_PATTERN_LEN;r++) {
    u8*d=get_pattern_row(p,r);
    u8 note0=0,ins0=0,fx0=0,parm0=0;
    u8 skip=0,repeat=0;
    u8 note=0,ins=0,fx=0,parm=0;
    u8 c;
    for(c=song.num_channels;c;c--) {
      /* read pattern's event */
      if(skip) { skip--; note=ins=fx=parm=0; }
      else if(repeat) {
        repeat--;
        note=note0; ins=ins0; fx=fx0; parm=parm0;
      } else {
        bool save,readins;
        if(*d&128) {
          note=0;
          save=readins=true;
        } else if(*d<32) {
          if(*d) skip=*d; else skip=SONG_CHANNELS_MAX;
          skip--;
          note=ins=fx=parm=0;
          save=readins=false;
          d++;
        } else if(*d<32+36) {
          note=*d-32+36+1; ins=fx=parm=0;
          save=true; readins=false;
          d++;
        } else if(*d<32+36*2) {
          note=*d-32-36+36+1;
          save=readins=true;
          d++;
        } else {
          repeat=*d-32-36*2;
          note=note0; ins=ins0; fx=fx0; parm=parm0;
          save=readins=false;
          d++;
        }
        if(readins) {
          bool readfx;
          if(*d&64) { ins=(*d&31)+1; readfx=*d&32; d++; }
          else { ins=0; readfx=true; }
          if(readfx) { fx=(*d&15)+1; parm=d[1]; d+=2; }
          else { fx=parm=0; }
        }
        if(save) { note0=note; ins0=ins; fx0=fx; parm0=parm; }
      }
      UPDATE_EVENT_STATS(ins,note,fx,parm,iu,nc,fx_all,fx_multi);
    }
  }
}

static void _dump_events_mod_packed96(const struct pat_t*p,u8*iu,u32*nc,
  u32*fx_all,u32*fx_multi) {

  u8 r;
  for(r=0;r<SONG_PATTERN_LEN;r++) {
    u8*d=get_pattern_row(p,r);
    u8 note0=0,ins0=0,fx0=0,parm0=0;
    u8 skip=0,repeat=0;
    u8 note=0,ins,fx,parm;
    u8 c;
    for(c=song.num_channels;c;c--) {
      /* read pattern's event */
      if(skip) { skip--; note=ins=fx=parm=0; }
      else if(repeat) {
        repeat--;
        note=note0; ins=ins0; fx=fx0; parm=parm0;
      } else {
        bool save,readins;
        if(*d&128) {
          note=0;
          save=readins=true;
        } else if(*d<=2) {
          if(*d==2) { d++; skip=*d; }
          else if(*d) skip=*d;
          else skip=SONG_CHANNELS_MAX;
          skip--;
          note=ins=fx=parm=0;
          save=readins=false;
          d++;
        } else if(*d<=4) {
          if(*d==4) { d++; repeat=*d; } else repeat=1;
          repeat--;
          note=note0; ins=ins0; fx=fx0; parm=parm0;
          save=readins=false;
          d++;
        } else if(*d<=6) {
          if(*d==6) readins=true; else readins=false;
          save=true; d++;
          note=*d;
          d++;
        } else if(*d<=7+60-1) {
          note=*d-7+24+1;
          ins=fx=parm=0;
          save=true; readins=false;
          d++;
        } else {
          note=*d-7-60+24+1;
          save=readins=true;
          d++;
        }
        if(readins) {
          bool readfx;
          if(*d&64) { ins=(*d&31)+1; readfx=*d&32; d++; }
          else { ins=0; readfx=true; }
          if(readfx) { fx=(*d&15)+1; parm=d[1]; d+=2; }
          else { fx=parm=0; }
        }
        if(save) { note0=note; ins0=ins; fx0=fx; parm0=parm; }
      }
      UPDATE_EVENT_STATS(ins,note,fx,parm,iu,nc,fx_all,fx_multi);
    }
  }
}

static void DBG_DUMP_PATTERNS_STATS() {
  u8 iu[(SONG_INSTRUMENTS_MAX+7)/8]; /* instruments used flags */
  u32 nc[SONG_NOTES]; /* note counters */
  u32 fx_all[FX_MAX]; /* all effects counters */
  u32 fx_multi[16]; /* multi-effect counters */
  u16 count;
  if(song.num_patterns) {
    const struct pat_t*p=song.patterns;
    u16 i;
    memset(iu,0,sizeof(iu));
    memset(nc,0,sizeof(nc));
    memset(fx_all,0,sizeof(fx_all));
    memset(fx_multi,0,sizeof(fx_multi));
    for(i=0;i<song.num_patterns;i++) {
      if(p->data) {
        switch(p->format) {
        case PATFMT_MOD_RAW:
          _dump_events_mod_raw(p,iu,nc,fx_all,fx_multi);
          break;
        case PATFMT_MOD_PACKED36:
          _dump_events_mod_packed36(p,iu,nc,fx_all,fx_multi);
          break;
        case PATFMT_MOD_PACKED96:
          _dump_events_mod_packed96(p,iu,nc,fx_all,fx_multi);
          break;
        default: break;
        }
      }
      p++;
    }
  }
  DBG_LOG("instruments=");
  count=0;
  if(song.num_patterns) {
    u16 i;
    for(i=0;i<SONG_INSTRUMENTS_MAX;i++) if(iu[i>>3]&(i&7)) {
      DBG_PRINTF(count?" %hu":"%hu",(u16)i+1);
      count++;
    }
  }
  if(!count) { DBG_LOG("<no instruments>"); }
  DBG_LOG(NL
    "notes=");
  count=0;
  if(song.num_patterns) {
    u16 i;
    for(i=0;i<SONG_NOTES;i++) if(nc[i]) {
      char c[4];
      DBG_PRINTF(count?" %s:%u":"%s:%u",
        (char*)DBG_GET_NOTE_NAME(c,i+1),(u32)nc[i]);
      count++;
    }
  }
  if(!count) { DBG_LOG("<no notes>"); }
  DBG_LOG(NL
    "effects=");
  count=0;
  if(song.num_patterns) {
    u16 i;
    for(i=1;i<=FX_MAX;i++) if(fx_all[i-1]) {
      if(i==FX_MULTI) {
        u8 j;
        for(j=0;j<=15;j++) if(fx_multi[j]) {
          const char*s=DBG_GET_FX_NAME(i,j<<4);
          DBG_PRINTF(count?" %s(E%hhX):%u":"%s(E%hhX):%u",
            (char*)s,(u8)j,(u32)fx_multi[j]);
          count++;
        }
      } else {
        const char*s=DBG_GET_FX_NAME(i,0);
        DBG_PRINTF(count?" %s(%hhX):%u":"%s(%hhX):%u",
          (char*)s,(u8)i-1,(u32)fx_all[i-1]);
      }
      count++;
    }
  }
  if(!count) DBG_LOG("<no effects>");
  DBG_LOG(NL);
}

#else /* !DEBUG_DUMP_PATTERNS_STATS */

# define DBG_DUMP_PATTERNS_STATS()

#endif /* !DEBUG_DUMP_PATTERNS_STATS */

/****************************************************************************/

#if DEBUG

const char*DBG_GET_FX_NAME(u8 fx,u8 parm) {
  switch(fx) {
  default:
  case 0: return "N/A";
  case FX_ARP: return "Arpeggio";
  case FX_SLUP: return "SlideUp";
  case FX_SLDN: return "SlideDown";
  case FX_SLTN: return "SlideToNote";
  case FX_VIB: return "Vibrato";
  case FX_SLTNVOLSL: return "SlideToNote+VolumeSlide";
  case FX_VIBVOLSL: return "Vibrato+VolumeSlide";
  case FX_TREM: return "Tremolo";
  case FX_PAN: return "SetPanPos";
  case FX_SAMOFS: return "SetSampleOffset";
  case FX_VOLSL: return
    (parm&0xF0)?"VolumeSlideUp":parm?"VolumeSlideDown":"NoVolumeSlide";
  case FX_JUMP: return "JumpToPosition";
  case FX_VOL: return "SetVolume";
  case FX_BREAK: return "PatternBreak";
  case FX_MULTI: /* DBG_GET_FX_NAME */
    switch(parm>>4){
    default:
    case FX_MULTI_0: return "N/A(SetFilter)";
    case FX_MULTI_FSLUP: return "FineSlideUp";
    case FX_MULTI_FSLDN: return "FineSlideDown";
    case FX_MULTI_3: return "N/A(Glissando)";
    case FX_MULTI_VIBWAVE: return "SetVibratoWave";
    case FX_MULTI_FINETUNE: return "SetFineTune";
    case FX_MULTI_PATLOOP: return "PatternLoop";
    case FX_MULTI_TREMWAVE: return "SetTremoloWave";
    case FX_MULTI_8: return "N/A(NotUsed)";
    case FX_MULTI_RETRIG: return "Retrigger";
    case FX_MULTI_FVOLSLUP: return
      (parm&0xF)?"FineVolumeSlideUp":"NoFineVolumeSlide";
    case FX_MULTI_FVOLSLDN: return
      (parm&0xF)?"FineVolumeSlideDown":"NoFineVolumeSlide";
    case FX_MULTI_NOTECUT: return "NoteCut";
    case FX_MULTI_NOTEDLY: return "NoteDelay";
    case FX_MULTI_PATDLY: return "PatternDelay";
    case FX_MULTI_15: return "N/A(InvLoop)";
    }
  case FX_SPEED: return (parm<SONG_TEMPO_MIN)?"SetTPR":"SetTempo";
  }
}

void DBG_DUMP_FX_PARM(u8 fx,u8 parm) {
  const char*s;
  char sn[4*2];
  s=DBG_GET_FX_NAME(fx,parm);
  switch(fx) {
  default: case 0: /* N/A */
    DBG_LOG("N/A");
    break;
  case FX_ARP: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s notes=%s,%s",
      (char*)s,
      (char*)DBG_GET_NOTE_NAME(sn,1+(parm>>4)),
      (char*)DBG_GET_NOTE_NAME(sn+4,1+(parm&0xF)));
    break;
  case FX_SLUP: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s speed=%02hhX",(char*)s,(u8)parm);
    break;
  case FX_SLDN: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s speed=%02hhX",(char*)s,(u8)parm);
    break;
  case FX_SLTN: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s speed=%02hhX",(char*)s,(u8)parm);
    break;
  case FX_VIB: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s speed=%hhX depth=%hhX",
      (char*)s,
      (u8)(parm>>4),
      (u8)(parm&0xF));
    break;
  case FX_SLTNVOLSL: /* DBG_DUMP_FX_PARM [TODO] */
    DBG_PRINTF("%s param=%02hhX",(char*)s,(u8)parm);
    break;
  case FX_VIBVOLSL: /* DBG_DUMP_FX_PARM [TODO] */
    DBG_PRINTF("%s param=%02hhX",(char*)s,(u8)parm);
    break;
  case FX_TREM: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s speed=%hhX depth=%hhX",
      (char*)s,
      (u8)(parm>>4),
      (u8)(parm&0xF));
    break;
  case FX_PAN: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s pos=%02hhX",(char*)s,(u8)parm);
    break;
  case FX_SAMOFS: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s offset=%X",
      (char*)s,
      (u32)parm<<8);
    break;
  case FX_VOLSL: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s speed=%hhX",
      (char*)s,
      (u8)((parm&0xF0)?parm>>4:parm)&0xF);
    break;
  case FX_JUMP: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s pos=%02hhX",(char*)s,(u8)parm);
    break;
  case FX_VOL: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s value=%02hhX",(char*)s,(u8)parm);
    break;
  case FX_BREAK: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s row=%02hhX",(char*)s,(u8)parm);
    break;
  case FX_MULTI: /* DBG_DUMP_FX_PARM */
    switch(parm>>4) {
    default: /* N/A */
    case FX_MULTI_0: /* N/A */
    case FX_MULTI_3: /* N/A */
    case FX_MULTI_8: /* N/A */
    case FX_MULTI_15: /* N/A */
      DBG_PRINTF("%s parm=%hhX",(char*)s,(u8)parm&0xF);
      break;
    case FX_MULTI_FSLUP: /* DBG_DUMP_FX_PARM */
    case FX_MULTI_FSLDN: /* DBG_DUMP_FX_PARM */
    case FX_MULTI_FVOLSLUP: /* DBG_DUMP_FX_PARM */
    case FX_MULTI_FVOLSLDN: /* DBG_DUMP_FX_PARM */
      DBG_PRINTF("%s speed=%hhX",(char*)s,(u8)parm&0xF);
      break;
    case FX_MULTI_VIBWAVE: /* DBG_DUMP_FX_PARM */
    case FX_MULTI_TREMWAVE: /* DBG_DUMP_FX_PARM */
      DBG_PRINTF("%s wave=%hhX",(char*)s,(u8)parm&0xF);
      break;
    case FX_MULTI_FINETUNE: /* DBG_DUMP_FX_PARM */
      DBG_PRINTF("%s finetune=%hhX",(char*)s,(u8)parm&0xF);
      break;
    case FX_MULTI_PATLOOP: /* DBG_DUMP_FX_PARM */
      DBG_PRINTF("%s count=%hhX",(char*)s,(u8)parm&0xF);
      break;
    case FX_MULTI_RETRIG: /* DBG_DUMP_FX_PARM */
    case FX_MULTI_NOTECUT: /* DBG_DUMP_FX_PARM */
    case FX_MULTI_NOTEDLY: /* DBG_DUMP_FX_PARM */
      DBG_PRINTF("%s ticks=%hhX",(char*)s,(u8)parm&0xF);
      break;
    case FX_MULTI_PATDLY: /* DBG_DUMP_FX_PARM */
      DBG_PRINTF("%s rows=%hhX",(char*)s,(u8)parm&0xF);
      break;
    }
    break;
  case FX_SPEED: /* DBG_DUMP_FX_PARM */
    DBG_PRINTF("%s value=%02hhX",(char*)s,(u8)parm);
    break;
  }
}

#endif /* !DEBUG */

#if DEBUG_DUMP_PATTERNS

static void DBG_DUMP_MOD_RAW_PATTERN(const struct pat_t*pat) {
  u8*d=pat->data,r=0;
  DBG_MEMDUMP(pat->data,pat->size);
  do {
    u8 c=song.num_channels;
    DBG_PRINTF("%02hhu: ",r);
    do { DBG_DUMP_CHN(*d,d[1],d[2],d[3]); d+=4; c--; } while(c);
    DBG_LOG("|"NL);
    r++;
  } while(r<SONG_PATTERN_LEN);
}

static void DBG_DUMP_MOD_PACKED36_PATTERN(const struct pat_t*pat) {
  u8 r=0;
  DBG_MEMDUMP(pat->data,pat->size);
  do {
    u8 *p=get_pattern_row(pat,r),n=song.num_channels,skip=0,repeat=0;
    u8 prev_note=0,prev_ins=0,prev_fx=0,prev_parm=0;
    DBG_PRINTF("%02hhu:%04hX=",
      (u8)r,(u16)(get_pattern_row(pat,r)-pat->data));
    do {
      u8 note,ins,fx,parm;
      if(skip) { skip--; note=ins=fx=parm=0; }
      else if(repeat) {
        repeat--; note=prev_note; ins=prev_ins; fx=prev_fx; parm=prev_parm;
      } else {
        bool save,readins;
        if(*p&128) { note=0; save=readins=true; }
        else if(*p<32) {
          if(*p) skip=*p; else skip=SONG_CHANNELS_MAX;
          skip--;
          note=ins=fx=parm=0; p++;
          save=readins=false;
        } else if(*p<32+36) {
          note=*p-32+36+1; p++; ins=fx=parm=0;
          save=true; readins=false;
        } else if(*p<32+36*2) {
          note=*p-32-36+36+1; p++;
          save=readins=true;
        } else {
          repeat=*p-32-36*2; p++;
          note=prev_note; ins=prev_ins; fx=prev_fx; parm=prev_parm;
          save=readins=false;
        }
        if(readins) {
          bool readfx;
          if(*p&64) { ins=(*p&31)+1; readfx=*p&32; p++; }
          else { ins=0; readfx=true; }
          if(readfx) { fx=(*p&15)+1; parm=p[1]; p+=2; }
          else fx=parm=0;
        }
        if(save) {
          prev_note=note;
          prev_ins=ins;
          prev_fx=fx;
          prev_parm=parm;
        }
      }
      DBG_DUMP_CHN(note,ins,fx,parm);
      n--;
    } while(n);
    DBG_LOG("|"NL);
    r++;
  } while(r<SONG_PATTERN_LEN);
}

static void DBG_DUMP_MOD_PACKED96_PATTERN(const struct pat_t*pat) {
  u8 r=0;
  DBG_MEMDUMP(pat->data,pat->size);
  do {
    u8 *p=get_pattern_row(pat,r),n=song.num_channels,skip=0,repeat=0;
    u8 prev_note=0,prev_ins=0,prev_fx=0,prev_parm=0;
    DBG_PRINTF("%02hhu:%04hX=",
      (u8)r,(u16)(get_pattern_row(pat,r)-pat->data));
    do {
      u8 note,ins,fx,parm;
      if(skip) { skip--; note=ins=fx=parm=0; }
      else if(repeat) {
        repeat--; note=prev_note; ins=prev_ins; fx=prev_fx; parm=prev_parm;
      } else {
        bool save,readins;
        if(*p&128) { note=0; save=readins=true; }
        else if(*p<=2) {
          if(*p==0) skip=SONG_CHANNELS_MAX-1;
          else {
            if(*p==2) p++;
            skip=*p-1;
          }
          p++;
          note=ins=fx=parm=0;
          save=readins=false;
        } else if(*p<=4) {
          if(*p==4) { p++; repeat=*p-1; } else repeat=0;
          p++;
          note=prev_note; ins=prev_ins; fx=prev_fx; parm=prev_parm;
          save=readins=false;
        } else if(*p<=6) {
          if(*p==6) readins=true; else readins=false;
          save=true; p++;
          note=*p; p++;
        } else if(*p<=7+60-1) {
          note=*p-7+24+1; p++;
          ins=fx=parm=0;
          save=true; readins=false;
        } else {
          note=*p-7-60+24+1; p++;
          save=readins=true;
        }
        if(readins) {
          bool readfx;
          if(*p&64) { ins=(*p&31)+1; readfx=*p&32; p++; }
          else { ins=0; readfx=true; }
          if(readfx) { fx=(*p&15)+1; parm=p[1]; p+=2; }
          else fx=parm=0;
        }
        if(save) {
          prev_note=note;
          prev_ins=ins;
          prev_fx=fx;
          prev_parm=parm;
        }
      }
      DBG_DUMP_CHN(note,ins,fx,parm);
      n--;
    } while(n);
    DBG_LOG("|"NL);
    r++;
  } while(r<SONG_PATTERN_LEN);
}

static void DBG_DUMP_PATTERN(const struct pat_t*pat) {
  DBG_PRINTF("pattern=%hd: ",(i16)get_pattern_index(pat));
  if(pat->data) {
    const char*s;
    u16 raw;
    switch(pat->format) {
    case PATFMT_MOD_RAW:      s="raw MOD"; break;
    case PATFMT_MOD_PACKED36: s="packed MOD (36 notes)"; break;
    case PATFMT_MOD_PACKED96: s="packed MOD (96 notes)"; break;
    default:                  s="unknown"; break;
    }
    DBG_PRINTF("type=\"%s\", size=%hu",(char*)s,(u16)pat->size);
    switch(pat->format) {
    default:
    case PATFMT_MOD_RAW: break;
    case PATFMT_MOD_PACKED36:
    case PATFMT_MOD_PACKED96:
      raw=song.num_channels*4*SONG_PATTERN_LEN;
      DBG_PRINTF(" (-%hu bytes, -%hhu%%)",
        (u16)raw-pat->size,(u8)(((u32)100-pat->size*100/raw)));
      break;
    }
    DBG_LOG(NL);
    switch(pat->format) {
    case PATFMT_MOD_RAW:      DBG_DUMP_MOD_RAW_PATTERN(pat); break;
    case PATFMT_MOD_PACKED36: DBG_DUMP_MOD_PACKED36_PATTERN(pat); break;
    case PATFMT_MOD_PACKED96: DBG_DUMP_MOD_PACKED96_PATTERN(pat); break;
    default: break;
    }
  } else {
    DBG_LOG("<empty>"NL);
  }
}

static void DBG_DUMP_PATTERNS() {
  if(song.num_patterns) {
    u16 size;
    u32 size_raw,size_packed;
    const struct pat_t*p;
    u16 i;

    size=song.pat_rowsize*SONG_PATTERN_LEN;
    size_raw=song.num_patterns*((size+15)&~15);
    size_packed=0;
    p=song.patterns;
    for(i=0;i<song.num_patterns;i++) {
      DBG_DUMP_PATTERN(p);
      size_packed+=(p->size+15)&~15;
      p++;
    }

    DBG_PRINTF("Raw patterns size: %u bytes, packed size: %u bytes "
      "(-%u bytes, -%hhu%%)"NL,
      (u32)size_raw,(u32)size_packed,
      (u32)size_raw-size_packed,(u8)((u32)100-size_packed*100/size_raw));
  }
}

#else /* !DEBUG_DUMP_PATTERNS */

# define DBG_DUMP_PATTERNS()

#endif /* !DEBUG_DUMP_PATTERNS */

#if DEBUG

void DBG_DUMP_SONG() {
  DBG_PRINTF(
    "num_instruments=%hhu, "
    "num_samples=%hhu, "
    "num_channels=%hhu, "
    "num_patterns=%hu, "
    "order_len=%hhu"NL,
    (u8)song.num_instruments,
    (u8)song.num_samples,
    (u8)song.num_channels,
    (u16)song.num_patterns,
    (u8)song.order_len
  );
  if(DEBUG_DUMP_INSTRUMENTS) { DBG_DUMP_INSTRUMENTS(); }
  if(DEBUG_DUMP_SAMPLES) { DBG_DUMP_SAMPLES(); }
  if(DEBUG_DUMP_ORDER) { DBG_DUMP_ORDER(); }
  if(DEBUG_DUMP_PATTERNS_STATS) { DBG_DUMP_PATTERNS_STATS(); }
  if(DEBUG_DUMP_PATTERNS) { DBG_DUMP_PATTERNS(); }
}

#else /* !DEBUG */

# define DBG_DUMP_SONG()

#endif /* !DEBUG */

/****************************************************************************/

static void song_free_samples() {
  struct ins_t*i;
  u8 n;

  if(DEBUG_SONG) { DBG_ENTER(); }
  n=song.num_instruments;
  i=song.instruments;
  while(n) {
    if(i->data) { free(i->data); i->data=NULL; }
    i++; n--;
  }
  song.num_instruments=0;
  song.num_samples=0;
  if(DEBUG_SONG) { DBG_LEAVE(); }
}

/****************************************************************************/

static void song_free_patterns() {
  struct pat_t*p;
  u16 n;

  if(DEBUG_SONG) { DBG_ENTER(); }
  n=song.num_patterns;
  song.num_patterns=0;
  song.pat_rowsize=0;
  p=song.patterns;
  while(n) {
    if(p->data) { free(p->data); p->data=NULL; }
    p++; n--;
  }
  if(DEBUG_SONG) { DBG_LEAVE(); }
}

/****************************************************************************/

void song_free() {
  if(DEBUG_SONG) { DBG_ENTER(); }
  song_free_samples();
  song_free_patterns();
  if(DEBUG_SONG) { DBG_LEAVE(); }
}
