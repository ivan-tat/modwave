/*
 * song.h - internal song representation
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef SONG_H
#define SONG_H

#include <stdbool.h>
#include "Defines.h"
#include "common.h"

#define SONG_NOTES 96
#define SONG_INSTRUMENTS_MAX 31
#define SONG_CHANNELS_MAX 32
#define SONG_PATTERN_LEN 64
#define SONG_PATTERNS_MAX 256
#define SONG_ORDER_MAX 128
#define SONG_ORDER_SKIP -1
#define SONG_TEMPO_MIN 32 /* max. 255 */
#define SONG_TPR_MAX 31 /* min. 1 */
#define SONG_VOL_BITS 6
#define SONG_VOL_MAX (1<<SONG_VOL_BITS)
#define SONG_PAN_L 0
#define SONG_PAN_C 64
#define SONG_PAN_R 128 /* everything above will be 64 */
/*#define SONG_PAN_S 164*/ /* Surround: NOT USED */

struct ins_t {
  u8 vol;
  u8 tune;
  u32 len;
  u32 lbeg;
  u32 llen;
  i8 *data;
};

/* Channel Effects */
#define FX_ARP       1  /* Effect 0: Arpeggio */
#define FX_SLUP      2  /* Effect 1: Pitch slide up */
#define FX_SLDN      3  /* Effect 2: Pitch slide down */
#define FX_SLTN      4  /* Effect 3: Pitch slide to note */
#define FX_VIB       5  /* Effect 4: Pitch vibrato */
#define FX_SLTNVOLSL 6  /* Effect 5: Continue 3, but also do 10 */
#define FX_VIBVOLSL  7  /* Effect 6: Continue 5, but also do 10 */
#define FX_TREM      8  /* Effect 7: Tremolo */
#define FX_PAN       9  /* Effect 8: Set panning position */
#define FX_SAMOFS    10 /* Effect 9: Set sample offset */
#define FX_VOLSL     11 /* Effect 10: Volume slide */
#define FX_JUMP      12 /* Effect 11: Position Jump */
#define FX_VOL       13 /* Effect 12: Set volume */
#define FX_BREAK     14 /* Effect 13: Pattern Break */
#define FX_MULTI     15 /* Effect 14: Multi-effect (see below) */
#define FX_SPEED     16 /* Effect 15: Set ticks per row or tempo */
#define FX_MAX       16

/* Multi-effect */
#define FX_MULTI_0        0  /* Effect 14.0: Set filter on/off (N/A) */
#define FX_MULTI_FSLUP    1  /* Effect 14.1: Fine pitch slide up */
#define FX_MULTI_FSLDN    2  /* Effect 14.2: Fine pitch slide down */
#define FX_MULTI_3        3  /* Effect 14.3: Set glissando on/off (TODO) */
#define FX_MULTI_VIBWAVE  4  /* Effect 14.4: Set vibrato waveform */
#define FX_MULTI_FINETUNE 5  /* Effect 14.5: Set fine-tune */
#define FX_MULTI_PATLOOP  6  /* Effect 14.6: Pattern loop */
#define FX_MULTI_TREMWAVE 7  /* Effect 14.7: Set tremolo waveform */
#define FX_MULTI_8        8  /* Effect 14.8: No effect */
#define FX_MULTI_RETRIG   9  /* Effect 14.9: Note retrigger */
#define FX_MULTI_FVOLSLUP 10 /* Effect 14.10: Fine volume slide up */
#define FX_MULTI_FVOLSLDN 11 /* Effect 14.11: Fine volume slide down */
#define FX_MULTI_NOTECUT  12 /* Effect 14.12: Note cut */
#define FX_MULTI_NOTEDLY  13 /* Effect 14.13: Note delay */
#define FX_MULTI_PATDLY   14 /* Effect 14.14: Pattern delay */
#define FX_MULTI_15       15 /* Effect 14.15: Invert loop (N/A) */

/* Pattern format */
#define PATFMT_MOD_RAW 0
#define PATFMT_MOD_PACKED36 1
#define PATFMT_MOD_PACKED96 2

struct pat_t {
  u8 format;
  u16 size;
  u8*data;
};

extern struct song_t {
  /* instruments/samples */
  struct ins_t instruments[SONG_INSTRUMENTS_MAX];
  u8 num_instruments;
  u8 num_samples;
  /* patterns */
  u8 num_channels;
  u16 num_patterns;
  u16 pat_rowsize; /* =channels*4 */
  struct pat_t patterns[SONG_PATTERNS_MAX];
  /* order */
  u8 order_len;
  i16 order[SONG_ORDER_MAX];
} song;

#if DEBUG
extern char*DBG_GET_NOTE_NAME(char*dst,u8 note); /* store 4 bytes in `dst' */
extern const char*DBG_GET_FX_NAME(u8 fx,u8 parm);
extern void DBG_DUMP_FX_PARM(u8 fx,u8 parm);
extern void DBG_DUMP_SONG();
#else /* !DEBUG */
# define DBG_GET_NOTE_NAME(a,b)
# define DBG_GET_FX_NAME(a,b)
# define DBG_DUMP_FX_PARM(a,b)
# define DBG_DUMP_SONG()
#endif /* !DEBUG */

/* Returns `true' on success */
bool is_mod_raw36_pattern(const struct pat_t*pat);

/* Returns: 0=success, 1=memory allocation failed, 2=not compressable */
extern u8 pack_pattern(struct pat_t*p);

#if DEBUG
extern i16 get_pattern_index(const struct pat_t*p);
#endif /* DEBUG */
extern u8*get_pattern_row(const struct pat_t*p,u8 r);

#if DEBUG
extern u8 get_instrument_index(const struct ins_t*p);
#endif /* DEBUG */

extern void song_clear();
extern void song_free();

#endif /* !SONG_H */
