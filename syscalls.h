/*
 * syscalls.h - platform-specific routines
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#ifndef SYSCALLS_H
#define SYSCALLS_H

#include <fcntl.h>  /* open(), close() */
#include <unistd.h> /* lseek(), read(), write() */

#endif /* !SYSCALLS_H */
