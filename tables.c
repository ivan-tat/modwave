/*
 * tables.c - static tables
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include "common.h"
#include "song.h"
#include "tables.h"

const u16 Finetune[16]={
#include "Finetune.inc"
};
const u16 Note_periods[SONG_NOTES*16]={
#include "Periods.inc"
};
const u16 Semitones[16]={ /* for arpeggio */
#include "Semitone.inc"
};
const i8 Sinewave[64]={
#include "Sinewave.inc"
};
