/*
 * tables.h - static tables
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef TABLES_H
#define TABLES_H

#include "common.h"
#include "song.h"

/* format: unsigned fixed point 1.15 */
extern const u16 Finetune[16];

/* format: 4 times Amiga note period */
extern const u16 Note_periods[SONG_NOTES*16];

/* format: unsigned fixed point 1.16 minus 1 to fit in 16 bits */
extern const u16 Semitones[16];

/* format: signed char, range: -32..32 */
extern const i8 Sinewave[64];

#define PERIOD_MIN Note_periods[SONG_NOTES-1]
#define PERIOD_MAX Note_periods[0]

#endif /* !TABLES_H */
