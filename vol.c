/*
 * vol.c - low quality volume table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdlib.h> /* malloc(), free() */
#include <string.h> /* memset() */
#include "Defines.h"
#include "common.h"
#include "vol.h"
#include "song.h"

i8 (*Voltab)[(SONG_VOL_MAX+1)*256]=NULL;

/*
 * volume=0->SONG_VOL_MAX
 * x=-128->127
 * Voltab[volume*256+(x+128)^128]=x*volume/SONG_VOL_MAX
 *
 * Returns `false' on success, `true' on error
 */
bool init_Voltab() {
  i8*t;
  i16 Y=-128;
  u8 s=1;
  if(!(Voltab=malloc(sizeof(*Voltab)))) return true;
  t=(i8*)Voltab;
  memset(t,0,256); t+=256;
  do {
    i16 y;
    u8 n;
    y=0; n=128; do { *t=y/SONG_VOL_MAX; y+=s; t++; n--; } while(n);
    y=Y; n=128; do { *t=y/SONG_VOL_MAX; y+=s; t++; n--; } while(n);
    Y-=128; s++;
  } while(s<=SONG_VOL_MAX);
  return false;
}

void free_Voltab() {
  if(Voltab) {
    free(Voltab);
    Voltab=NULL;
  }
}
