/*
 * vol.h - low quality volume table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef VOL_H
#define VOL_H

#include "Defines.h"
#include "common.h"
#include "song.h"

extern i8 (*Voltab)[(SONG_VOL_MAX+1)*256];
extern bool init_Voltab();
extern void free_Voltab();

#endif /* !VOL_H */
