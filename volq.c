/*
 * volq.c - normal quality volume table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdlib.h> /* malloc(), free() */
#include <string.h> /* memset() */
#include "Defines.h"
#include "common.h"
#include "volq.h"
#include "song.h"

i16 (*Voltab_q)[(SONG_VOL_MAX+1)*256]=NULL;

u8 calc_ovs(u8 channels) {
  u8 n=0;
  channels>>=1;
  while(channels) { channels>>=1; n++; }
  return (n<=OVS_Q_MAX)?n:OVS_Q_MAX;
}

/*
 * ovs=0->OVS_Q_MAX
 * volume=0->SONG_VOL_MAX
 * x=-128->127
 * Voltab_q[volume*256+(x+128)^128]=x*volume*(256/SONG_VOL_MAX)/(1<<ovs)
 *
 * Returns `false' on success, `true' on error
 */
bool init_Voltab_q(u8 ovs) {
  i16*t,d=1<<ovs;
  i16 Y=-128;
  u8 s=1;
  if(!(Voltab_q=malloc(sizeof(*Voltab_q)))) return true;
  t=(i16*)Voltab_q;
  memset(t,0,256*2); t+=256;
  do {
    i16 y;
    u8 n;
    y=0; n=128; do { *t=y*(256/SONG_VOL_MAX)/d; y+=s; t++; n--; } while(n);
    y=Y; n=128; do { *t=y*(256/SONG_VOL_MAX)/d; y+=s; t++; n--; } while(n);
    Y-=128; s++;
  } while(s<=SONG_VOL_MAX);
  return false;
}

void free_Voltab_q() {
  if(Voltab_q) {
    free(Voltab_q);
    Voltab_q=NULL;
  }
}
