/*
 * volq.h - normal quality volume table
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef VOLQ_H
#define VOLQ_H

#include "Defines.h"
#include "common.h"
#include "song.h"

#define OVS_Q_MAX 5 /* maximal output volume shift */

extern i16 (*Voltab_q)[(SONG_VOL_MAX+1)*256];
extern u8 calc_ovs(u8 channels);
extern bool init_Voltab_q(u8 ovs);
extern void free_Voltab_q();

#endif /* !VOLQ_H */
