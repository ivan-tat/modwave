/*
 * wav.c - basic WAV file writer
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include "common.h"
#include "errors.h"
#include "syscalls.h"
#include "wav.h"

static int _riffwave_fd=-1;

#pragma pack(push,1)
struct _chunk_hdr_t {
  u32 dID;
  u32 dSize;
};
struct _wave_fmt_t {
  struct _chunk_hdr_t hdr; /* "fmt " */
  u16 wPCM;             /* 1 */
  u16 wChannels;        /* 1 or 2 */
  u32 dSampleRate;
  u32 dBytesPerSec;     /* SampleRate*BitsPerSample*Channels/8 */
  u16 wBytesPerChannel; /* BitsPerSample*Channels/8 */
  u16 wBitsPerSample;   /* 8 or 16 */
};
static struct _riffwave_t {
  struct _chunk_hdr_t hdr;  /* "RIFF" */
  u32 dWaveID;              /* "WAVE" */
  struct _wave_fmt_t fmt;
  struct _chunk_hdr_t data; /* "data" */
} _riffwave_hdr={
  .hdr={
    .dID=MK_U32('R','I','F','F'),
    .dSize=sizeof(struct _riffwave_t)-sizeof(struct _chunk_hdr_t)
  },
  .dWaveID=MK_U32('W','A','V','E'),
  .fmt={
    .hdr={
      .dID=MK_U32('f','m','t',' '),
      .dSize=sizeof(struct _wave_fmt_t)-sizeof(struct _chunk_hdr_t)
    },
    .wPCM=1,
    .wChannels=0,
    .dSampleRate=0,
    .dBytesPerSec=0,
    .wBytesPerChannel=0,
    .wBitsPerSample=0
  },
  .data={
    .dID=MK_U32('d','a','t','a'),
    .dSize=0
  }
};
#pragma pack(pop)

/****************************************************************************/

static ERR _wave_write_header() {
  if (write(_riffwave_fd, &_riffwave_hdr,sizeof(struct _riffwave_t))!=
    sizeof(struct _riffwave_t)) return E_FWRITE;
  return E_OK;
}

/****************************************************************************/

ERR wave_create(const char*filename,u32 rate,u8 channels,u8 bits) {
  _riffwave_hdr.fmt.wChannels=channels;
  _riffwave_hdr.fmt.dSampleRate=rate;
  _riffwave_hdr.fmt.wBitsPerSample=bits;
  _riffwave_hdr.fmt.wBytesPerChannel=
    _riffwave_hdr.fmt.wBitsPerSample*_riffwave_hdr.fmt.wChannels/8;
  _riffwave_hdr.fmt.dBytesPerSec=
    _riffwave_hdr.fmt.dSampleRate*_riffwave_hdr.fmt.wBytesPerChannel;
  _riffwave_fd=open(filename,O_CREAT|O_WRONLY|O_TRUNC
#if defined(__linux__) || defined(__unix__)
    /*,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH*/
    ,0644
#endif /* defined(__linux__) || defined(__unix__) */
  );
  return(_riffwave_fd!=-1)?E_OK:E_FCREATE;
}

/****************************************************************************/

ERR wave_write(const void*buf,u16 size) {
  ERR err=E_OK;
  if(_riffwave_fd==-1) goto _exit;
  if(!_riffwave_hdr.data.dSize) {
    err=_wave_write_header(); if(err!=E_OK) goto _exit;
  }
  _riffwave_hdr.data.dSize+=size;
  if(write(_riffwave_fd,buf,size)!=size) err=E_FWRITE;
_exit:
  return err;
}

/****************************************************************************/

u32 wave_written() {
  return _riffwave_hdr.data.dSize;
}

/****************************************************************************/

void wave_close() {
  if((_riffwave_fd==-1)||(!_riffwave_hdr.data.dSize)) return;
  /* update values */
  _riffwave_hdr.hdr.dSize=_riffwave_hdr.data.dSize+
    sizeof(struct _riffwave_t)-sizeof(struct _chunk_hdr_t);
  if(lseek(_riffwave_fd,0,SEEK_SET)==0) _wave_write_header();
  close(_riffwave_fd);
}
