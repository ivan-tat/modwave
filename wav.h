/*
 * wav.h - basic WAV file writer
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#pragma once

#ifndef RIFFWAVE_H
#define RIFFWAVE_H

#include <stdbool.h>
#include "common.h"
#include "errors.h"

extern ERR wave_create(const char*filename,u32 rate,u8 channels,u8 bits);
extern ERR wave_write(const void*buf,u16 size);
extern u32 wave_written();
extern void wave_close();

#endif /* !RIFFWAVE_H */
